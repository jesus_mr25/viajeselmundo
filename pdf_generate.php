<?php

//DESDE AQUI ES DONDE LLAMO AL PLUGIN MPDF PARA GENERAR EL DOCUMENTO PDF A PARTIR DEL TEXTO HTML QUE LE PASO COMO VARIABLE

if (isset($_POST['html_no_user'])) {
	$html = $_POST['html_no_user'];
}

if (isset($_POST['token'])) {
	$token = "Viajes El Mundo - " . $_POST['token'] . ".pdf";
}

//AQUI ES DONDE IMPORTO LA LIBRERIA, CREO UNA NUEVA INSTANCIA, SE HACE LA CONVERSION DE HTML A PDF Y DESPUES POR EL OUTPUT SE LE PASA EL NOMBRE DEL ARCHIVO. QUE EN ESTE CASO PASO EL TOKEN DE LA COMPRA

include('mpdf60/mpdf.php');
$mpdf=new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output($token);
exit;


?>
