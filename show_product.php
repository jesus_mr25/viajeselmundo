<?php
//INICIO LA SESION Y HAGO LA CONEXION A LA BASE DE DATOS
	session_start();


	include('backend/conexionDB.php');


  if (isset($_GET['product_generic_id'])) {
		//no hago nada
  }
  else {
    header('location: index.php');
  }


	//CON ESTA CONSULTA MUESTRO TODOS LOS DATOD REFERIDOS A LAS OFERTAS

    $show_product = $db->query("select link_pdf, product_generic.id as product_generic_id, link_blog, discount.name as discount_name, discount.discount as discount_discount, subtype.id as subtype_id, title, description, `foreign`, locate, price_individual, price_group, tickets_availables, discount.id as discount_id, subclass from " . $_GET['table'] . " inner join product_generic on " . $_GET['table'] . ".id_product_generic=product_generic.id inner join discount on product_generic.id_discount=discount.id inner join subtype on product_generic.id_subtype=subtype.id where id_product_generic=" . $_GET['product_generic_id']);

	//RECORRO LOS RESULTADOS DE LA CONSULTA Y LOS ALMACENO EN VARIABLES

    foreach ($show_product as $row) {
        $title = $row['title'];
        $description = $row['description'];
        $discount = $row['discount_discount'];
        $priceIndividual = $row['price_individual'];
        $priceGroup = $row['price_group'];
        $ticketsAvailables = $row['tickets_availables'];
        $locate = $row['locate'];
        $link_blog = $row['link_blog'];
        $discountName = $row['discount_name'];
        $discountId = $row['discount_id'];
        $productGenericId = $row['product_generic_id'];
        $link_pdf = $row['link_pdf'];
    }

    //HALLO EL PRECIO CON DESCUENTO DE CADA TIPO DE PRECIO

    $price_individual_with_discount = ((100-$discount)/100)*$priceIndividual;
    $price_group_with_discount = ((100-$discount)/100)*$priceGroup;


    $imagenes = $db->query("select name from multimedia where id_product_generic=" . $_GET['product_generic_id'] . " limit 1");

    foreach ($imagenes as $images) {
      $name_img2 = $images['name'];
    }


	$host= $_SERVER["HTTP_HOST"];
	$url= $_SERVER["REQUEST_URI"];

 ?>


<!DOCTYPE html>
<html lang="es">
<head>
	<title>Viajes El Mundo</title>
	<meta charset="utf-8">
	<meta property="og:url" content="<?php echo "http://" . $host . $url; ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:site_name" content="Viajes El Mundo" />
	<meta property="og:title" content="<?php echo $title; ?>" />
	<meta property="og:image" content="<?php echo $host; ?>/backend/uploads/<?php echo $name_img2; ?>" />

    <!--SCRIPT PARA EL SHARE ON FACEBOOK-->

	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.6&appId=1702384370012754";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<!--SCRIPT PARA EL SHARE ON TWITTER-->

	<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>


<!--SCRIPT PARA EL SHARE ON GOOGLE-->

<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>


<!=================INCLUYO LOS ARCHIVOS CSS Y LOS ARCHIVOS JS ====================>

	<?php include('everycss.php'); ?>
	<?php include('everyjs.php'); ?>



</head>
<body class="page1">
<header>

    <!--MENU IZQUIERDO PARA MOSTRAR EL MENU-->

	<ul class="menu2 cleafix2">
      	<li class='parent2'> <a class="fa fa-bars"></a>
        	<div class="sublist2">
            	<ul id="active_web_edition2" class='children2'>
	            	<li><a href="index.php">Inicio</a></li>
	            	<li><a href="tickets.php">Entradas</a></li>
	            	<li><a href="sports.php">Deportes</a></li>
	            	<li><a href="freetime.php">Ocio</a></li>
					<li><a href="campus.php">Campus</a></li>
					<li><a href="offer.php">Ofertas</a></li>
					<li><a href="http://worldtrravel.blogspot.com.es/">Blog</a></li>
					<li class="two_lines"><a href="organize.php">Nosotros te lo organizamos</a></li>
            	</ul>
        	</div>
    	</li>
    </ul>

    <!--MENU DERECHO PARA INICIAR SESION-->

	<ul class="menu cleafix">
      <li class='parent'> <a class="glyphicon glyphicon-user"></a>
        <div class="sublist">
            <ul class='children'>
            <?php

            //SI NO HAY NINGUNA SESION INICIADA MUESTRA INICIAR SESION

				if (!isset($_SESSION['username'])) {
					?>
		            <li><a href="login.php">Iniciar Sesión</a></li>
		        	<?php


		        	//SI NO HAY SESION ACTIVA EL ID DEL USUARIO AL COMPRAR SERA NULL, ESTO SERA USADO PARA PASAR EL ID DEL USUARIO PARA EL TICKET DE LA COMPRA

		        	$id_user = "NULL";


				}

				//SI HAY SESION MUESTRA CERRAR SESION. ADEMAS, SI EL USUARIO LOGEADO ES UN ADMIN, TAMBIEN APARECE LA OPCION DE ABRIR EL PANEL DE CONTROL

				else {

					$result = $db->query("select username from admin where active=1 and username='" . $_SESSION['username'] . "'");

					if ($result->num_rows > 0) {
						?>
			            <li class="backend"><a href="backend/index.php">Panel de Control</a></li>
			            <!--<li><a class="active_web_edition" href="#">Editar Web</a><input class="active_check" type="checkbox" hidden></li>-->
						<?php
					}


					?>
					<li><a href="logout.php">Cerrar Sesión</a></li>
					<?php


					//CON LA SIGUIENTE CONSULTA OBTENGO EL ID DEL USUARIO PARA AÑADIRLO EN LA TABLA BUY CUANDO ESTE HAYA COMPRADO

					$result2 = $db->query("select id from user where active=1 and username='" . $_SESSION['username'] . "'");

					foreach ($result2 as $row) {
						$id_user = $row['id'];
					}

				}
				?>
            </ul>
		</div>
      </li>
    </ul>

	<!--ENLACES PARA NNAVEGAR POR LOS DISTINTOS SITIOS DE LA WEB-->

	<div class="container_99">
		<div class="menu_block">
			<ul id="active_web_edition" class="sf-menu">

				<li class="current"><a href="index.php">Inicio</a></li>
				<li><a href="tickets.php">Entradas</a></li>
				<li><a href="sports.php">Deportes</a></li>
				<li><a href="offer.php">Ofertas</a></li>
				<li style="border-radius: 100%;"><a href="index.php"><img class="image_logo" src="images/logo.png" alt="Viajes El Mundo"></a></li>
				<li><a href="campus.php">Campus</a></li>
				<li><a href="freetime.php">Ocio</a></li>
				<li><a class="more-product" style="cursor: pointer;">Otros Productos</a>
					<ul class="sub_sfmenu" style="display: none;">
						<li><a href="http://worldtrravel.blogspot.com.es/">Blog</a></li>
						<li><a href="organize.php">Nosotros te lo organizamos</a></li>
					</ul>
				</li>
				<li><a href="contact.php">Contacto</a></li>
			</ul>
		</div>

		<div class="clear"></div>
	</div>
</header>


<!--ESTE SCRIPT ES PARA MOSTRAR Y OCULTAR EL SUBMENU QUE TIENE EL MENU PRINCIPAL  (SI SE QUITA DE AQUI NO FUNCIONA)-->

<script type="text/javascript">
  $(".more-product").mouseover(function(){
    $(".sub_sfmenu").css("display", "");
  });

  $("html").click(function(){
    $(".sub_sfmenu").css("display", "none");
  });
</script>

<body class="page1">

	<div class="main">
		<div class="container_12">


			    <div class="grid_9 show_product">
        <h3><?php echo $title; ?></h3>
        <div class="map_and_slider">
          <section class="demo">
            <div class="container_product_slide">
							<section id="algo">
								<span class="container_arrow_left"></span>
	              <span class="container_arrow_right"></span>
	              <i class="flaticon-espalda prev_product_slide button_product_slide" aria-hidden="true"></i>
	              <i class="flaticon-proximo next_product_slide button_product_slide" aria-hidden="true"></i>
							</section>


                  <?php

                    $cont = 1;

                    //SI NO QUEDAN ENTRADAS DISPONIBLES SE MOSTRARA LA IMAGEN DE ENTRADAS AGOTADAS DELANTE DE TODAS LAS DEMAS

                    $query_image = $db->query("select name from multimedia where id_product_generic=" . $_GET['product_generic_id']);

                    if ($ticketsAvailables==0) {
                      echo "<img class='no_ticket' src='images/entradas-agotadas.png'>";

                       foreach ($query_image as $image_query) {
                          if ($cont == 1) {
                               echo "<div style='display: inline-block;'><img src='backend/uploads/" . $image_query['name'] . "'></div>";
                          } else {
                               echo "<div><img src='backend/uploads/" . $image_query['name'] . "'></div>";
                          }
                          $cont++;
                        }

												if ($cont<=2) {
													?>
														<style type="text/css">
															#algo {
																display: none !important;
															}
														</style>
													<?php
												}
                    }
                    else{

                      $cont = 1;

                      //MUESTRO LAS IMAGENES RELACIONADAS AL PRODUCTO QUE SE LE HA PULSADO EN LA PANTALLA ANTERIOR EN DETALLES
                      //A LA PRIMERA IMAGEN LE COLOCO LA PROPIEDAD DISPLAY: INLINE-BLOCK;

                      foreach ($query_image as $image_query) {
                          if ($cont == 1) {

                               echo "<div style='display: inline-block;'><img src='backend/uploads/" . $image_query['name'] . "'></div>";
                          } else {
                               echo "<div><img src='backend/uploads/" . $image_query['name'] . "'></div>";
                          }
                          $cont++;
                      }
                    }

										if ($cont<=2) {
											?>
												<style type="text/css">
													#algo {
														display: none !important;
													}
												</style>
											<?php
										}

                   ?>

              </div>
          </section>
          <div class="map"><?php echo $locate; ?></div>
          <br><br><br>
          <?php
          		$host= $_SERVER["HTTP_HOST"];
				      $url= $_SERVER["REQUEST_URI"];
 					?>

          <div>
          	<div class="fb-like" data-href="<?php echo "http://" . $host . $url; ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>

          	<a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php echo "http://" . $host . $url; ?>" data-text="Viajes El Mundo" data-via="viajeselmundo" data-lang="es" data-related="viajeselmundo" data-hashtags="viajeselmundo">Twittear</a>

			     <div class="google_button"><div class="g-follow" data-href="https://plus.google.com/u/0/101381277718012304374"></div></div>

          </div>
        </div>

            <br>

            <h1 style="font-size: 35px;">Descripción</h1><br>
            <div class="description"><p><?php echo $description; ?></p></div><br><br>
            <h1 style="font-size: 35px;">Precio</h1><br>

            <?php

                //SI EL ID DEL DESCUENTO ES DISTINTO DE 5 ES PORQUE TIENE DESCUENTO APLICADO POR LO QUE MUESTRA EL PRECIO REDUCIDO TACHADO Y EL PRECIO ANTERIOR

                if ($discount!=0) {

                  ?>
                    <li style="font-size: 17px">Precio Individual:</li>
                    <p class="paragraph" style="margin-bottom: 5px;"><note style="text-decoration: line-through red;">Antes: <?php echo $priceIndividual; ?> €</note></p>
                    <p class="paragraph" style="margin-bottom: 5px; "><note>Ahora: <?php echo $price_individual_with_discount; ?> €</note></p>
                    <br>
                    <li class="group_pric" style="font-size: 17px">Precio Grupo (aplicado a partir de 11 entradas): </li>
                    <p class="paragraph" style="margin-bottom: 5px; "><note style="text-decoration: line-through red;">Antes: <?php echo $priceGroup; ?> €</note></p>
                    <p class="paragraph" style="margin-bottom: 5px; "><note>Ahora: <?php echo $price_group_with_discount; ?> €</note></p>
                    <br>
                  <?php
                }

                //EN CASO CONTRARIO MUESTRA EL PRECIO SIN DESCUENTO, PUES NO TIENE DESCUENTO APLICADO POR LO QUE SOLO MUESTRA EL PRECIO NORMAL

                else {
                  ?>
                    <li style="font-size: 17px">Precio Individual:</li>
                    <p class="paragraph" style="margin-bottom: 5px;"><?php echo $priceIndividual; ?> €</p>
                    <br>
                    <li class="group_pric" style="font-size: 17px">Precio Grupo (aplicado a partir de 11 entradas): </li>
                    <p class="paragraph" style="margin-bottom: 5px;"><?php echo $priceGroup; ?> €</p>
                    <br>
                  <?php
                }

            ?>

            <br>

            <h1 style="font-size: 35px;">Entradas Disponibles</h1><br>
            <p class="paragraph" style="margin-bottom: 5px;"><?php $retVal = ($ticketsAvailables==0) ? "Entradas Agotadas" : "Quedan " . $ticketsAvailables . " entradas" ; echo $retVal ?></p>

            <br>

            <?php

            //AQUI MODIFICO EL ACTION DEL FORMULARIO EN FUNCION DE SI EL USUARIO ESTA O NO REGISTRADO, EN CUYO CASO SI NO ESTA REGISTRADO LO MANDA AL ARCHIVO FROM_DATA_BUY, PARA QUE RELLENE UN MINIMO DE DATOS, PERO SI ESTA REGISTRADO LO MANDA DIRECTAMENTE A LA DE BUY_TICKET

              if (!isset($_SESSION['username'])) {
                $action = "form_data_buy.php";
              }
              else {
                $action = "buy_ticket.php";
              }


             ?>

             <!--POR EL FORMULARIO PASO LOS DATOS NECESARIOS PARA RELLENAR LOS DATOS DE LA FACTURA-->

              <br>

              <form action="<?php echo $action; ?>" method="get">
              <h1 style="font-size: 35px;">Cantidad de Tickets</h1><br><input type="number" class="form-control range" name="num_ticket" value="0" min="1" max="<?php echo $ticketsAvailables; ?>" style="width: 80px;" required><br><br>

              <h1 style="font-size: 35px;"><?php if($discountId!=1){echo "Aplicado " . $discountName . " (" . $discount . "%)";} ?></h1>
              <h1 style="font-size: 35px; list-style-type: none;" class="group_price" hidden>Se aplicará precio de grupo <input class="group" class="form-group price_group" type="checkbox" name="type_price" hidden></h1>
              <input type="text" name="title" hidden value="<?php echo $title; ?>">

              <?php

                //SI EL ID DEL DESCUENTO ES DISTINTO DE 5 ES PORQUE TIENE DESCUENTO APLICADO. LE CARGO EN UN INPUT TEXT OCULTO EL PRECIO QUE TIENE LA OFERTA TANTO INDICIDUAL COMO DE GRUPO

                if ($discountId!=1) {
                  echo "<input type='text' name='price_individual' hidden value='" . $price_individual_with_discount . "'>
                        <input type='text' name='price_group' hidden value='" . $price_group_with_discount . "'>";
                }

                //EN CASO CONTRARIO CARGO EL PRECIO SIN DESCUENTO, TANTO INDIVIDUAL COMO DE GRUPO

                else {
                  echo "<input type='text' name='price_individual' hidden value='" . $priceIndividual . "'>
                        <input type='text' name='price_group' hidden value='" . $priceGroup . "'>";
                }


               ?>

                <input type="text" name="table" value="ticket" hidden>
                <input type="text" name="id_user" hidden value="<?php echo $id_user; ?>">
                <input type="text" name="discount" hidden value="<?php echo $discount; ?>">
                <input type="text" name="id_product_generic" hidden value="<?php echo $productGenericId; ?>">

                <br><br>

                <div class="button_buy">
                    <button class="btn btn-info">Comprar</button>
                </div>
          </form>

					<div class="button_pdf_link">

						<?php

	          //SI NO HAY LINK DEL PDF INSERTADO O LINK DEL BLOG OCULTARA EL BOTON RESPECTIVO

	            if ($link_pdf == "") {
	              echo "<button style='margin-top: 10px; display: none;' class='btn btn-info download_pdf'>Descargar Pdf</button>";
	            }
	            else {
	              ?><button style='margin-top: 10px;' class='btn btn-info download_pdf' onclick="location='backend/uploads/<?php echo $link_pdf; ?>'">Descargar Pdf</button><?php
	            }


	            if ($link_blog == "") {
	              echo "<br><button style='margin-top: 10px; display: none;' class='btn btn-info more_information' onclick='location='" . $link_blog . "''>Más Información</button>";
	            }
	            else {
	              ?><br><button style="margin-top: 10px;" class="btn btn-info more_information" onclick="window.open('<?php echo $link_blog; ?>', '_blank')">Más Información</button><?php
	            }

	          ?>

					</div>
    </div>


<!--ESTE SCRIPT EJECUTA EL CAMBIO DE IMAGEN AL PULSAR SOBRE LOS BOTONES  (NO PUEDO SACARLO DE AQUI, SINO EL SLIDER DE LOS PRODUCTOS DEJA DE FUNCIONAR)-->

    <script type="text/javascript">
    var currentIndex = 0,
  items = $('.container_product_slide div'),
  itemAmt = items.length;

function cycleItems() {
  var item = $('.container_product_slide div').eq(currentIndex);
  items.hide();
  item.css('display','inline-block');
}

var autoSlide = setInterval(function() {
  currentIndex += 1;
  if (currentIndex > itemAmt - 1) {
    currentIndex = 0;
  }
  cycleItems();
}, 3000);

$('.next_product_slide').click(function() {
  clearInterval(autoSlide);
  currentIndex += 1;
  if (currentIndex > itemAmt - 1) {
    currentIndex = 0;
  }
  cycleItems();
});

$('.prev_product_slide').click(function() {
  clearInterval(autoSlide);
  currentIndex -= 1;
  if (currentIndex < 0) {
    currentIndex = itemAmt - 1;
  }
  cycleItems();
});
</script>

		</div>
		<div class="clear"></div>
	</div>

	<!================ BLUE FOOTER ===================>

	<?php include('layouts/blue_footer.php'); ?>

	<!================ SOCIAL FOOTER ===================>

	<?php include('layouts/social_footer.php'); ?>

	<!================ BUTTON FOOTER ===================>

	<?php include('layouts/button_bottom.php'); ?>

</body>
</html>

<?php

	$db->close();

 ?>
