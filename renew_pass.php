<?php
  if (isset($_POST['email'])) {
    require('backend/conexionDB.php');

    $query_user = $db->query("select id, username from user where address='" . $_POST['email'] . "'");

    foreach ($query_user as $row23) {
      $username = $row23['username'];
    }

    if (mysqli_num_rows($query_user) > 0) {

      $host= $_SERVER["HTTP_HOST"];

      $sms2 = "<!DOCTYPE html>
      <html lang='es'>
      <head>
        <meta charset='utf-8'>
      </head>
      <body>
        <img src='http://www.viajeselmundo.esy.es/images/mini_logo.png'>
        <br>
        <br>
        <p>Hola " . $username . ",</p>
        <p>Haga click <a href='http://" . $host . "/pass_forgot.php?new_pass=" . $_POST['email'] ."'>AQUÍ</a> para restaurar su contraseña</p>
        <br>
        <br>
        <p>Si usted no ha solicitado ningún cambio de contraseña, por favor ignore este mensaje.</p>
      </body>
      </html>";

      require('phpmailer/PHPMailerAutoload.php');

      $subject = "Recuperación de contraseña";
      $email = $_POST['email'];


      //CREO UN NUEVA CLASE DE PHPMAILER
      //LE PASO EL CORREO DEL RECEPTOR Y EL NOMBRE DEL REMITENTE
      //LE PASO EL CORREO DEL RECEPTOR
      //LE PONGO EL ASUNTO

      //LE PASO EL CUERPO DEL MENSAJE
      //LE PASO LA CODIFICACION DEL LENGUAJE
      //LE PASO EL TIPO DE MENSAJE
      //Y LO ENVIO

      $mailer = new PHPMailer();
            $mailer->setFrom('no-reply@viajeselmundo.es','Recuperación de contraseña - Viajes El Mundo');
            $mailer->addAddress($_POST['email']);
            $mailer->Subject = "Recuperación de contraseña";
            $mailer->IsHTML(true);
            $mailer->Body = $sms2;
            $mailer->CharSet = 'utf-8';

      //SI NO SE ENVIA EL MENSAJE SE MOSTRARA UN MENSAJE DE DISCONFORMIDAD

      if (!$mailer->send()) {
        ?>
          <div class="callout callout-danger">
            <h4>¡Ups, algo salió mal!</h4>
            <p>El mensaje no se ha podido enviar, por favor vuelva a intentarlo.</p>
          </div>
        <?php
      }
      else {
        ?>
          <div class="callout callout-info">
            <h4>¡Estupendo!</h4>
            <p>Mensaje enviado a la dirección facilitada, si no lo recibe, por favor mire en su Bandeja de Correos Basura o Spam.</p>
          </div>
        <?php
      }
    }
    else{
      ?>
          <div class="callout callout-warning">
            <h4>¡Ups, algo salió mal!</h4>
            <p>El correo introducido no pertenece a ningún usuario registrado, por favor vuelva a intentarlo.</p>
          </div>
        <?php
    }
  }

 ?>




<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Viajes El Mundo</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="backend/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="backend/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition lockscreen">
    <!-- Automatic element centering -->
    <div class="lockscreen-wrapper">
      <div class="lockscreen-logo">
        <a href="index.php"><b>Viajes El Mundo</b></a>
      </div>

      <!-- START LOCK SCREEN ITEM -->
      <div class="lockscreen-item">

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials" method="post" action="renew_pass.php">
          <div class="input-group">
            <input type="email" class="form-control" name="email" placeholder="Email" required>
            <div class="input-group-btn">
              <button class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
            </div>
          </div>
        </form><!-- /.lockscreen credentials -->

      </div><!-- /.lockscreen-item -->
      <div class="help-block text-center">
        Introduce tu email y te ayudaremos a restaurar tu contraseña
      </div>

    </div><!-- /.center -->

    <!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
