<?php

include('backend/conexionDB.php');

$user = "";
$pass = "";
$repass = "";
$email = "";
$dni = "";
$now = date("Y-m-d") . " " . date("G:i:s");

if (isset($_POST['user'])) {

    if (isset($_POST['user'])) {
      $user = $_POST['user'];
    }

    if (isset($_POST['pass'])) {
      $pass = $_POST['pass'];
    }

    if (isset($_POST['repass'])) {
      $repass = $_POST['repass'];
    }

    if (isset($_POST['email'])) {
      $email = $_POST['email'];
    }

    if (isset($_POST['dni'])) {
      $dni = $_POST['dni'];
    }


    //SI LAS CONTRASEÑAS SON DISTINTAS CUMPLE LA CONDICION Y MUESTRA UN ALERT INDICANDO QUE LAS CONTRASSEÑAS SON DISTINTAS

    if ($pass!=$repass) {
       ?>
        <div class="callout callout-danger">
          <h4>¡Ups, algo salió mal!</h4>
          <p>Las contraseñas introducidas no son iguales, por favor vuelva a intentarlo.</p>
        </div>
      <?php
    }

    //SI LAS CONTRASEÑAS SON IGUALES CUMPLE LA CONDICION Y COMPRUEBA QUE EL USUARIO EXISTA O NO

    else {
      $query_user = $db->query("select id from user where username='" . $user . "'");

      //SI LA CONSULTA DEL USUARIO DEVUELVE ALGUN VALOR, ES PORQUE ESE USUARIO EXISTE, POR LO QUE MUESTRA UN MENSAJE DE ERROR

      if (mysqli_num_rows($query_user) > 0) {
        ?>
          <div class="callout callout-danger">
            <h4>¡Ups, algo salió mal!</h4>
                <p>Nombre de usuario asociado a otro usuario, por favor vuelva a intentarlo con otro nombre de usuario diferente o vaya a <a href='renew_pass.php'>olvidé mis credenciales</a>.</p>
          </div>
        <?php
      }

      //SI LA CONSULTA NO DEVUELVE NADA, PASA A COMPROBAR EL EMAIL

      else{
        $query_email = $db->query("select id from user where address='" . $email . "'");

        //SI LA CONSULTA DEL EMAIL DEVUELVE ALGUN VALOR, ES PORQUE ESE EMAIL EXISTE, POR LO QUE MUESTRA UN MENSAJE DE ERROR

        if (mysqli_num_rows($query_email) > 0) {
          ?>
            <div class="callout callout-danger">
              <h4>¡Ups, algo salió mal!</h4>
                <p>Correo electrónico asociado a otro usuario, por favor vuelva a intentarlo con otro email diferente o vaya a <a href='renew_pass.php'>olvidé mis credenciales</a>.</p>
            </div>
          <?php
        }

        //SI LA CONSULTA NO DEVUELVE NADA, PASA A COMPROBAR EL DNI

        else {
          $query_dni = $db->query("select id from user where dni='" . $dni . "'");

          //SI LA CONSULTA DEL DNI DEVUELVE ALGUN VALOR, ES PORQUE ESE DNI EXISTE, POR LO QUE MUESTRA UN MENSAJE DE ERROR

          if (mysqli_num_rows($query_dni) > 0) {
            ?>
              <div class="callout callout-danger">
                <h4>¡Ups, algo salió mal!</h4>
                <p>DNI asociado a otro usuario, por favor vuelva a intentarlo con otro DNI diferente o vaya a <a href='renew_pass.php'>olvidé mis credenciales</a>.</p>
              </div>
            <?php
          }
          else {

            //SI NO HA SALTADO NINGUN ERROR, REALIZO LA INSERCION DE LOS DATOS DEL NUEVO USUARIO EN LA TABLA DE USUARIO, PERO EL USUARIO NECESITARA ACEPTAR EL CORREO QUE HA RECIBIDO PARA QUE SE ACTIVE SU USUARIO

            $new_user = $db->query("insert into user values (NULL, '" . $_POST['name'] . "', '" . $_POST['surname'] . "', '" . $_POST['dni'] . "', '" . $_POST['date'] . "', '" . $_POST['email'] . "', '" . $_POST['address'] . "', '" . $_POST['phone'] . "', '" . $_POST['cp'] . "', '" . $_POST['city'] . "', '" . $_POST['country'] . "', '" . $_POST['user'] . "', '" . hash("sha256" , $_POST['pass']) . "', 0, 'NULL', '" . $now . "', '" . $now . "')");





		    //A CONTINUACION, ENVIO AL CORREO DEL USUARIO RECIEN CREADO UN MENSAJE PARA QUE PUEDA ACTIVAR SU USUARIO

		    require('phpmailer/PHPMailerAutoload.php');
		    require('functions.php');

		    $token = "";

		    //CREO UN TOKEN NUEVO Y LO AÑADO A LA TABLA DE BUY PARA QUE SE PUEDA ACTIVAR EL USUAIRO AL ACCEDER A LA WEB DESDE EL ENLACE ADJUNTADO EN EL CORREO

		    $token = getRandomCode();

		    //INTRODUZCO EL TOKEN EN LA TABLA CON EL UPDATE

		    $db->query("update user set token_to_active='" . $token . "', updated='" . $now . "' where username='" . $_POST['user'] . "'");

		    //INTRODUZCO EN LA VARIABLE SMS EL CODIGO QUE SE VISUALIZARA AL RECIBIR EL MENSAJE

		    $sms="<!DOCTYPE html>
		          <html lang='es'><head>
		            <meta charset='utf-8'>

		            </head>
		            <body class='page1' style='margin-bottom: 40px;'>
		            <header>
		              <ul class='menu cleafix'>

		                </ul>

		            </header>
		            <div class='main' style='padding: 50px;'>
		              <center>
		                <div style='height: 200px;'>
		                  <img src='http://viajeselmundo.esy.es/images/mini_logo.jpg'>
		                </div>
		              </center>
		              <div style='width: 65%; margin-right: 10px;'>
		                  <h1 style='font-size: 30px;'><a>Hola, " . $_POST['user'] . "</a></h1 style='font-size: 20px;'>
		                  <br>
		                  <p>Necesitamos verificar tu dirección de correo para completar tu registro.</p>

                      <br>

                      <p>Haz click <a href='http://viajeselmundo.esy.es/login.php?token=$token'>AQUÍ</a> para activar tu usuario.</p>
		              </div>

                  <br>

		              <div style='width: 100%;'>
		                  <p>¡Gracias!</p>

		              <br>

		              <p>- El equipo de Viajes El Mundo</p>
		              </div>
		            </div>
		          </body>
		          </html>
		    ";

		    //CREO UN NUEVA CLASE DE PHPMAILER
		    //LE PASO EL NOMBRE DEL RECEPTOR Y EL NOMBRE DEL REMITENTE
		    //LE PASO EL CORREO DEL RECEPTOR
		    //LE PONGO EL ASUNTO
		    //ACTIVO LA FUNCION ISHTML() Y LE PASO EL PARAMETRO A TRUE PARA QUE INTERPRETE EL CODIGO HTML
		    //LE PASO EL CUERPO DEL MENSAJE
		    //LE PASO LA CODIFICACION DEL LENGUAJE
		    //LE PASO EL TIPO DE MENSAJE
		    //Y LO ENVIO

		    $mailer = new PHPMailer();
		    $mailer->setFrom('no-reply@viajeselmundo.es','Nuevo Usuario - Viajes El Mundo');
		    $mailer->addAddress($_POST['email']);
		    $mailer->Subject = "Correo de activación para " . $_POST['user'];
		    $mailer->IsHTML(true);
		    $mailer->Body = $sms;
		    $mailer->CharSet = 'utf-8';
		    $mailer->mailer = 'mail';

		    //SI EL MENSAJE ES ENVIADO SIN PROBLEMAS MEREDIRECCIONA AL INDEX DE LA WEB

		    if ($mailer->send()) {
		      ?>
		        <div class="callout callout-info">
              <h4>¡Estupendo!</h4>
              <p>Su mensaje ha sido enviado, verifique su Bandeja de Entrada. En caso de demora en la recepción del mensaje, revise su bandeja de Correos Basura o Spam.</p>
            </div>
		      <?php
		    }
		    else {
		    	 ?>
			        <div class="callout callout-danger">
                <h4>¡Ups, algo salió mal!</h4>
                  <p>No se pudo enviar el mensaje, por favor vuelva a intentarlo con más tarde.</p>
              </div>
		        <?php
		    }
          }
        }
      }
    }
}
 ?>


<!--AQUI COMIENZA LA PAGINA DEL FORMULARIO DE REGISTRO-->


<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Viajes El Mundo</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="backend/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="backend/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="backend/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
      .calendar_hide {
        display: none;
      }


      @media (max-width: 980px) {

        .calendar_hide {
          display: inherit !important;
          margin-top: 9px;
        }
      }
    </style>

  </head>
  <body class="hold-transition register-page">
    <div class="register-box">
      <div class="register-logo">
        <a href="index.php"><b>Viajes El Mundo</b></a>
      </div>

      <!--LE PASO EN EL VALUE LOS DATOS DEL POST PARA QUE EN CASO DE ERROR NO TENGA QUE ESCRIBIR DE NUEVO TODOS LOS CAMPOS DEL FORMULARIO-->

      <div class="register-box-body">
        <p class="login-box-msg">Forme parte de nuestra comunidad</p>
        <form action="register.php" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Nombre" name="name" value="<?php if(isset($_POST['name'])){echo $_POST['name'];} ?>" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Apellidos" name="surname" value="<?php if(isset($_POST['surname'])){echo $_POST['surname'];} ?>" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="DNI" name="dni" value="<?php if(isset($_POST['dni'])){echo $_POST['dni'];} ?>" required>
            <span class="glyphicon glyphicon-credit-card form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="date" class="form-control" placeholder="25/06/2016" name="date" value="<?php if(isset($_POST['date'])){echo $_POST['date'];} ?>" required>
            <span id="date" class="form-control-feedback"><i class="fa fa-calendar calendar_hide" aria-hidden="true"></i></span>
          </div>
          <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email" name="email" value="<?php if(isset($_POST['email'])){echo $_POST['email'];} ?>" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Dirección" name="address" value="<?php if(isset($_POST['address'])){echo $_POST['address'];} ?>" required>
            <span class="glyphicon glyphicon-home form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Teléfono" name="phone" value="<?php if(isset($_POST['phone'])){echo $_POST['phone'];} ?>" maxlength="9" pattern="[0-9]{9}" title="Debe contener 9 números" required>
            <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Código Postal" name="cp" value="<?php if(isset($_POST['cp'])){echo $_POST['cp'];} ?>" maxlength="5" pattern="[0-9]{5}" title="Debe contener 5 números" required>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Ciudad" name="city" value="<?php if(isset($_POST['city'])){echo $_POST['city'];} ?>" required>
            <span style="margin-right: 7px;" class="fa fa-fw fa-building form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="País" name="country" value="<?php if(isset($_POST['country'])){echo $_POST['country'];} ?>" required>
            <span class="fa fa-globe form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Usuario" name="user" value="<?php if(isset($_POST['user'])){echo $_POST['user'];} ?>" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Contraseña" name="pass" value="<?php if(isset($_POST['pass'])){echo $_POST['pass'];} ?>" minlength="5" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Debe contener Mayúsculas, Minúsculas, Caracteres Especiales y debe contener al menos 8 caracteres" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Repite Contraseña" name="repass" value="<?php if(isset($_POST['repass'])){echo $_POST['repass'];} ?>" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox" required> Acepto los <a href="term_user.php">términos de uso</a>
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Registrar</button>
            </div><!-- /.col -->
          </div>
        </form>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->




    <!-- jQuery 2.1.4 -->
    <script src="backend/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="backend/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="backend/plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>

    <script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
    <script>
      webshims.setOptions('forms-ext', {types: 'date'});
      webshims.polyfill('forms forms-ext');


    </script>

  </body>
</html>
