<!--AQUI RELACIONO TODOS LOS SCRIPTS QUE NECESITO USAR EN LA WEB-->
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jqueryy.js"></script>
<script src="js/html5shiv.js"></script>
<script src="js/jquery-migrate-1.1.1.js"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="js/jquery.carouFredSel-6.1.0-packed.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.equalheights.js"></script>
<script src="js/jquery.jqtransform.js"></script>
<!--<script src="js/form.js"></script>-->
<script src="js/jquery.touchSwipe.min.js"></script>
<!--<script src="js/jquery.ui.totop.js"></script>-->
<script src="js/sForm.js"></script>
<script src="js/slider-main.js"></script>
<!--script src="js/sliders.js"></script>-->
<script src="js/superfish.js"></script>
<script src="js/tms-0.4.1.js"></script>
<script src="js/touchTouch.jquery.js"></script>
<script src="js/toolbar.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/sweetalert2.min.js"></script>




<!--CUANDO ABRO LAS VENTANAS MODALES SE PRODUCE UN ERROR REPETITIVO QUE VA INCREMENTANDO EL VALOR DEL PADDING RIGHT DE 17PX CADA VEZ QUE SE ABRE LA VENTANA MODAL. PARA ANULARLO USO ESTE SCRIPT-->

<script type="text/javascript">
    $(document).ready(function(){
      $(".modal").click(function(){
        var anchoHtml = $("html").width();
        var anchoBody = $(".page1").width();

        if (anchoBody<anchoHtml) {
          $(".page1").css("padding", 0);
        }
      });
    });
  </script>



<!--SCRIPT PARA QUE CUANDO SE PULSE UNA TECLA O SE MODIFIQUE VALOR EN EL INPUT RANGE ESTE CAMBIARA EL VALOR DEL NUMERO DE TICKETS Y ACTIVARA O NO EL CHECKBOX EN FUNCION DEL VALOR QUE TOME EN INPUT TEXT (SE ACTIVARA CUANDO SUPERE EL 10 Y SE DESACTIVARA CUANDO SEA 10 O MENOS), ademas, cada vez que haga click en el boton de detalles se me reinicia el valor del input text a ""-->

  <script type="text/javascript">
    $(document).ready(function(){
      $('.range').on('keyup change click', function() { 

        if ($(this).val() > 10) {
          $(".group").prop("checked", true);
          $(".group_price").prop("hidden", false);
        }
        else {
          $(".group").prop("checked", false);
          $(".group_price").prop("hidden", true);
        }
      });
    });
  </script> 



  <!--SCRIPT PARA CONTROLAR EL SLIDER DE LAS VENTANAS MODALES DE LOS PRODUCTOS-->

  <!--<script type="text/javascript">
  $(document).ready(function(){
    $('.slider1').bxSlider({
      slideWidth: 400,
      minSlides: 1,
      maxSlides: 1,
      slideMargin: 10
    });
  });
</script>-->



<!--SCRIPT PARA ACTIVAR LA EDICION DEL CONTENIDO EDITABLE DE LA WEB

  <script type="text/javascript">
  $(document).ready(function(){
    $(".active_web_edition").click(function(){
      if($(".active_check").is(":checked")) {
        $(".active_check").prop("checked", false);
        $(".active_web_edition").text("Editar Web");
        $("#active_web_edition").prop("contenteditable", false);
        $("#active_web_edition2").prop("contenteditable", false);
      }
      else {
        $(".active_check").prop("checked", true);
        $(".active_web_edition").text("Finalizar edición");
        $("#active_web_edition").prop("contenteditable", true);
        $("#active_web_edition2").prop("contenteditable", true);
      }
    });
  })
</script>-->