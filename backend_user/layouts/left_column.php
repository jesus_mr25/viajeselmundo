<?php

  //CON ESTAS CONDICIONES DEPENDIENDO DE LA PAGINA EN LA QUE ESTE POSICIONADO, MANTENDRE ACTIVA UNOS ENLACE U OTROS



  if (basename($_SERVER['PHP_SELF']) == "data_user.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_data_user").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "change_pass_user.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_change_pass_user").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "view_buy.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_view_buy").addClass("active");
          });
        </script>
     <?php
  }


   ?>




<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="../backend/dist/img/user-men-2-5121.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php get_name_admin($user, $db); ?></p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">Opciones</li>
            <!-- Optionally, you can add icons to the links -->
              <li id="td_data_user"><a href="data_user.php"><i class="fa fa-user" aria-hidden="true"></i> <span>Datos Personales</span></a></li>
              <li id="td_change_pass_user"><a href="change_pass_user.php"><i class="fa fa-key" aria-hidden="true"></i> <span>Cambiar Contraseña</span></a></li>
              <li id="td_view_buy"><a href="view_buy.php"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Compras realizadas</span></a></li>
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
