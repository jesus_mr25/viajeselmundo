<!--ESTA ES LA PAGINA DONDE ESTA LA PAGINA CON LOS DATOS DEL USUARIO REGISTRADO DEL PANEL DE CONTROL DEL USUARIO-->


<?php

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

    date_default_timezone_set("Europe/Madrid");
    $now = date("Y-m-d") . " " . date("G:i:s");


    //COMPRUEBO SI LA CONTRASEÑA ANTIGUA ES LA MISMA QUE LA INTRODUCIDA

    if (isset($_POST['old_pass'])) {
      $query_pass = $db->query("select pass from user where username='" . $user . "'");

      $old_pass = "";

      foreach ($query_pass as $row33) {
        $old_pass = $row33['pass'];
      }

      //SI SON DISTINTAS MUESTRA MESAJE DE ERROR

      if (hash("sha256", $_POST['old_pass']) != $old_pass) {
        ?>
            <style type="text/css">
                .error_old_pass, .message_error_pass_user {
                  display: inherit !important;
                }
            </style>
        <?php
      }
      else {

        //SI LAS CONTRASEÑAS NUEVAS SON IGUALES SE ACTUALIZA LA CONTRASEÑA

        if ($_POST['new_pass']==$_POST['re_new_pass']) {
          $result5 = $db->query("update user set pass='" . hash("sha256", $_POST['new_pass']) . "' where id='" . $_POST['id'] . "'");


          //TO REFRESH THE PAGE, AND ADMIN CAN SEE HOW CHANGE THE DATABASE WITHOUT PRESS F5

          if (!$result5) {
              ?>
                  <style type="text/css">
                      .message_error_pass_user, .general_error {
                        display: inherit !important;
                      }
                  </style>
              <?php
          }
          else {
          ?>
              <style type="text/css">
                .message_info_pass_user {
                  display: inherit !important;
                }
              </style>
          <?php
          }
        }

        //SI LAS CONTRASEÑAS NO COINCIDEN SE MUESTRA MENSAJE DE ERROR

        else {
          ?>
              <style type="text/css">
                  .error_new_pass, .message_error_pass_user {
                    display: inherit !important;
                  }
              </style>
          <?php
        }

      }
    }

 ?>


<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS TICKETS DE DESCUENTO-->

 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Contraseña de <?php echo $user; ?>
          <small>Cambio de contraseña de tu usuario</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

     	<!-- Your Page Content Here -->

      <div class="callout callout-success message_info_pass_user" style="display: none;">
        <h4>Contraseña cambiada!</h4>
        <p>Cambio de contraseña realizado satisfactoriamente.</p>
      </div>

      <div class="callout callout-warning message_error_pass_user" style="display: none;">
        <h4>¡Ups, algo ha salido mal!</h4>
        <p class="general_error" style="display: none;">El cambio de contraseña no se ha podido realizar, vuelva a intentarlo más tarde.</p>
        <p class="error_old_pass" style="display: none;">La contraseña antigua no es correccta, por favor vuelva a intentarlo.</p>
        <p class="error_new_pass" style="display: none;">Las contraseñas no coinciden, por favor vuelva a intentarlo.</p>
      </div>

      	<div class="box">
            <div class="box-header">

            </div><!-- /.box-header -->

            <div class="box-body">

            <!--CARGO LAS OPCIONES DISPONIBLES DE ESTA TABLA EN EL COMBOBOX-->

                <?php
                    //REALIZO LA CONSULTA A BASE DE DATOS PARA OBTENER LOS ELEMENTOS QUE NECESITO. POR DEFECTO NO MUESTRO NINGUNO, PERO DESPUES VOY MOSTRANDO EL QUE ESTE SELECCIONADO EN EL COMBOBOX

                 ?>

                <!--le paso los valores del get para que cuando cambie el combobox cambie tambien en los inputs-->
                <form class="form-sport" method="post" action="change_pass_user.php">

                  <?php

                    $select_user_data = $db->query("select * from user where username='" . $user . "'");

                    foreach ($select_user_data as $row32) {

                      $date_born = explode("-", $row32['date_born']);

                   ?>

                      <p style="font-size: 15px;">Contraseña Antigua<input type="password" class="form-control" style="width: 100%;" name="old_pass" value="<?php if(isset($_POST['old_pass'])) {echo $_POST['old_pass'];} ?>" required></p>
                      <p style="font-size: 15px;">Contraseña Nueva<input type="password" class="form-control" style="width: 100%;" name="new_pass" value="<?php if(isset($_POST['new_pass'])) {echo $_POST['new_pass'];} ?>" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required></p>
                      <p style="font-size: 15px;">Repite Contraseña Nueva<input type="password" class="form-control" style="width: 100%;" name="re_new_pass" value="<?php if(isset($_POST['re_new_pass'])) {echo $_POST['re_new_pass'];} ?>" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" required></p>

                    <?php

                    }

                     ?>


                    <p><input type="text" name="id" value="<?php echo $row32['id'] ?>" style="display: none;"></p>
                    <input class="btn btn-info" type="submit" style="background-color: #3c8dbc;" name="update" value="Actualizar Datos">
                </form>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
