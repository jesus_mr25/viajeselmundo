<?php

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

    date_default_timezone_set("Europe/Madrid");

?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS TICKETS DE DEPORTES-->

     <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Compras realizadas
          <small>Descripción de la compras realizadas por <?php echo $user; ?></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

     	<!-- Your Page Content Here -->

      	<div class="box">
            <div class="box-header">

            </div><!-- /.box-header -->

            <div class="box-body table_buy">

            <table aria-describedby="example2_info" role="grid" id="example2" class="table table-bordered table-hover dataTable">
                    <thead>
                      <tr role="row">
                        <th>Usuario</th>
                        <th>Email</th>
                        <th>Precio</th>
                        <th>Fecha Compra</th>
                        <th style="border-top: hidden; border-right: hidden;"></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php

                    $cnt = 1;

                    //CREO UN ARRAY EN EL QUE CONTENGO CARACTERES QUE ESTAN USADOS, MAS ABAJO, COMO SEPARADORES DE FECHA Y HORA

                    $characters = array("-", " ", ":");

                    //REALIZO LA CONSULTA DE LOS DATOS QUE NECESITO DE LA TABLA DE LAS COMPRAS REALIZADAS

                    $query= $db->query("select buy.id as id_buy, id_type, id_user, buy_code, discount, total, buy_day, full_name, buy.address, buy.post_code, buy.city, phone, buy.email, name, username from buy inner join user on buy.id_user=user.id where username='" . $user . "'");

                    foreach ($query as $row) {
                         echo "<tr style='cursor: pointer; text-align: center;' data-toggle='modal' data-target='#myModal" . $cnt . "' style='text-align:center;'>";

                                //SI EL NOMBRE DEL USUARIO ES NO_USER APARECERA NO REGISTRADO EN LA TABLA

                                if ($row['name']=="no_user") {
                                    echo "<td>No Registrado</td>";
                                }

                                //EN CASO CONTRARIO APARECERAA EL USERNAME DEL USUARIO

                                else{
                                    echo "<td>" . $row['username'] . "</td>";
                                }

                                //ADEMAS IMPRIMO EN PANTALLA EL EMAIL Y EL PRECIO DE LA COMPRA

                                echo "<td>" . $row['email'] . "</td>
                                <td>" . $row['total'] . " €</td><td>";

                                //AHORA REEMPLAZO LOS CARACTERES QUE COINCIDAN CON LOS DEL ARRAY POR LAS BARRAS OBLICUAS

                                $date = str_replace($characters, "/", $row['buy_day']);

                                //DIVIDO LA CADENA $date DIVIDIENDOLO POR LAS BARRAS PARA OBTENER UN ARRAY Y ASI IMPRIMO LA FECHA EN UN FORMATO BONITO PARA EL ADMINISTRADOR

                                $result = explode("/", $date);

                                echo $result[3] . ":" . $result[4] . ":" . $result[5] . " " . $result[2] . "-" . $result[1] . "-" . $result[0];

                            echo "</td><td><i class='glyphicon glyphicon-eye-open'></i></td></tr>";

                            ?>

                            <!--VENTANA MODAL CON LOS DATOS DE LA VENTA REALIZADA-->

                            <div class="modal fade" id="myModal<?php echo $cnt ?>" role="dialog">
                                <div class="modal-dialog modal_table_buy">

                                  <!-- Modal content-->
                                    <div class="modal-content" style="padding: 22px;">

                                        <div class="modal-header">
                                            <h2>Descripción detallada de la compra
                                           <button type="button" class="close" data-dismiss="modal">X</button></h2>
                                        </div>
                                        <div class="modal-body  modal_body_table_buy">

                                            <?php

                                                $query2 = $db->query("select class from type where id=" . $row['id_type']);

                                                foreach ($query2 as $row2) {
                                                    $class = $row2['class'];
                                                }

                                             ?>
                                             <div class="left_description">

                                                 <h3>Tipo de Producto</h3>
                                                 <p><?php echo $class; ?></p>

                                                 <h3>Usuario</h3>
                                                 <p><?php if($row['name']=="no_user") {echo "No Registrado";} else { echo $row['username'];} ?></p>

                                                 <h3>Código de Compra</h3>
                                                 <p class="break_word"><?php echo $row['buy_code'] ?></p>

                                                 <h3>Descuento Aplicado</h3>
                                                 <p><?php echo $row['discount'] ?> %</p>

                                                 <h3>Total de la Compra</h3>
                                                 <p><?php echo $row['total'] ?> €</p>

                                                 <h3>Fecha de la Compra</h3>
                                                 <p><?php echo $row['buy_day'] ?></p>

                                             </div>

                                             <div class="right_description">

                                                 <h3>Nombre Completo</h3>
                                                 <p><?php echo $row['full_name'] ?></p>

                                                 <h3>Dirección</h3>
                                                 <p><?php echo $row['address'] ?></p>

                                                 <h3>Código Postal</h3>
                                                 <p><?php echo $row['post_code'] ?></p>

                                                 <h3>Ciudad</h3>
                                                 <p><?php echo $row['city'] ?></p>

                                                 <h3>Teléfono</h3>
                                                 <p><?php echo $row['phone'] ?></p>

                                                 <h3>Email</h3>
                                                 <p class="break_word"><?php echo $row['email'] ?></p>

                                             </div>

                                             <?php

                                                $select_pdf = $db->query("select * from buy where id=" . $row['id_buy']);

                                                  foreach ($select_pdf as $row20) {
                                                    $full_name = $row20['full_name'];
                                                    $address = $row20['address'];
                                                    $post_code = $row20['post_code'];
                                                    $phone = $row20['phone'];
                                                    $email = $row20['email'];
                                                    $city = $row20['city'];
                                                    $token = $row20['buy_code'];
                                                    $token1 = $row20['buy_code'];
                                                    $title = $row20['title'];
                                                    $price = $row20['total'];
                                                    $date = $row20['buy_day'];
                                                    $num_ticket = $row20['quantity'];
                                                  }

                                                  $datetime = explode(" ", $date);

                                                  $date = explode("-", $datetime[0]);

                                                  $token = "Viajes Servicios Ocio - " . $token . ".pdf";

                                                include('../backend/pdf.php');


                                            ?>

                                             <form action="../backend/pdf_generate.php" method="post">
                                                <button class="btn btn-info">Obtener PDF</button>
                                                <input type="text" name="html" value="<?php echo $html; ?>" hidden>
                                                <input type="text" name="token" value="<?php echo $token; ?>" hidden>
                                            </form>

                                         </div>
                                    </div>
                                </div>
                            </div>

                        <?php

                            $cnt ++;
                    }


                     ?>
                </table>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
