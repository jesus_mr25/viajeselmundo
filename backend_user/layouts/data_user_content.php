<!--ESTA ES LA PAGINA DONDE ESTA LA PAGINA CON LOS DATOS DEL USUARIO REGISTRADO DEL PANEL DE CONTROL DEL USUARIO-->


<?php

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

    date_default_timezone_set("Europe/Madrid");
    $now = date("Y-m-d") . " " . date("G:i:s");


    if (isset($_POST['update'])) {

        //THIS OPERATION IS TO FLIP THE FIELD "foreign" FOR ADD IN THE DATABASE

        $date_born2 = explode("-", $_POST['date_born']);


        $result4 = $db->query("update user set name='" . $_POST['name'] . "',surname='" . $_POST['surname'] . "',dni='" . $_POST['dni'] . "',
        date_born='" . $date_born2[2] . "-" . $date_born2[1] . "-" . $date_born2[0] . "',home_address='" . $_POST['home_address'] . "',
        telephone='" . $_POST['telephone'] . "',post_code='" . $_POST['post_code'] . "',city='" . $_POST['city'] . "',country='" . $_POST['country'] . "',
        updated='" . $now . "' WHERE id='" . $_POST['id'] . "'");


        //TO REFRESH THE PAGE, AND ADMIN CAN SEE HOW CHANGE THE DATABASE WITHOUT PRESS F5

        if (!$result4) {
            ?>
                <style type="text/css">
                    .message_error_data_user {
                      display: inherit !important;
                    }
                </style>
            <?php
        }
        else {
        ?>
            <style type="text/css">
              .message_info_data_user {
                display: inherit !important;
              }
            </style>
        <?php
        }
    }
 ?>



<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS TICKETS DE DESCUENTO-->

 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Datos de <?php echo $user; ?>
          <small>Descripción de los datos personales de tu usuario</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

     	<!-- Your Page Content Here -->

      <div class="callout callout-success message_info_data_user" style="display: none;">
        <h4>¡Datos cambiados!</h4>

        <p>Cambio de datos realizado satisfactoriamente.</p>
      </div>

      <div class="callout callout-warning message_error_data_user" style="display: none;">
        <h4>¡Ups, algo ha salido mal!</h4>

        <p>El cambio de datos no se ha podido realizar, vuelva a intentarlo más tarde.</p>
      </div>

      	<div class="box">
            <div class="box-header">

            </div><!-- /.box-header -->

            <div class="box-body">

            <!--CARGO LAS OPCIONES DISPONIBLES DE ESTA TABLA EN EL COMBOBOX-->

                <?php
                    //REALIZO LA CONSULTA A BASE DE DATOS PARA OBTENER LOS ELEMENTOS QUE NECESITO. POR DEFECTO NO MUESTRO NINGUNO, PERO DESPUES VOY MOSTRANDO EL QUE ESTE SELECCIONADO EN EL COMBOBOX

                 ?>

                <!--le paso los valores del get para que cuando cambie el combobox cambie tambien en los inputs-->
                <form class="form-sport" method="post" action="data_user.php">

                  <?php

                    $select_user_data = $db->query("select * from user where username='" . $user . "'");

                    foreach ($select_user_data as $row31) {

                      $date_born = explode("-", $row31['date_born']);

                   ?>

                      <p style="font-size: 15px;">Nombre<input type="text" class="form-control" style="width: 100%;" name="name" value="<?php echo $row31['name'] ?>" required></p>
                      <p style="font-size: 15px;">Apellidos<input type="text" class="form-control" style="width: 100%;" name="surname" value="<?php echo $row31['surname'] ?>" required></p>
                      <p style="font-size: 15px;">DNI<input type="text" class="form-control" style="width: 100%;" name="dni" value="<?php echo $row31['dni'] ?>" required></p>
                      <p style="font-size: 15px;">Fecha Nacimiento<input type="text" class="form-control" style="width: 100%;" name="date_born" value="<?php echo $date_born[2] . "-" . $date_born[1] . "-" . $date_born[0]; ?>" required></p>
                      <p style="font-size: 15px;">Dirección<input type="text" class="form-control" style="width: 100%;" name="home_address" value="<?php echo $row31['home_address'] ?>" required></p>
                      <p style="font-size: 15px;">Email<input type="text" class="form-control" style="width: 100%;" name="address" value="<?php echo $row31['address'] ?>" disabled></p>
                      <p style="font-size: 15px;">Teléfono<input type="text" class="form-control" style="width: 100%;" name="telephone" value="<?php echo $row31['telephone'] ?>" required></p>
                      <p style="font-size: 15px;">Código Postal<input type="text" class="form-control" style="width: 100%;" name="post_code" value="<?php echo $row31['post_code'] ?>" required></p>
                      <p style="font-size: 15px;">Ciudad<input type="text" class="form-control" style="width: 100%;" name="city" value="<?php echo $row31['city'] ?>" required></p>
                      <p style="font-size: 15px;">País<input type="text" class="form-control" style="width: 100%;" name="country" value="<?php echo $row31['country'] ?>" required></p>

                    <?php

                    }

                     ?>


                    <p><input type="text" name="id" value="<?php echo $row31['id'] ?>" style="display: none;"></p>
                    <input class="btn btn-info" type="submit" style="background-color: #3c8dbc;" name="update" value="Actualizar Datos">
                </form>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
