<!--ESTA ES LA PAGINA DONDE ESTALA CABECERA DEL PANEL DE CONTROL-->


<?php
  //INICIO LA SESION

  session_start();

  //IMPORTO LAS FUNCIONES Y LA CONEXION A LA BASE DE DATOS

  include('../backend/conexionDB.php');
  include('functions.php');


  $user = $_SESSION['username'];

  //If there isn't session, the web will send to index of the web

  if (!isset($_SESSION['username'])) {
    header('location: ../index.php');
  }

  //If there is session, will check if that user is a admin

  else {
    $result = $db->query("select username from user where active=1 and username='" . $_SESSION['username'] . "'");

    if ($result->num_rows == 0) {
      header('location: ../index.php');
    }
  }

 ?>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->

<html lang="es">
  <head>
    <meta charset="utf-8">
    <title>Panel de Usuario</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../backend/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../backend/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="../backend/dist/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="../backend/bootstrap/css/style.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="../backend/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../backend/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="../backend/dist/js/app.min.js"></script>

    <!-- jQuery 2.2.0 -->
    <script src="//www.google-analytics.com/analytics.js" async=""></script>
    <script src="../backend/plugins/jQuery/jQuery-2.2.0.min.js"></script>
    <!-- Slimscroll -->
    <script src="../backend/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="../backend/plugins/fastclick/fastclick.js"></script>
    <!-- iCheck -->
    <script src="../backend/plugins/iCheck/icheck.min.js"></script>
    <?php

      if (basename($_SERVER['PHP_SELF']) == "tab_last_news.php") {
        ?>
          <script src="//cdn.ckeditor.com/4.5.9/full/ckeditor.js"></script>
          <script type="text/javascript">window.onload = function(){CKEDITOR.replace('full_ckeditor');}</script>
        <?php
      }
      else {
        ?>
          <script src="../backend/plugins/ckeditor/ckeditor.js"></script>
          <script type="text/javascript">window.onload = function(){CKEDITOR.replace('ckeditor');}</script>
        <?php
      }

     ?>


  </head>


  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="index.php" class="logo special">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b></b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">Panel de Control</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle special" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="../backend/dist/img/user-men-2-5121.png" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php get_name_admin($user, $db); ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="../backend/dist/img/user-men-2-5121.png" class="img-circle" alt="User Image">
                    <p>
                      <?php get_name_admin($user, $db); ?> - Usuario
                    </p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-right">
                      <a href="../index.php" class="btn btn-default btn-flat">Salir</a>
                    </div>
                    <div class="pull-left">
                      <a href="../logout.php" class="btn btn-default btn-flat">Cerrar Sesión</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
