<?php 

//AQUI ES DONDE VIENE EL USUARIO CUANDO CIERRA LA SESION Y DONDE AUTOMATICAMENTE SE LE REDIRIGE A LA PAGINA CORRESPONDIENTE

	session_start();

	if (!isset($_SESSION['username'])) {
		header('location: login.php');
	}
	else {
		session_unset();
		session_destroy(); 

		header('location: index.php');
	}

?>