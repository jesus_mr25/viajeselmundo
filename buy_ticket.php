<?php

//INICIO LA SESION, INCLUYO AAQUI LA CONEXION DE LA BASE DE DATOS Y LAS FUNCIONES, COMPRUEBO SI EXISTE VARIABLE GET, SINO COMPRUEBO SI HAY SESION USERNAME, EN CASO NEGATIVO ME REDIRECCIONA AL INDEX

    session_start();

    include('backend/conexionDB.php');
    include('functions.php');

    $validate = 0;

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

    date_default_timezone_set("Europe/Madrid");

    if ($_GET) {
      //SI HAY GET NO SE HACE NADA
    }
    elseif (isset($_SESSION['username'])) {
      //SI HAY SESION DE USUARIO NO HACE NADA
    }
    else {

      //EN CASO CONTRARIO DEVUELVE AL INDEX

    	header('location: index.php');
    }

    //SI HAY GET DE TOKEN_DICOUNT HACE LA CONSULTA PARA COMPROBAR EL TOKEN DEL DESCUENTO QUE SE ACABA DE INTRODUCIR

	if (isset($_GET['token_discount'])) {
		$result_token = $db->query("select discount from token_discount where token='" . $_GET['token_discount'] . "'");

    //SI LA CONSULTA NO DEVUELVE NADA SE MUESTRA UN MENSAJE DE ERROR PORQUE EL CODIGO NO ES VALIDO

	    if (mysqli_num_rows($result_token) == 0) {
	    	?>

	    		<style type="text/css">
	    			#error {
						display: inherit !important;
					}

					.btn-success {
						margin-top: 14px;
					}
	    		</style>

	    	<?php
	    }

      //SI LA CONSULTA DEVUELVE ALGO SE MUESTRA UN MENSAJE DE ACIERTO

	    else  {
	    	?>

	    		<style type="text/css">
	    			#good {
						display: inherit !important;
					}

					.btn-success {
						margin-top: 14px;
					}
	    		</style>

	    	<?php

        //OBTENGO EL VALOR EL PORCENTAJE DEL DESCUENTO

	    	foreach ($result_token as $row21) {
	    		$discount_token = $row21['discount'];
	    	}

        //SI HAY PRECIO DE GRUPO, MODIFICA EL PRECIO DE GRUPO SOBRESCRIBIENDO EL VALOR DEL GET Y APLICANDO EL DESCUENTO

	    	if (isset($_GET['type_price'])) {
	    		if ($_GET['type_price']=="on") {
	    			$group_disc = $_GET['price_group'] - (($discount_token / 100) * $_GET['price_group']);
            $validate = 1;
	    		}

          //EN CASO CONTRARIO MODIFICA EL PRECIO INDIVIDUAL SOBRESCRIBIENDO EL VALOR DEL GET Y APLICANDO EL DESCUENTO

	    		if ($_GET['type_price']=="") {
	    			$individual_disc = $_GET['price_individual'] - (($discount_token / 100) * $_GET['price_individual']);
            $validate = 1;
	    		}
	    	}
	    }
	}


?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Viajes El Mundo</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="backend/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <!-- Theme style -->
    <link rel="stylesheet" href="backend/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


	<style type="text/css">
		.token_discount {
			float: right;
			margin-right: 10px;
			height: 33px;
		}

		.validate_discount {
			background-color: rgb(253, 129, 39);
		}

		.validate_discount:hover {
			background-color: rgb(253, 129, 39);
		}

		#good {
			display: none;
		}

		#error {
			display: none;
		}
	</style>


  </head>
  <body>

      <?php


      //DE AQUI LLAMO A LA FUNCION PARA OBTENER EL NUMERO DE LA COMPRA PERO ANTES DE OBTENERLO, TENGO QUE COMPROBAR QUE NO EXISTA ESE CODIGO. SI EXISTE EL CODIGO VUELVE A REPETIRSE EL BUCLE. MIENTRAS SE CUMPLA ESTO NO SALDRA DEL BUCLE

      $free = 0;

      while ($free==0) {
        $token = getRandomCode();

        $result = $db->query("select buy_code from buy where buy_code='" . $token . "'");

        if (mysqli_num_rows($result) == 0) {
          $free = 1;
        }
      }


      //SI HAY SESION INICIADA MOSTRARA AL USUARIO TODOS SUS DATOS EXCEPTO QUE SEA UN ADMINISTRADOR QUE LE REDIRECCINARA AL INDEX

      if (isset($_SESSION['username'])) {


          $whoIs = $db->query("select id, name from admin where username='" . $_SESSION['username'] . "'");

            //SI LA CONSULTA DEVUELVE ALGUN VALOR CUMPLE LA CONDICION, MUESTRA UN MENSAJE AL ADMINISTRADOR Y ADEMAS, GUARDA EN UNA VARIABLE UN BOOLEANO DE SI LA CONSULTA HA DEVUELTO ALGUN VALOR O NO Y OTRA VARIABLE PARA REDIRECCIONAR AL INDEX AL ADMINISTRADOR

            if ($whoIs->num_rows > 0) {

                foreach ($whoIs as $row) {
                  $nameAdmin = $row['name'];
                }
              ?>

                <script type="text/javascript">

                  alert("Buenas <?php echo $nameAdmin; ?>, le recordamos que si desea comprar, debe iniciar sesión con un usuario sin derechos de administrador. Gracias.");
                  window.location.href='index.php';

                </script>

              <?php
            }
            ?>


            <div id="good" class="callout callout-success">
              <h4>¡ESTUPENDO!</h4>
              <p>El código promocional introducido es correcto, su descuento ya se ha aplicado.</p>
            </div>

            <div id="error" class="callout callout-danger">
              <h4>¡Ups, algo salió mal!</h4>
              <p>El código promocional introducido es erróneo o está en desuso, por favor vuelva a intentarlo.</p>
            </div>


            <!-- Main content -->
          <section class="invoice">
            <!-- title row -->
            <div class="row">
              <div class="col-xs-12">
                <h2 class="page-header">
                  <img src="images/logo.png" width="150px" height="auto">
                  <small class="pull-right"><?php echo date("d-m-Y") . "&nbsp&nbsp&nbsp&nbsp" . date("H:i"); ?></small>
                </h2>
              </div><!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                De
                <address>
                  <strong>Viajes El Munddo</strong><br>
                  Calle Salinas, 7<br>
                  29630, Benalmádena Costa, Málaga<br>
                  Teléfono: (+34) 640 649 280<br>
                  Email: viajeselmundo@hotmail.com
                </address>
              </div><!-- /.col -->
              <div class="col-sm-4 invoice-col">
                Para
                <address>
                <?php


                //OBTENGO LOS DATOS DEL USUARIO MEDIANTE LA CONSULTA DE ABAJO PARA LUEGO IMPRIMIRLO MEDIANTE EL FOREACH

                $select1 = $db->query("select name, surname, home_address, city, post_code, telephone, address from user where username='" . $_SESSION['username'] . "'");

                foreach ($select1 as $row) {
                  echo "<strong>". $row['name'] . " " . $row['surname'] . "</strong><br>
                        ". $row['home_address'] . "<br>
                        ". $row['post_code'] . " " . $row['city'] . "<br>
                        Teléfono: (+34) ". $row['telephone'] . "<br>
                        Email: ". $row['address'];
                }


                 ?>
                </address>
              </div><!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <b>Número Factura: <?php echo $token; ?></b><br>
                <br>
                <b>Pago realizado:</b> <?php echo date("d-m-Y"); ?><br>
              </div><!-- /.col -->
            </div><!-- /.row -->

            <!-- Table row -->
            <div class="row">
              <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Cantidad</th>
                      <th>Producto</th>
                      <th>Precio Unitario</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>

                      <!--A PARTIR DEL GET COJO LOS DATOS QUE RECIBO, PARA QUE SE MUESTREN EN EL TICKET-->

                      <td><?php if (isset($_GET['num_ticket'])) {echo $_GET['num_ticket'];} ?></td>
                      <td><?php if (isset($_GET['title'])) {echo $_GET['title'];} ?></td>
                      <td><?php
                        if ($validate==1) {
                          if ($_GET['type_price']=="on") {echo $group_disc;} else {echo $individual_disc;}
                        }
                        else {
                          if ($_GET['type_price']=="on") {echo $_GET['price_group'];} else {echo $_GET['price_individual'];}
                        }
                      ?>€</td>
                    </tr>
                  </tbody>
                </table>
              </div><!-- /.col -->
            </div><!-- /.row -->

            <?php

            //REALIZO LOS CALCULOS PERTIENTES PARA EL CALCULO DEL PRECIO TOTAL EN FUNCION DEL NUMERO DE TICKETS Y SU PRECIO
            //SI POR EL GET HE RECIBIDO LA VARIABLE TYPE_PRICE (QUE APARECEE EN ON PORQUE EL CHECKBOX FUE CHECKEADO) SIGNIFICA QUE HAY PRECIO DE GRUPO, POR LO QUE PARA EL CALCULO USO EL PPRECIO DE GRUPO

              if (isset($_GET['type_price'])) {
                $totalPrice = $_GET['price_group'] * $_GET['num_ticket'];
                $price = $_GET['price_group'];
              }

              //EN CASO DE QUE NO ESTE LA VARIABLE CONTEMPLADA EN EL GET, HARA LOS CALCULOS CON EL PRECIO INDIVIDUAL

              else {
                $totalPrice = $_GET['price_individual'] * $_GET['num_ticket'];
                $price = $_GET['price_individual'];
              }
            ?>

            <div class="row">
              <!-- accepted payments column -->
              <div class="col-xs-6">
                <p class="lead">Métodos de Pago:</p>
                <img src="backend/dist/img/credit/visa.png" alt="Visa">
                <img src="backend/dist/img/credit/mastercard.png" alt="Mastercard">
                <img src="backend/dist/img/credit/american-express.png" alt="American Express">
                <img src="backend/dist/img/credit/paypal2.png" alt="Paypal">
                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                  Para proteger los datos, los datos de pago que introduzcas aquí se eliminan de los campos una vez que abandones esta página. os tomamos muy en serio la protección de tus datos. La información sobre este tema puede encontrarse en nuestra <a href="term_user.php">Política de Privacidad</a>.
                  <br>La posesión de este documento no otorga la validez del mismo, hasta que no se haya realizado su correspondiente pago.
                </p>

              </div><!-- /.col -->
              <div class="col-xs-6">
                <p class="lead">Cantidad a Pagar</p>
                <div class="table-responsive">
                  <table class="table">
                    <tr>
                      <th style="width:50%">Precio sin IVA:</th>
                      <?php

                      //BTENGO EL VALOR DEL PRODUCTO SIN EL IVA Y EL TOTAL DEL IVA APLICADO ADEMAS LE APLICO LA FUNCION NUMBER_FORMT PARA QUE SE VEA EN FORMATO DECIMAL

                        $totalSinIVA = $totalPrice / 1.21;
                        $iva = $totalPrice - $totalSinIVA;

                       ?>
                      <td><?php echo number_format($totalSinIVA, 2, '.', ''); ?>€</td>
                    </tr>
                    <tr>
                      <th>IVA (21%)</th>
                      <td><?php echo number_format($iva, 2, '.', ''); ?>€</td>
                    </tr>
                    <tr>
                      <th>Total:</th>
                      <td><?php echo $totalPrice; ?>€</td>
                    </tr>
                  </table>
                </div>
              </div><!-- /.col -->
            </div><!-- /.row -->

            <!--INCLUYO LA VARIABLE DONDE ESTA INTRODUCIDO EL CODIGO HTML QUE USARA MPDF-->

             <?php include('pdf.php'); ?>

            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-xs-12">
                <a target="_blank" class="btn btn-default" onclick="window.print()" style="position: absolute; bottom: 0; display: <?php echo $none; ?>"><i class="fa fa-print"></i> Imprimir</a>

                <!--
                    BUSSINESS:        CORREO DEL VENDEDOR DE PAYPAL
                    CURRENCY_CODE:    TIPO DE MONEDA
                    ITEM-NAME:        NOMBRE DEL PRODUCTO
                    AMOUNT:           PRECIO DE UN PRODUCTO
                    QUANTITY:         CANTIDAD DE PRODUCTOS
                    RETURN:           DONDE REENVIARA AL FINALIZAR LA COMPRA
                    CANCEL_RETURN:    DONDE REDIRECCIONARA SI SE CANCELA LA COMPRA
                    NOTIFY_URL:       PAGINA HACIA DONDE IRA LA NOTIFICACION
                    CUSTOM:           VARIABLE CUSTOMIZABLE A LA QUE YO LE PASO LO QUE QUIERA (EXPLICADO CADA VARIABLE EN EL ARCHIVO BUY_ACCEPT.PHP)
                    -->

                    <?php

                      $full_name = $row['name'] . " " . $row['surname'];

                     ?>

                <form name="_xclick" action="https://www.paypal.com/es/cgi-bin/webscr" method="post">
                  <input type="hidden" name="cmd" value="_xclick">
                  <input type="hidden" name="business" value="viajeselmundo1@gmail.com">
                  <input type="hidden" name="currency_code" value="EUR">
                  <input type="hidden" name="item_name" value="<?php echo $_GET['title']; ?>">
                  <input type="hidden" name="amount" value="<?php echo $price; ?>">
                  <input type="hidden" name="quantity" value="<?php echo $_GET['num_ticket']; ?>">
                  <input type="hidden" name="return" value="http://viajeselmundo.esy.es/">
                  <input type="hidden" name="cancel_return" value="http://viajeselmundo.esy.es/">
                  <input type="hidden" name="notify_url" value="http://viajeselmundo.esy.es/buy_accept.php">
                  <input type="hidden" name="custom" value="<?php echo $_GET['table'] . "," . $_GET['discount'] . "," . $_GET['id_user'] . "," . $token . "," . $_GET['full_name'] . "," . $_GET['address'] . "," . $_GET['post_code'] . "," . $_GET['city'] . "," . $_GET['phone'] . "," . $_GET['email'] . "," . $_GET['id_product_generic'] . "," . $_GET['title'] . "," . $_GET['num_ticket']; ?>">
                  <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Finalizar Compra</button>
                </form>

                <!--EN LA VARIABLE HTML PASO EL CODIGO PARA CREAR EL PDF Y ADEMAS LE PASO EL TOKEN PARA MODIFICR EL NOMBRE DEL ARCHIVO DEL PDF AL CREARSE-->

                <form action="pdf_generate.php" method="post">
                  <input class="pdf" type="text" name="html_no_user" value="<?php echo $html; ?>" hidden>
                  <input class="pdf" type="text" name="token" value="<?php echo $token; ?>" hidden>
                  <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generar Factura</button>
                </form>
                <form action="buy_ticket.php" method="get">
                  	<input class="token_discount" type="text" name="token_discount" placeholder="Descuento Promocional" required>
                  	<input type="text" name="table" value="<?php echo $_GET['table']; ?>" hidden>
                  	<input type="text" name="discount" value="<?php echo $_GET['discount']; ?>" hidden>
                  	<input type="text" name="id_user" value="<?php echo $_GET['id_user']; ?>" hidden>
                  	<input type="text" name="full_name" value="<?php echo $_GET['full_name']; ?>" hidden>
                  	<input type="text" name="address" value="<?php echo $_GET['address']; ?>" hidden>
                  	<input type="text" name="post_code" value="<?php echo $_GET['post_code']; ?>" hidden>
                  	<input type="text" name="city" value="<?php echo $_GET['city']; ?>" hidden>
                  	<input type="text" name="phone" value="<?php echo $_GET['phone']; ?>" hidden>
                  	<input type="text" name="email" value="<?php echo $_GET['email']; ?>" hidden>
                  	<input type="text" name="id_product_generic" value="<?php echo $_GET['id_product_generic']; ?>" hidden>
                  	<input type="text" name="title" value="<?php echo $_GET['title']; ?>" hidden>
                  	<input type="text" name="num_ticket" value="<?php echo $_GET['num_ticket']; ?>" hidden>
                  	<input type="text" name="type_price" value="<?php echo $_GET['type_price']; ?>" hidden>
                  	<input type="text" name="price_individual" value="<?php echo $_GET['price_individual']; ?>" hidden>
                  	<input type="text" name="price_group" value="<?php echo $_GET['price_group']; ?>" hidden>
                	<button class="btn btn-primary pull-right validate_discount" style="margin-right: 5px;">Validar Código</button>
                </form>
              </div>
            </div>

          </section><!-- /.content -->
          <div class="clearfix"></div>

        <?php
}



















          //SI NO HAY NINGUNA SESION INICIADA PEDIRA AL USUARIO UNA SERIE DE DATOS QUE DEBE RELLENAR EN LA PANTALLA ANTERIROR (FORM_DATA_BUY.PHP), LOS CUALES APARECEN DEBAJO DE LA COLUMNA "PARA"
          //TODO LO QUE APARECE A COTINUACION ES LO MISMO QUE HAY ARRIBA PERO CON EL CAMBIO DE QUE COGE LAS VARIABLES DEL GET QUE RECIBE DEL ARCHIVO "FORM_DATA_BUY.PPH"


      else {

        ?>

        	<div id="good" class="callout callout-success">
              <h4>¡ESTUPENDO!</h4>
              <p>El código promocional introducido es correcto, su descuento ya se ha aplicado.</p>
            </div>

            <div id="error" class="callout callout-danger">
              <h4>¡Ups, algo salió mal!</h4>
              <p>El código promocional introducido es erróneo o está en desuso, por favor vuelva a intentarlo.</p>
            </div>


            <!-- Main content -->
          <section class="invoice">
            <!-- title row -->
            <div class="row">
              <div class="col-xs-12">
                <h2 class="page-header">
                  <img src="images/logo.png" width="150px" height="auto">
                  <small class="pull-right"><?php echo date("d-m-Y") . "&nbsp&nbsp&nbsp&nbsp" . date("H:i"); ?></small>
                </h2>
              </div><!-- /.col -->
            </div>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-sm-4 invoice-col">
                De
                <address>
                  <strong>Viajes El Munddo</strong><br>
                  Calle Salinas, 7<br>
                  29630, Benalmádena Costa, Málaga<br>
                  Teléfono: (+34) 640 649 280<br>
                  Email: viajeselmundo@hotmail.com
                </address>
              </div><!-- /.col -->
              <div class="col-sm-4 invoice-col">
                Para
                <address>
                    <strong><?php echo $_GET['full_name']; ?></strong><br>
                          <?php echo $_GET['address']; ?><br>
                          <?php echo $_GET['post_code'] . " " . $_GET['city']; ?><br>
                          Teléfono: <?php echo $_GET['phone']; ?><br>
                          Email: <?php echo $_GET['email']; ?>
                </address>
              </div><!-- /.col -->
              <div class="col-sm-4 invoice-col">
                <b>Número Factura: <?php echo $token; ?></b><br>
                <br>
                <b>Pago realizado:</b> <?php echo date("d-m-Y"); ?><br>
              </div><!-- /.col -->
            </div><!-- /.row -->

            <!-- Table row -->
            <div class="row">
              <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Cantidad <?php echo $validate; ?></th>
                      <th>Producto</th>
                      <th>Precio Unitario</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>

                      <!--A PARTIR DEL GET COJO LOS DATOS QUE RECIBO, PARA QUE SE MUESTREN EN EL TICKET-->

                      <td><?php if (isset($_GET['num_ticket'])) {echo $_GET['num_ticket'];} ?></td>
                      <td><?php if (isset($_GET['title'])) {echo $_GET['title'];} ?></td>
                      <td><?php
                        if (isset($_GET['token_discount'])) {
                          if ($_GET['type_price']=="on") {echo $_GET['price_group'] - (($discount_token / 100) * $_GET['price_group']);} else {echo $_GET['price_individual'] - (($discount_token / 100) * $_GET['price_individual']);}
                        }
                        else {
                          if ($_GET['type_price']=="on") {echo $_GET['price_group'];} else {echo $_GET['price_individual'];}
                        }
                      ?>€</td>
                    </tr>
                  </tbody>
                </table>
              </div><!-- /.col -->
            </div><!-- /.row -->


            <?php

            if (isset($_GET['token_discount'])) {
              if ($_GET['type_price']=="on") {
                $totalPrice = ($_GET['price_group'] - (($discount_token / 100) * $_GET['price_group'])) * $_GET['num_ticket'];
                $price = $_GET['price_group'] - (($discount_token / 100) * $_GET['price_group']);
              }
              else {
                $totalPrice = ($_GET['price_individual'] - (($discount_token / 100) * $_GET['price_individual'])) * $_GET['num_ticket'];
                $price = $_GET['price_individual'] - (($discount_token / 100) * $_GET['price_individual']);
              }
            }
            else {
              if ($_GET['type_price']=="on") {
                $totalPrice = $_GET['price_group'] * $_GET['num_ticket'];
                $price = $_GET['price_group'];
              }
              else {
                $totalPrice = $_GET['price_individual'] * $_GET['num_ticket'];
                $price = $_GET['price_individual'];
              }
            }



            ?>


            <div class="row">
              <!-- accepted payments column -->
              <div class="col-xs-6">
                <p class="lead">Métodos de Pago:</p>
                <img src="backend/dist/img/credit/visa.png" alt="Visa">
                <img src="backend/dist/img/credit/mastercard.png" alt="Mastercard">
                <img src="backend/dist/img/credit/american-express.png" alt="American Express">
                <img src="backend/dist/img/credit/paypal2.png" alt="Paypal">
                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                  Para proteger los datos, los datos de pago que introduzcas aquí se eliminan de los campos una vez que abandones esta página. os tomamos muy en serio la protección de tus datos. La información sobre este tema puede encontrarse en nuestra <a href="term_user.php">Política de Privacidad</a>.
                </p>
                <p>La posesión de este documento no otorga la validez del mismo, hasta que no se haya realizado su correspondiente pago.</p>
              </div><!-- /.col -->
              <div class="col-xs-6">
                <p class="lead">Cantidad a Pagar</p>
                <div class="table-responsive">
                  <table class="table">
                    <tr>
                      <th style="width:50%">Precio sin IVA:</th>
                      <?php

                        $totalSinIVA = $totalPrice / 1.21;
                        $iva = $totalPrice - $totalSinIVA;

                       ?>
                      <td><?php echo number_format($totalSinIVA, 2, '.', ''); ?>€</td>
                    </tr>
                    <tr>
                      <th>IVA (21%)</th>
                      <td><?php echo number_format($iva, 2, '.', ''); ?>€</td>
                    </tr>
                    <tr>
                      <th>Total:</th>
                      <td><?php if (isset($_GET['token_discount'])) {
                        if ($_GET['type_price']=="on") {echo $_GET['price_group'] - (($discount_token / 100) * $_GET['price_group']);} else {echo $_GET['price_individual'] - (($discount_token / 100) * $_GET['price_individual']);}
                      }
                      else {
                        if ($_GET['type_price']=="on") {echo $_GET['price_group'];} else {echo $_GET['price_individual'];}
                      } ?>€</td>
                    </tr>
                  </table>
                </div>
              </div><!-- /.col -->
            </div><!-- /.row -->

            <?php include('pdf.php'); ?>

            <!-- this row will not appear when printing -->
            <div class="row no-print">
              <div class="col-xs-12">
                <a target="_blank" class="btn btn-default" onclick="window.print()" style="position: absolute; bottom: 0; display: <?php echo $none; ?>"><i class="fa fa-print"></i> Imprimir</a>

                <form name="_xclick" action="https://www.paypal.com/es/cgi-bin/webscr" method="post">
                  <input type="hidden" name="cmd" value="_xclick">
                  <input type="hidden" name="business" value="viajeselmundo1@gmail.com">
                  <input type="hidden" name="currency_code" value="EUR">
                  <input type="hidden" name="item_name" value="<?php echo $_GET['title']; ?>">
                  <input type="hidden" name="amount" value="<?php echo $price; ?>">
                  <input type="hidden" name="quantity" value="<?php echo $_GET['num_ticket']; ?>">
                  <input type="hidden" name="return" value="http://viajeselmundo.esy.es/">
                  <input type="hidden" name="cancel_return" value="http://viajeselmundo.esy.es/">
                  <input type="hidden" name="notify_url" value="http://viajeselmundo.esy.es/buy_accept.php">
                  <input type="hidden" name="custom" value="<?php echo $_GET['table'] . "," . $_GET['discount'] . "," . $_GET['id_user'] . "," . $token . "," . $_GET['full_name'] . "," . $_GET['address'] . "," . $_GET['post_code'] . "," . $_GET['city'] . "," . $_GET['phone'] . "," . $_GET['email'] . "," . $_GET['id_product_generic'] . "," . $_GET['title'] . "," . $_GET['num_ticket']; ?>">
                  <button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Finalizar Compra</button>
                </form>
                <form action="pdf_generate.php" method="post">
                  <input class="pdf" type="text" name="html_no_user" value="<?php echo $html; ?>" hidden>
                  <input class="pdf" type="text" name="token" value="<?php echo $token; ?>" hidden>
                  <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generar Factura</button>
                </form>
                <form action="buy_ticket.php" method="get">
                  	<input class="token_discount" type="text" name="token_discount" placeholder="Descuento Promocional" required>
                  	<input type="text" name="table" value="<?php echo $_GET['table']; ?>" hidden>
                  	<input type="text" name="discount" value="<?php echo $_GET['discount']; ?>" hidden>
                  	<input type="text" name="id_user" value="<?php echo $_GET['id_user']; ?>" hidden>
                  	<input type="text" name="full_name" value="<?php echo $_GET['full_name']; ?>" hidden>
                  	<input type="text" name="address" value="<?php echo $_GET['address']; ?>" hidden>
                  	<input type="text" name="post_code" value="<?php echo $_GET['post_code']; ?>" hidden>
                  	<input type="text" name="city" value="<?php echo $_GET['city']; ?>" hidden>
                  	<input type="text" name="phone" value="<?php echo $_GET['phone']; ?>" hidden>
                  	<input type="text" name="email" value="<?php echo $_GET['email']; ?>" hidden>
                  	<input type="text" name="id_product_generic" value="<?php echo $_GET['id_product_generic']; ?>" hidden>
                  	<input type="text" name="title" value="<?php echo $_GET['title']; ?>" hidden>
                  	<input type="text" name="num_ticket" value="<?php echo $_GET['num_ticket']; ?>" hidden>
                  	<input type="text" name="type_price" value="<?php echo $_GET['type_price']; ?>" hidden>
                  	<input type="text" name="price_individual" value="<?php echo $_GET['price_individual']; ?>" hidden>
                  	<input type="text" name="price_group" value="<?php echo $_GET['price_group']; ?>" hidden>
                	<button class="btn btn-primary pull-right validate_discount" style="margin-right: 5px;">Validar Código</button>
                </form>
              </div>
            </div>

          </section><!-- /.content -->
          <div class="clearfix"></div>



<?php
  }
 ?>


  </body>
</html>

<?php

  $db->close();

 ?>
