<!================= HEADER ====================>

	<?php include('layouts/header.php'); ?>
	
	<div class="main">

		<!================ CONTACT ===================>

		<?php include('layouts/contact_content.php'); ?>
	</div>

	<!================ BLUE FOOTER ===================>

	<?php include('layouts/blue_footer.php'); ?>

	<!================ SOCIAL FOOTER ===================>

	<?php include('layouts/social_footer.php'); ?>

	<!================ BUTTON FOOTER ===================>

	<?php include('layouts/button_bottom.php'); ?>

</body>
</html>

<?php 

	$db->close();

 ?>