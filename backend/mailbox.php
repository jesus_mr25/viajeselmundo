<?php


  include("layouts/header.php");
  include("layouts/left_column.php");
  include("layouts/top_emails.php");


//DIVIDO EL NUMERO TOTAL DE CORREOS PARA MOSTRARLOS DE 10 EN 10 POSTERIORMENTE

$result = $num_emails/10;

$cont = 1;
$cont2 = 1;
$color = "";

//CON EL BUCLE SACO TODOS LOS MENSAJE QUE TENGA EN LA BANDEJA DE ENTRADA

foreach($emails as $email_number) {
    $overview = imap_fetch_overview($inbox,$email_number,0);
    $from = $overview[0]->from;
    $subject = $overview[0]->subject;
    $date = $overview[0]->date;
    $read = $overview[0]->seen;
    $msgno = $overview[0]->msgno;
    $num = imap_num_msg($inbox);
    $color = "";


    //SI EL MENSAJE ESTA MARCADO COMO NO LEIDO, SE MOSTRARA EN COLOR VERDE

    if($read == 0) {
        $color = "green";
    }

    $percents = (100*$cont)/$num_emails;

    echo "<div class='progress progress-striped active progress_" . $cont . "' hidden style='margin-right: 5px; margin-left: 5px;'>
      <div class='progress-bar' role='progressbar'
           aria-valuenow='" . $percents . "' aria-valuemin='0' aria-valuemax='100'
           style='width: " . $percents . "%'>
      </div>
    </div>";

    echo "<script type='text/javascript'>
            $('.progress').prop('hidden', true);
            $('.progress_" . $cont . "').prop('hidden', false);
          </script>";


    //PARA QUE DESDE EL PRINCIPIO NO SE MUESTREN TODOS LOS CORREOS, MIENTRAS $CONT2 SEA 1 SOLO SE IRAN CARGANDO TODOS LOS CORREOS, PERO CUANDO $CONT LLEGUE A 10, $CONT2 SE INCREMENTARA EN 1 POR LO QUE SOLO SE MOSTRARAN LOS 10 PRIMEROS CORREOS, TODOS LOS DEMAS SE IRAN CARGANDO PERO ESTARAN OCULTOS. UNA VEZ QUE SE HAYAN CARGADO TODOS LOS CORREOS, AL PINCHAR SOBRE LAS FLECHAS DERECHA E IZQUIERDA, SE IRAN PAGINANDO DE 10 EN 10

    if ($cont2==1) {
      echo "<tr class='td" . $cont2 . "'>
            <td>
              <input class='check' name='checkboxs' type='checkbox' value='" . $msgno . "'>
            </td>
            <td class='mailbox-name'>
              <a href='read-mail.php?date=" . date("d/m/Y G:i:s", strtotime($date)) . "&msgno=" . $msgno . "&box=" . $box . "&from=" . str_replace("'", "", $from) . "&subject='" . $subject . "'' style='color: " . $color . "; text-decoration: none; cursor: pointer;'><b>" . iconv_mime_decode($from) . "</b></a>
            </td>
            <td class='mailbox-subject'>" . iconv_mime_decode($subject) . "
          </td>
          <td class='mailbox-date'>" . date("d/m/Y", strtotime($date)) . "</td>
          </tr>
          ";
    }
    else {
      echo "<tr class='td" . $cont2 . "' hidden>
            <td>
              <input class='check' name='checkboxs' type='checkbox' value='" . $msgno . "'>
            </td>
            <td class='mailbox-name'>
              <a href='read-mail.php?date=" . date("d/m/Y G:i:s", strtotime($date)) . "&msgno=" . $msgno . "&box=" . $box . "&from=" . str_replace("'", "", $from) . "&subject=" . $subject . "' style='color: " . $color . "; text-decoration: none; cursor: pointer;'><b>" . iconv_mime_decode($from) . "</b></a>
            </td>
            <td class='mailbox-subject'>" . iconv_mime_decode($subject) . "
          </td>
          <td class='mailbox-date'>" . date("d/m/Y", strtotime($date)) . "</td>
          </tr>";
    }

    if ($cont%10==0) {
        $cont2++;
    }

    $cont++;
}


  echo "<script type='text/javascript'>
          $('.progress').prop('hidden', true);
          $('.next').prop('disabled', false);
          $('.prev').prop('disabled', false);
          $('.button-reload').prop('disabled', false);
          $('.button-delete').prop('disabled', false);
          $('.checkbox-toggle').prop('disabled', false);
          $('.dropdown-toggle').prop('disabled', false);
        </script>";


  imap_close($inbox);


    include("layouts/bottom_emails.php");
    include("layouts/footer.php");

?>

<script type="text/javascript">
  $(document).ready(function(){
    $('.button-delete').click(function() {
      var quantity = $('.check').val();

      for (var i=1; i<=quantity; i++) {
          alert($('input:checkbox[name=checkboxs]:checked').val());
      }

    });
  });
</script>


 <!---->

 <script type="text/javascript">
     $(document).ready(function(){
        var i = 1;

        if (i == <?php echo $cont2 ?>) {                      /*SI EL VALOR DE I ES IGUAL QUE EL ULTIMO GRUPO DE DIEZ DE LOS CORREOS, QUE SERIA $CONT2, PONE EL BOTON SIGUIENTE EN DISABLE*/
            $('.next').prop('disabled', true);
        }

        $('.td1').prop('hidden', false);                          /*LOS 10 PRIMEROS TD ESTAN SIN OCULTAR*/
        $('.prev').prop('disabled', true);                        /*EL BOTON ANTERIOR ESTA DESACTIVADO*/

        $('.next').click(function(){                              /*AL HACER CLICK EN EL BOTON SIGUIENTE, OBTENGO EL VALOR DE I Y LE VOY SUMANDO 1 CADA VEZ QUE PINCHE EN SIGUIENTE*/
            $('.td' + i).prop('hidden', true);

            i++;

            $('.td' + i).prop('hidden', false);                   /*COMO SIEMPRE ESTAN OCULTOS LOS MENSAJES DE LAS PAGINAS SIGUIENTES, VOY OCULTANDO EL ANTERIOR Y MOSTRANDO EL SIGUIENTE*/
            $('.prev').prop('disabled', false);                   /*EN EL MOMENTO EN EL QUE PULSE SIGUIENTE UNA VEZ EL BOTON ANTERIOR SE ACTIVA*/

            if (i == <?php echo $cont2 ?>) {                      /*SI EL VALOR DE I ES IGUAL QUE EL ULTIMO GRUPO DE DIEZ DE LOS CORREOS, QUE SERIA $CONT2, PONE EL BOTON SIGUIENTE EN DISABLE*/
                $('.next').prop('disabled', true);
            }
        });


        $('.prev').click(function(){/*EL BOTON ANTERIOR FUNCIONA IGUAL QUE SIGUIENTE, PERO EN LA CONDICION EN VEZ DE SER EL NUMER MÁXIMO PAGINAS SERIA SOLO EN LA 1 CUANDO SE PONE EL BOTON CON LA PROPIEDAD DISABLE*/
            $('.td' + i).prop('hidden', true);

            i--;

            $('.td' + i).prop('hidden', false);
            $('.next').prop('disabled', false);

            if (i == 1) {
                $('.prev').prop('disabled', true);
            }
        });

     });
 </script>
