<?php

//GUARDO TODO ESTE CODIGO EN UNA VARIABLE PARA QUE LUEGO LO INTERPRETE EL PLUGIN DE MPDF Y LO GENERE


$totalSinIVA = $price / 1.21;

$iva = $price - $totalSinIVA;

$total = $num_ticket * $price;


  $html = "<!DOCTYPE html>
  <html lang='es'>
    <head>
      <meta charset='utf-8'>
      <meta http-equiv='X-UA-Compatible' content='IE=edge'>
      <title>Viajes Servicios Ocio</title>
      <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
      <link rel='stylesheet' href='bootstrap/css/bootstrap.min.css'>
      <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'>
      <link rel='stylesheet' href='dist/css/AdminLTE.min.css'>
    </head>
    <body>

        <!-- Main content -->
            <section class='invoice'>
              <!-- title row -->
              <div class='row'>
                <div class='col-xs-12'>
                  <h2 class='page-header'>
                    <img src='../images/logo.png' width='150px' height='auto'>
                    <small class='pull-right'>". $date[2] . "-" . $date[1] . "-" . $date[0] . " &nbsp&nbsp&nbsp&nbsp " . $datetime[1] ." </small>
                  </h2>
                </div><!-- /.col -->
              </div>
              <!-- info row -->
              <div class='row invoice-info'>
                <div class='col-sm-4 invoice-col'>
                  De
                  <address>
                    <strong>Viajes El Mundo</strong><br>
                    Calle Salinas, 7<br>
                    29630, Benalmádena Costa, Málaga<br>
                    Teléfono: (+34) 640 649 280<br>
                    Email: viajeselmundo@hotmail.com
                  </address>
                </div><!-- /.col -->
                <div class='col-sm-4 invoice-col'>
                  Para
                  <address>
                      <strong>" . $full_name . "</strong><br>
                            " . $address . "<br>
                            " . $post_code . " " . $city . "<br>
                            Teléfono: " . $phone . "<br>
                            Email: " . $email . "



                  </address>
                </div><!-- /.col -->
                <div class='col-sm-4 invoice-col'>
                  <b>Número Factura: " . $token1 . "</b><br>
                  <br>
                  <b>Pago realizado:</b> " . $datetime[1] . " " . $date[2] . "-" . $date[1] . "-" . $date[0] .  "<br>
                </div><!-- /.col -->
              </div><!-- /.row -->

              <!-- Table row -->
              <div class='row'>
                <div class='col-xs-12 table-responsive'>
                  <table class='table table-striped'>
                    <thead>
                      <tr>
                        <th>Cantidad</th>
                        <th>Producto</th>
                        <th>Precio Unitario</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>" . $num_ticket . "</td>
                        <td>" . $title . "</td>
                        <td>" . $price . "€</td>
                      </tr>
                    </tbody>
                  </table>
                </div><!-- /.col -->
              </div><!-- /.row -->



              <div class='row'>
                <!-- accepted payments column -->
                <div class='col-xs-6'>
                  <p class='lead'>Métodos de Pago:</p>
                  <img src='dist/img/credit/visa.png' alt='Visa'>
                  <img src='dist/img/credit/mastercard.png' alt='Mastercard'>
                  <img src='dist/img/credit/american-express.png' alt='American Express'>
                  <img src='dist/img/credit/paypal2.png' alt='Paypal'>
                  <p class='text-muted well well-sm no-shadow' style='margin-top: 10px;'>
                    Para proteger los datos, los datos de pago que introduzcas aquí se eliminan de los campos una vez que abandones esta página. os tomamos muy en serio la protección de tus datos. La información sobre este tema puede encontrarse en nuestra <a href='term_user.php'>Política de Privacidad</a>.
                    <br>La posesión de este documento no otorga la validez del mismo, hasta que no se haya realizado su correspondiente pago.
                  </p>
                </div><!-- /.col -->
                <div class='col-xs-6'>
                  <p class='lead'>Cantidad a Pagar</p>
                  <div class='table-responsive'>
                    <table class='table'>
                      <tr>
                        <th style='width:50%'>Precio sin IVA:</th>
                        <?php

                         ?>
                        <td>" . number_format($totalSinIVA, 2, '.', '') . "€</td>
                      </tr>
                      <tr>
                        <th>IVA (21%)</th>
                        <td>" . number_format($iva, 2, '.', '') . "€</td>
                      </tr>
                      <tr>
                        <th>Total:</th>
                        <td>" . $total  . "€</td>
                      </tr>
                    </table>
                  </div>
                </div><!-- /.col -->
              </div><!-- /.row -->

            </section><!-- /.content -->
            <div class='clearfix'></div>

    </body>
  </html>";


 ?>
