<?php

if (isset($_GET['excel'])) {

  include('conexionDB.php');

  header('Content-type: application/vnd.ms-excel;charset=utf-8');
  header("Content-Disposition: attachment; filename=Suscriptores.xls");
  header("Pragma: no-cache");
  header("Expires: 0");

  $query4 = $db->query("select email, date_suscriber from subscriber");

  if (mysqli_num_rows($query4) > 0) {
      echo "<table><tr><td>Email</td><td>Fecha Suscripcion</td></tr>";

      foreach ($query4 as $row26) {
        echo "<tr><td>" . $row26['email'] . "</td><td>" . $row26['date_suscriber'] . "</td></tr>";
      }

      echo "</table>";
  }

}
else {
  ?>
    <script type="text/javascript">
      window.location.href="index.php";
    </script>
  <?php
}

 ?>
