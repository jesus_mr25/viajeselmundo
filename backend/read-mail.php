<?php
header ('Content-type: text/html; charset=utf-8');
if (!isset($_GET['msgno'])) {
  ?>
    <script type="text/javascript">
      window.location.href='index.php';
    </script>
  <?php
}


  include("layouts/header.php");
  include("layouts/left_column.php");

?>

<div style="min-height: 946px;" class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Leer Mensaje
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <a href="new_message.php" class="btn btn-primary btn-block margin-bottom">Redactar Mensaje</a>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Carpetas</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li class="mailbox"><a href="mailbox.php"><i class="fa fa-inbox"></i> Bandeja de entrada</a></li>
                <li class="sent"><a href="sent.php"><i class="fa fa-envelope-o"></i> Enviados</a></li>
                <li class="drafts"><a href="draft.php"><i class="fa fa-file-text-o"></i> Borradores </a></li>
                <li class="junk"><a href="junk.php"><i class="fa fa-filter"></i> Junk </a>
                </li>
                <li class="trash"><a href="trash.php"><i class="fa fa-trash-o"></i> Eliminados</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->

          <?php

            $msgno = $_GET['msgno'];
            $box = $_GET['box'];
            $date = $_GET['date'];
            $from = $_GET['from'];
            $subject = $_GET['subject'];

            $hostname = '{imap.gmail.com:993/imap/ssl}' . $box;
            $username = 'viajeselmundo1@gmail.com';
            $password = 'qwertyuiop1234567890..';
            $inbox = imap_open($hostname,$username,$password);

           ?>

        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3><?php echo iconv_mime_decode($subject); ?></h3>
                <br>
                <h5>De: <?php echo iconv_mime_decode($from); ?>
                  <span class="mailbox-read-time pull-right"><?php echo $date; ?></span></h5>
              </div>
              <!-- /.mailbox-read-info -->
              <div class="mailbox-read-message">
                <?php echo imap_qprint(imap_fetchbody($inbox, $msgno, 2)); ?>
              </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
              <div class="pull-right">
              </div>
              <button type="button" class="btn btn-default"><i class="fa fa-trash-o"></i> Borrar Mensaje</button>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.3
    </div>
    <strong>Copyright © 2016 Jesús Martín Ruiz.</strong> Todos los derechos reservados.
  </footer>
  </div>
<!-- ./wrapper -->
</body>
</html>
