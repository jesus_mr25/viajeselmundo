<style type="text/css">
  .message_add_admin, .message_update_admin, .message_delete_admin {
    display: none;
  }
</style>

<?php



if (isset($_GET['message'])) {
  if ($_GET['message']=="1") {
    ?>
    <style type="text/css">
      .message_add_admin {
        display: block !important;
      }
    </style>
    <?php
  }
  elseif ($_GET['message']=="2") {
    ?>
    <style type="text/css">
      .message_update_admin {
        display: block !important;
      }
    </style>
    <?php
  }
  elseif ($_GET['message']=="3") {
    ?>
    <style type="text/css">
      .message_delete_admin {
        display: block !important;
      }
    </style>
    <?php
  }
}



    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

    date_default_timezone_set("Europe/Madrid");

?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS ADMINISTRADORES-->

<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Tabla Administrador
            <small>Datos de los administradores</small>
          </h1>

        </section>

        <!-- Main content -->
        <section class="content">

        <!-- Your Page Content Here -->



        <div class="callout callout-success message_add_admin">
          <h4>¡Administrador añadido!</h4>

          <p>Administrador añadido satisfactoriamente.</p>
        </div>


        <div class="callout callout-success message_update_admin">
        <h4>¡Administrador actualizado!</h4>

        <p>Administrador actualizado satisfactoriamente.</p>
        </div>


        <div class="callout callout-success message_delete_admin">
        <h4>¡Administrador eliminado!</h4>

        <p>Administrador eliminado satisfactoriamente.</p>
        </div>



    <div class="box">

  <div class="box-body">
    <div class="dataTables_wrapper form-inline dt-bootstrap" id="example2_wrapper">
    <div class="row">
      <div class="col-sm-6"></div>
      <div class="col-sm-6"></div>
    </div>

    <div class="row">
      <div class="col-sm-12">
      <form method="get" action="admin.php" class="table_admin">
        <div class="ghost" style="margin-bottom: 30px">
          <table aria-describedby="example2_info" role="grid" id="example2" class="table table-bordered table-hover dataTable dataTable">
            <thead>
              <tr role="row"></tr>
            </thead>
            <tbody>

            <?php

            //I PUT INTO A ARRAY THE TABLE HEADER

             $table_header = array("Editar", "Actualizar", "Borrar", "Nombre", "Apellidos", "DNI", "Usuario", "Contraseña", "Email", "Activo");
            ?>


                    <?php

                        //I ROAM THE ARRAY AND I INSERT THE TABLE HEADER WITH DE ARRAY'S DATA

                        for ($i=3; $i < count($table_header); $i++) {
                            echo "<th>";
                             echo $table_header[$i];
                             echo "</th>";
                        }

                     ?>

                     <!--LOS INPUTS ESTAN VACIOS POR DEFECTO, SI HAY GET DEL QUE COINCIDA CON CADA UNO MOSTRARA EL VALOR DE CADA GET EN EL INPUT-->

                    <tr>
                        <td><input type='text' name='name' class='form-control add' style='width:100%; text-align:center;' required placeholder="Pedro" value="<?php if (isset($_GET['name'])) {echo $_GET['name'];} ?>"></td>
                        <td><input type='text' name='surname' class='form-control add' style='width:100%; text-align:center;' required placeholder="Castillo Pérez" value="<?php if (isset($_GET['surname'])) {echo $_GET['surname'];} ?>"></td>
                        <td><input type='text' name='dni' class='form-control add' style='width:100%; text-align:center;' required maxlength="9" placeholder="12345678A" value="<?php if (isset($_GET['dni'])) {echo $_GET['dni'];} ?>"></td>
                        <td><input type='text' name='user' class='form-control add' style='width:100%; text-align:center;' required placeholder="pepe_cp" value="<?php if (isset($_GET['user'])) {echo $_GET['user'];} ?>"></td>
                        <td><input type='text' name='pass' class='form-control add' style='width:100%; text-align:center;' required placeholder="123" value="<?php if (isset($_GET['pass'])) {echo $_GET['pass'];} ?>"></td>
                        <td><input type='email' name='email' class='form-control add' style='width:100%; text-align:center;' required placeholder="pedro@hotmail.com" value="<?php if (isset($_GET['email'])) {echo $_GET['email'];} ?>"></td>
                        <td><input type='text' name='active' class='form-control add' style='width:100%; text-align:center;' required maxlength="2" placeholder="Si" value="<?php if (isset($_GET['active'])) {echo $_GET['active'];} ?>"></td>
                    </tr>

            </tbody>
          </table>

        </div>
       <input class="add btn btn-info" type="submit" name="add" value="Nuevo Administrador" style="margin: -15px 0 10px 0; background-color: #3c8dbc;">
      </form>


        <div class="box-header">
          <h3 class="box-title"></h3>
        </div>
      <div class="ghost">
        <table aria-describedby="example2_info" role="grid" id="example2" class="table table-bordered table-hover dataTable dataTable">
        <thead>
          <tr role="row"></tr>
        </thead>
        <tbody>

          <?php

            //I MADE TWO COUNTER, WHICH I INITIALIZE THEM TO ONE

            $cont = 1;
            $num = 1;

            //I MADE A QUERY WHERE I TAKE EVERYTHING UNLESS UPDATE DATE AND CREATE DATE

            $result = $db->query("SELECT name, surname, dni, username, pass, email, active, id FROM admin where username!='jesus_mr25'");

             //I ROAM THE ARRAY AND I INSERT THE TABLE HEADER WITH DE ARRAY'S DATA

            for ($i=0; $i < count($table_header); $i++) {
                echo "<th>";
                 echo $table_header[$i];
                 echo "</th>";
            }

            //I MADE A LOOP WHERE I AM GOING TO PUT EVERY DATAS TO THE PREVIOUS QUERY WITH TWO OPTIONS: DELETE AND UPDATE

            //THE NUMBERS OF COUNTER $cont, I USE THEM FOR CREATE A FORM TO EACH ROUND TO THE LOOP, WHICH IS AROW WHERE THE NAME WILL BE, form1, form2, form3, ...., IT WILL BE LIKE UNTIL THE COUNTER ARRIVE TO THE FINAL OF THE RESULTS TO  THE BEFORE QUERY

            foreach ($result as $fila) {
              echo"<input type='checkbox' class='checkbox$cont' style='display:none'>";
              echo "<tr>";

              //OPEN THE FORM WHERE IT WILL STAY EVERY DATAS TO THE QUERY

              echo "<form name='form$cont' method='get' action='admin.php'>";

              //HERE, I INSERT A SWITCH TO ACTIVATE EVERY FIELD TO THAT ROW

              echo "<td>";
              echo "<div class='center'><div class='toggle-button$cont'><button disabled></button></div></div>";
              echo "</td>";

              //BUTTONS AND FORMS HAVE THE VARIABLE ADDED FOR THAT EACH FORM HAVE A DIFFERENTS NAMES

              //BUTTON TO UPDATE

              echo "<td>";
              echo "<input class='update$cont btn btn-info' type='submit' value='Actualizar' disabled name='update' style='background-color: #3c8dbc;'>";
              echo "</td>";

              //BUTTON TO DELETE

              echo "<td>";
              echo "<input class='drop$cont btn btn-info' type='submit' value='Borrar' disabled name='delete' style='background-color: #3c8dbc;'>";
              echo "</td>";

              //FROM HERE, EACH <TD> INSIDE HAVE A INPUT TYPE TEXT WHERE THE DEFAULT VALUE IS THE FIELD WITH DATABASE'S TABLE. ALSO, HAVE A PROPERTY READONLY, WHICH I WILL ACTIVATE WITH THE SWITCH. BESIDES, THEIR CLASSES ARE THE SAME NAME, BUT ONLY TO EACH FORM

              echo "<td>";
              echo "<div class='form-group'><input type='text' name='text$num' class='inputs$cont form-control' value='" . $fila["name"] . "' readonly style='width:100%; text-align:center;'></div>";
              echo "</td>";

              $num++;

              echo "<td>";
              echo "<div class=''><input type='text' name='text$num' class='inputs$cont form-control' value='" . $fila["surname"] . "' readonly style='width:100%; text-align:center'></div>";
              echo "</td>";

              $num++;

              echo "<td>";
              echo "<div class='form-group'><input type='text' name='text$num' class='inputs$cont form-control' value='" . $fila["dni"] . "' readonly style='width:100%; text-align:center' maxlength='9'></div>";
              echo "</td>";

              $num++;

             echo "<td>";
              echo "<div class='form-group'><input type='text' name='text$num' class='inputs$cont form-control' value='" . $fila["username"] . "' readonly style='width:100%; text-align:center'></div>";
              echo "</td>";

              $num++;

              echo "<td>";
              echo "<div class='form-group'><input type='text' name='text$num' class='inputs$cont form-control' value='" . $fila["pass"] . "' readonly style='width:100%; text-align:center'></div>";
              echo "</td>";

              $num++;

              echo "<td>";
              echo "<div class='form-group'><input type='email' name='text$num' class='inputs$cont form-control' value='" . $fila["email"] . "' readonly style='width:100%; text-align:center'></div>";
              echo "</td>";

              $num++;

              if ($fila["active"]==1) {
                  $active = "Si";
              }
              else {
                $active = "No";
              }

              echo "<td>";
              echo "<div class='form-group'><input type='text' name='text$num' class='inputs$cont form-control' value='" . $active . "' readonly style='width:45px; text-align:center' maxlength='2'></div>";
              echo "</td>";

              $num++;

              echo "<input type='hidden' name='text$num' value='" . $fila["id"] . "' >";

              $num++;

              echo "</form>";
              echo "</tr>";

              //THAT SCRIPT WILL BE WHICH CONTROL CHANGES AND ACTIVATIONS TO THE DIFFERENTS ELEMENTS INTO THE TABLE

              echo "<script type='text/javascript'>
                    $(document).ready(function(){
                      $(document).on('click', '.toggle-button$cont', function() {

                          if($('.checkbox$cont').is(':checked')) {
                              $('.checkbox$cont').prop('checked', false);
                              $('.inputs$cont').prop('readOnly', true);
                              $('.update$cont').prop('disabled', true);
                              $('.drop$cont').prop('disabled', true);
                              $(this).removeClass('toggle-button-selected');

                          } else {
                            $('div[class^=toggle-button]').removeClass('toggle-button-selected');
                            $('input[type=checkbox]').not('.add').prop('checked', false);
                            $('input[type=text]').not('.add').prop('readonly', true);
                            $('input[type=email]').not('.add').prop('readonly', true);
                            $('input[type=submit]').not('.add').prop('disabled', true);
                            $('.checkbox$cont').prop('checked', true);
                            $('.inputs$cont').prop('readOnly', false);
                            $('.update$cont').prop('disabled', false);
                            $('.drop$cont').prop('disabled', false);
                            $(this).addClass('toggle-button-selected');

                          }
                        });
                    });

                    </script>";

              //I OPEN THE TAG <script>
              //WHEN I DO CLICK, THE SWITCH WITH THAT CLASS NAME, I ADD A CLASS WITH THE NAME toggle-button(NUMBER LOOP ITERATION)-selected

              //IF THE HIDDEN CHECKBOK, TO THIS ROW, IS CHECKED
                  //CHANGE ONLY THIS CHECKBOX TO CHECK
                  //IT CAN TO EDIT EVERY INPUT TEXT IN THIS ROW
                  //IT DISABLE THE BUTTON TO UPDATE THIS ROW
                  //IT DISABLE THE BUTTON TO DELETE THIS ROW
                  //THE CLASS toggle-button-selected IS DELETED

              //IF THE HIDDEN CHECKBOK, TO THIS ROW, ISN'T CHECKED
                  //EVERY DIV THAT HIS NAME CONTAINS ANYTHING LIKE toggle-button, THE CLASS toggle-button-selected WILL BE DELETED
                  //EVERY INPUT TYPE CHECKBOX, EXCEPT THE OBJECTS WITH THE CLASS NAME "ADD", HIS PROPERTY OF CHECKED WILL CHANGE TO FALSE
                  //EVERY INPUT TYPE TEXT, EXCEPT THE OBJECTS WITH THE CLASS NAME "ADD", HIS PROPERTY OF READONLY WILL CHANGE TO TRUE
                  //EVERY INPUT TYPE EMAIL, EXCEPT THE OBJECTS WITH THE CLASS NAME "ADD", HIS PROPERTY OF READONLY WILL CHANGE TO TRUE
                  //EVERY INPUT TYPE SUBMIT, EXCEPT THE OBJECTS WITH THE CLASS NAME "ADD", HIS PROPERTY OF READONLY WILL CHANGE TO TRUE
                  //CHANGE ONLY THIS CHECKBOX TO UNCHECK
                  //IT DISABLE THE PROPERTY READONLY TO EVERY OBJECTS WITH THE CLASS NAME "inputs"
                  //IT ENABLE THE BUTTON TO UPDATE IN THIS ROW
                  //IT ENABLE THE BUTTON TO DELETE IN THIS ROW
              //CLOSE THE TAG <script>


            //NOW, THEY ARE NECESSARY STYLES TO WORK THE SWITCH AND IT DO THE ANIMATION
            //IT DON'T PUT STYLESHEET, BECAUSE I DON'T KNOW HOW MANY ITERATIONS HAVE THE LOOP IN EVERY QUERIES, IN THAT WAY, I PUT IT INTO THE LOOP AND I PUT THE STYLESHEET INTO THE LOOP AND I PUT INTO NAME'S CLASSES THE COUNTER'S VALUE. SO, IF THE LOOP DO 20 ITERATIONS, IT ISN'T NECESSARY REPEAT THE SAME TIMES INTO A STYLESHEET

              echo "<style type='text/css'>
                      .toggle-button$cont {
                        background-color: white;
                        margin: 0;
                        border-radius: 20px;
                        border: 2px solid #D0D0D0;
                        height: 24px;
                        cursor: pointer;
                        width: 50px;
                        position: relative;
                        display: inline-block;
                        user-select: none;
                        -webkit-user-select: none;
                        -ms-user-select: none;
                        -moz-user-select: none;
                      }

                      .toggle-button$cont button {
                        outline: 0;
                        display:block;
                        position: absolute;
                        left: 0;
                        top: -2px;
                        border-radius: 100%;
                        width: 30px;
                        height: 30px;
                        background-color: white;
                        float: left;
                        margin: -3px 0 0 -3px;
                        border: 2px solid #D0D0D0;
                        transition: left 0.3s;
                      }

                      .toggle-button-selected {
                        background-color: #3c8dbc;
                        border: 2px solid #3c8dbc;
                      }

                      .toggle-button-selected button {
                        left: 26px;
                        top: -5px;
                        margin: 0;
                        border: border: 2px solid #D0D0D0; ;
                        width: 30px;
                        height: 30px;
                        box-shadow: 0 0 4px rgba(0,0,0,0.1);
                      }
                    </style>";

            //I INCREASE ONE MORE THE COUNTER FOR EVERYTHING HAVE YOUR NAME APPROPIATE

            $cont++;

            //I RETURN TO PUT THE VALUE OF THAT VARIABLE IN ONE, IN ORDER TO TODAS LAS FILAS SE LLAMAN IGUAL

            $num = 1;
            }

          ?>

        </tbody>
        </table>
        </div>
      </div>
    </div>

   </div>
  </div><!-- /.box-body -->
  </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


    <!--HERE I AM GOING TO CHECK THAT BUTTON IS SEND IN $_GET TO REALIZE THE BEST ACTION-->

    <?php

        // IF $_GET IS TO UPDATE A ROW

        if (isset($_GET['update'])) {

            //THIS OPERATION IS TO FLIP THE DATE FOR ADD IN THE DATABASE

            $fecha = $_GET["text4"];
            $array = explode("-", $fecha);



            if ($_GET["text7"]=="Si" || $_GET["text7"]=="si") {
                $active = "1";
            }
            else {
                $active = "0";
            }

            //TO INSERT DATE OF UPDATE AND DATE OF CREATE, BOTH IS NOW()

            $now = date("Y-m-d") . " " . date("G:i:s");

            $result = $db->query("update admin set name='" . $_GET["text1"] . "', surname='" . $_GET["text2"] . "', dni='" . $_GET["text3"] . "', username='" . $_GET["text4"] . "', pass='" . hash("sha256" , $_GET["text5"]) . "', email='" . $_GET["text6"] . "', active='" . $active . "', updated='" . $now . "' where id='" . $_GET["text8"] . "'");

            //TO REFRESH THE PAGE, AND ADMIN CAN SEE HOW CHANGE THE DATABASE WITHOUT PRESS F5

            ?>

                <script language=JavaScript>
                 window.location = "admin.php?message=2";
                </script>
            <?php
        }


        // IF $_GET IS TO DELETE A ROW

        if (isset($_GET['delete'])) {
            $result = $db->query("delete from admin where dni='" . $_GET["text3"] . "'");

            ?>
                <script type='text/javascript'>
                  window.location = "admin.php?message=3";
                </script>
            <?php
        }


        // IF $_GET IS TO ADD A ROW

        if (isset($_GET['add'])) {

            //THIS OPERATION IS TO fLIP THE DATE FOR ADD IN THE DATABASE

            if ($_GET["active"]=="Si" || $_GET["active"]=="si") {
                $active = "1";
            }
            else {
                $active = "0";
            }

            //TO INSERT DATE OF UPDATE AND DATE OF CREATE, BOTH IS NOW()

            $now = date("Y-m-d") . " " . date("G:i:s");

            $result = $db->query("insert into `admin` VALUES (NULL, '" . $_GET['name'] . "', '" . $_GET['surname'] . "', '" . $_GET['dni'] . "', '" . $_GET['user'] . "', '" . hash("sha256" , $_GET['pass']) . "', '" . $_GET['email'] . "', '" . $active . "', '" . $now . "', '" . $now . "')");

            //IF THERE ARE ANY PROBLEM WITH THE QUERY, I SEND A GET AND I SHOW A ALERT.

            if (mysqli_error($db)) {
              ?>
               <script type='text/javascript'>
                  window.location = "admin.php?name=<?php echo $_GET['name']; ?>&surname=<?php echo $_GET['surname']; ?>&dni=<?php echo $_GET['dni']; ?>&user=<?php echo $_GET['user']; ?>&pass=<?php echo hash("sha256" , $_GET['pass']); ?>&email=<?php echo $_GET['email']; ?>&active=<?php echo $_GET['active']; ?>&error";
                </script>
              <?php
            }

            //BUT IF THE QUERY IT'S OK, DON'T SEND A GET

            else {
              ?>
               <script type='text/javascript'>
                  window.location = "admin.php?message=1";
                </script>
              <?php
            }
         }

         if (isset($_GET['error'])) {
           ?>
              <script type="text/javascript">
                alert("Error al insertar en la tabla. Revisa los datos introducidos.");
              </script>
           <?php
         }
    ?>
