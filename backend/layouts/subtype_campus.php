<?php

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

    date_default_timezone_set("Europe/Madrid");

?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS TIPOS DE DEPORTES-->

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Tipos de Campus</h3>
    </div><!-- /.box-header -->

    <div class="box-body">

    <form method="get" action="subtypes.php">

        <!--INTRODUZCO EN EL COMBOBOX TODAS LAS CLASES QUE HAY EN LA TABLA-->

        <select class="combo_campus form-control" name="combo_campus" id="id" onchange="submit()" style="width: 200px;">
            <option value="0">Seleccione una opción</option>
            <?php
                $result  = $db->query("select id, subclass from subtype where id_type=4 order by id");

                foreach ($result as $fila) {
                    echo "<option value='" . $fila['id'] . "'>" . $fila['subclass'] . "</option>";
                }
            ?>
        </select>
    </form>

        <?php

            //realizo la consulta a base de datos para obtener los elementos que necesito. por defecto no muestro ninguno, pero despues voy mostrando el que este seleccionado en el combobox.

            if (isset($_GET['combo_campus'])) {
                $combo_campus = $_GET['combo_campus'];

                $result3_campus = $db->query("select id, subclass from subtype where id=" . $combo_campus);

                if ($combo_campus!=0) {
                    foreach ($result3_campus as $fila_campus) {
                        $data_campus[0] = $fila_campus['id'];
                        $data_campus[1] = $fila_campus['subclass'];
                    }
                }
                else {
                    $data_campus[0] = "";
                    $data_campus[1] = "";
                }
            }

         ?>

        <!--le paso los valores del get para que cuando cambie el combobox cambie tambien en los inputs-->


        <form class="form-ticket" method="post" action="subtypes.php" name="form1">
            <p style="font-size: 15px;">Subcategoria<input type="text" class="form-control" style="width: 100%;" name="class_campus" value="<?php if (isset($_GET['combo_campus'])) {echo $data_campus[1];} ?>" placeholder="Nueva entrada" required></p>
            <p><input type="text" name="id_campus" value="<?php if (isset($_GET['combo_campus'])) {echo $data_campus[0];} ?>" style="display: none;"></p>

            <input class="btn btn-info add" type="submit" style="background-color: #3c8dbc;" name="add_campus" value="Nueva Entrada">
            <input class="btn btn-info update" type="submit" style="background-color: #3c8dbc;" name="update_campus" value="Actualizar">
            <input class="btn btn-info delete" type="submit" style="background-color: #3c8dbc;" name="delete_campus" value="Borrar">
        </form>

        <?php

        if (isset($_GET['error_campus'])) {
            ?>
              <script type="text/javascript">
                alert("Error al insertar en la tabla. Revisa los datos introducidos.");
              </script>
            <?php
        }

        if (isset($_POST['update_campus'])) {

            //THIS OPERATION IS TO FLIP THE FIELD "foreign" FOR ADD IN THE DATABASE

            $now_campus = date("Y-m-d") . " " . date("G:i:s");

            $result4_campus = $db->query("update subtype set subclass='" . $_POST['class_campus'] . "', updated='" . $now_campus . "' WHERE id=" . $_POST['id_campus'] . "");

            //TO REFRESH THE PAGE, AND ADMIN CAN SEE HOW CHANGE THE DATABASE WITHOUT PRESS F5

            if (!$result4_campus) {
                ?>
                    <script language=JavaScript>
                        window.location = "subtypes.php?error_campus";
                    </script>
                <?php
            }
            else {
                ?>
                    <script language=JavaScript>
                      window.location = "subtypes.php?visible_campus=&message=2";
                    </script>
                <?php
            }
        }



        if (isset($_POST['delete_campus'])) {
          $check = $db->query("select * from product_generic where id_subtype = (select id from subtype where subclass= '" . $_POST['class_campus'] . "')");
          if (mysqli_num_rows($check) > 0) {
            ?>
              <style type="text/css">
                .show_error_delete {
                  display: inherit !important;
                }
              </style>
            <?php
          }
          else{
            $db->query("delete from subtype where subtype.id='" . $_POST["id_campus"] . "'");

              ?>
                  <script type='text/javascript'>
                    window.location = "subtypes.php?visible_campus=&message=3";
                  </script>
              <?php
          }
        }



        if (isset($_POST['add_campus'])) {
            $now_campus = date("Y-m-d") . " " . date("G:i:s");

            $insert_campus = $db->query("insert into subtype values (null, 4, '" . $_POST['class_campus'] . "', '" . $now_campus . "', '" . $now_campus . "')");


            if (!$insert_campus) {
                ?>
                    <script language=JavaScript>
                        //window.location = "subtypes.php?error_campus";
                    </script>
                <?php

            }
            else {
                ?>
                    <script language=JavaScript>
                      window.location = "subtypes.php?visible_campus=&message=1";
                    </script>
                <?php
            }
        }
                ?>
    </div>
</div>

<?php include('scripts.php'); ?>
