<style type="text/css">
  .message_add_token-discount, .message_update_token-discount, .message_delete_token-discount {
    display: none;
  }
</style>

<?php

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

    date_default_timezone_set("Europe/Madrid");


    if (isset($_GET['message'])) {
      if ($_GET['message']=="1") {
        ?>
        <style type="text/css">
          .message_add_token-discount {
            display: block !important;
          }
        </style>
        <?php
      }
      elseif ($_GET['message']=="2") {
        ?>
        <style type="text/css">
          .message_update_token-discount {
            display: block !important;
          }
        </style>
        <?php
      }
      elseif ($_GET['message']=="3") {
        ?>
        <style type="text/css">
          .message_delete_token-discount {
            display: block !important;
          }
        </style>
        <?php
      }
    }


?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS TICKETS DE DESCUENTO-->

 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Tabla Códigos Promocionales
          <small>Descripción de los códigos promocionales</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

     	<!-- Your Page Content Here -->



      <div class="callout callout-success message_add_token-discount">
        <h4>¡Codigo Promocional añadido!</h4>

        <p>Codigo Promocional añadido satisfactoriamente.</p>
      </div>


      <div class="callout callout-success message_update_token-discount">
      <h4>¡Codigo Promocional actualizado!</h4>

      <p>Codigo Promocional actualizado satisfactoriamente.</p>
      </div>


      <div class="callout callout-success message_delete_token-discount">
      <h4>¡Codigo Promocional eliminado!</h4>

      <p>Codigo Promocional eliminado satisfactoriamente.</p>
      </div>



      	<div class="box">
            <div class="box-header">

            </div><!-- /.box-header -->

            <div class="box-body">

            <!--CARGO LAS OPCIONES DISPONIBLES DE ESTA TABLA EN EL COMBOBOX-->

            <select class="token combo form-control" name="id" id="id" onchange="token_discount()" style="width: 200px;">
                <option value="0">Seleccione una opción</option>
                <?php
                    $result  = $db->query("select `id`, `token` from token_discount order by id");

                    foreach ($result as $fila) {
                        echo "<option value='" . $fila['id'] . "'>" . $fila['token'] . "</option>";

                    }
                ?>
            </select>

                <?php
                    //REALIZO LA CONSULTA A BASE DE DATOS PARA OBTENER LOS ELEMENTOS QUE NECESITO. POR DEFECTO NO MUESTRO NINGUNO, PERO DESPUES VOY MOSTRANDO EL QUE ESTE SELECCIONADO EN EL COMBOBOX.

                    if (isset($_GET['token'])) {
                        $token = $_GET['token'];

                        if ($token!=0) {
                            $result3 = $db->query("select discount, id, token from token_discount where id=" . $token);

                            foreach ($result3 as $fila) {
                                $data[0] = $fila['discount'];
                                $data[1] = $fila['token'];
                                $data[2] = $fila['id'];
                            }
                        }
                        else {
                            $data[0] = "";
                            $data[1] = "";
                            $data[2] = "";
                        }
                    }

                 ?>

                <!--le paso los valores del get para que cuando cambie el combobox cambie tambien en los inputs-->
                <form class="form-sport" method="post" action="token_discount.php">
                    <p style="font-size: 15px;">Nombre<input type="text" class="form-control" style="width: 100%;" name="token" value="<?php if (isset($_GET['token'])) {echo $data[1];} ?>" placeholder="Descuento por familia numerosa" required></p>
                    <p style="font-size: 15px;">Descuento<input type="number" class="form-control" style="width: 100%; max-width: 100%; min-width: 100%;" name="discount" placeholder="20" required value="<?php if (isset($_GET['token'])) {echo $data[0];} ?>" min="1" max="100"></p>

                    <p><input type="text" name="id" value="<?php if (isset($_GET['token'])) {echo $data[2];} ?>" style="display: none;"></p>
                    <input class="btn btn-info" type="submit" style="background-color: #3c8dbc;" name="add" value="Nueva Entrada">
                    <input class="btn btn-info" type="submit" style="background-color: #3c8dbc;" name="update" value="Actualizar">
                    <input class="btn btn-info" type="submit" style="background-color: #3c8dbc;" name="delete" value="Borrar">
                </form>

<?php

    if (isset($_POST['update'])) {

        //THIS OPERATION IS TO FLIP THE FIELD "foreign" FOR ADD IN THE DATABASE

        $now = date("Y-m-d") . " " . date("G:i:s");


        $result4 = $db->query("update token_discount set token='" . $_POST['token'] . "', discount='" . $_POST['discount'] . "' WHERE id=" . $_POST['id'] . "");


        //TO REFRESH THE PAGE, AND ADMIN CAN SEE HOW CHANGE THE DATABASE WITHOUT PRESS F5

        if (!$result4) {
            ?>
                <script language=JavaScript>
                    window.location = "token_discount.php?error";
                </script>
            <?php
        }
        else {
        ?>
            <script language=JavaScript>
              window.location = "token_discount.php?message=2";
            </script>
        <?php
        }
    }

    if (isset($_POST['delete'])) {
        $db->query("delete from token_discount where id='" . $_POST["id"] . "'");

        ?>
            <script type='text/javascript'>
              window.location = "token_discount.php?message=3";
            </script>
        <?php
    }

    if (isset($_POST['add'])) {
        $now = date("Y-m-d") . " " . date("G:i:s");

        //THIS OPERATION IS TO FLIP THE FIELD "foreign" FOR ADD IN THE DATABASE


        $insert = $db->query("insert into token_discount values (null, '" . $_POST['token'] . "', '" . $_POST['discount'] . "')");


        if (!$insert) {
            ?>
                <script language=JavaScript>
                    window.location = "token_discount.php?error";
                </script>
            <?php
        }
        else {
        ?>
            <script language=JavaScript>
              window.location = "token_discount.php?message=1";
            </script>
        <?php
        }

    }
 ?>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php include('scripts.php'); ?>
