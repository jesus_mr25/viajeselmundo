<?php

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

    date_default_timezone_set("Europe/Madrid");

?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS TICKETS DE DEPORTES-->

     <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Tabla Suscriptores
          <small>Descripción de los suscriptores</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

     	<!-- Your Page Content Here -->

      	<div class="box">
            <div class="box-header">

            </div><!-- /.box-header -->

            <div class="box-body table_buy">

            <button type="button" class="btn btn-info" onclick="window.location.href='excel.php?excel'" style="margin-bottom: 20px;">Generar Archivo Excel</button>

            <table aria-describedby="example2_info" role="grid" id="example2" class="table table-bordered table-hover dataTable">
                    <thead>
                      <tr role="row">
                        <th>Email</th>
                        <th>Fecha Suscripción</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php


                    //REALIZO LA CONSULTA DE LOS DATOS QUE NECESITO DE LA TABLA DE LAS COMPRAS REALIZADAS

                    $query= $db->query("select email, date_suscriber from subscriber");

                    foreach ($query as $row25) {

                      $date = explode("-", $row25['date_suscriber']);

                         echo "<tr style='text-align: center;' style='text-align:center;'>
                                  <td>" . $row25['email'] . "</td>
                                  <td>" . $date[2] . "-" . $date[1] . "-" . $date[0] . "</td>
                              </tr>";

                            ?>

                        <?php
                    }


                     ?>
                </table>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
