<?php

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

    date_default_timezone_set("Europe/Madrid");

?>

<!--ESTA ES LA PAGINA DONDE ESTAN LOS TIPOS DE SUBTIPOS QUE EXISTEN-->

 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Tablas Tipos de Productos
          <small>Descripción de los tipos de productos</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="callout callout-warning show_error_delete" style="display: none;">
         <h4>¡Ups, algo salió mal!</h4>

         <p>Le recordamos que no puede borrar un tipo de producto, si existen prodoctos creados para este tipo.</p>
      </div>

      <?php
        if (isset($_GET['message'])) {
          if ($_GET['message']=="1") {
            echo "<div class='callout callout-success'>
               <h4>¡Estupendo!</h4>

               <p>Tipo de producto creado satisfactoriamente.</p>
            </div>";
          }
          elseif ($_GET['message']=="2") {
            echo "<div class='callout callout-success'>
               <h4>¡Estupendo!</h4>

               <p>Tipo de producto actualizado satisfactoriamente.</p>
            </div>";
          }
          elseif ($_GET['message']=="3") {
            echo "<div class='callout callout-success'>
               <h4>¡Estupendo!</h4>

               <p>Tipo de producto borrado satisfactoriamente.</p>
            </div>";
          }

        }
       ?>



     	<!-- Your Page Content Here -->

        <!--AQUI MUESTRO TODAS LAS OPCIONES MEDIANTE RADIOBUTTON QUE HAY DE SUBTIPOS, LAS CUALES PERMITEN MODIFICARLAS-->

       <div class="box">
            <form>
              <p style="padding: 10px 0 10px 10px;">
                <input class="radio_subtype r_c" type="radio" name="radio" value="4"> Tipos de Campus<br>
                <input class="radio_subtype r_s" type="radio" name="radio" value="1"> Tipos de Deportes<br>
                <input class="radio_subtype r_t" type="radio" name="radio" value="2"> Tipos de Entradas<br>
                <input class="radio_subtype r_f" type="radio" name="radio" value="3"> Tipos de Ocio<br>
                <input class="radio_subtype r_o" type="radio" name="radio" value="5"> Tipos de Ofertas
              </p>
            </form>
        </div>

        <div class="campus div_subtypes" hidden><?php include("layouts/subtype_campus.php"); ?></div>

      	<div class="sport div_subtypes" hidden><?php include("layouts/subtype_sport.php"); ?></div>

        <div class="ticket div_subtypes" hidden><?php include("layouts/subtype_ticket.php"); ?></div>

        <div class="freetime div_subtypes" hidden><?php include("layouts/subtype_freetime.php"); ?></div>

        <div class="offer div_subtypes" hidden><?php include("layouts/subtype_offer.php"); ?></div>



    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<?php include('scripts.php'); ?>
