<?php

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO, PARA MÁS ADELANTE USAR LAS FUNCIONES DATE()

    date_default_timezone_set("Europe/Madrid");

    $datetime = date("Y-m-d") . " " . date("G:i:s");


    $sending = "";


    //SI HAY POST ADD AÑADE UNA NOTICIA NUEVA


    if (isset($_POST['add'])) {
      $db->query("insert into tab_last_new values(NULL, '" . $_POST['new'] . "', '" . $datetime . "')");
      ?>

        <!--SI HAY GET MOSTRARÁ EL MENSAJE OCULTO INDICANDO EL MENSAJE-->

        <script type="text/javascript">
          $(document).ready(function(){
            $(".message_info_top_destiny").prop("hidden", false);
          });

        </script>

    <?php

    //MODIFICO LA VARIABLE SEND PARA QUE SE ENVIE EL CORREO

    $sending = "send";

    }


    //SI HAY POST UPDATE ACTUALIZA UNA NOTICIA ANTERIORMENTE CREADA


    if (isset($_POST['update'])) {

      $db->query("update tab_last_new set news='" . $_POST['new'] . "', date='" . $datetime . "' where id=" . $_POST['id']);
      ?>

        <!--SI HAY GET MOSTRARÁ EL MENSAJE OCULTO INDICANDO EL MENSAJE-->

        <script type="text/javascript">
          $(document).ready(function(){
            $(".message_update_top_destiny").prop("hidden", false);
          });

        </script>

    <?php

    $sending = "send";

    }

    //SI HAY POST DELETE BORRA LA NOTICIA SELECCIONADA


      if (isset($_POST['delete'])) {
        $db->query("delete from tab_last_new where id=" . $_POST['id']);
        ?>

          <!--SI HAY GET MOSTRARÁ EL MENSAJE OCULTO INDICANDO EL MENSAJE-->

          <script type="text/javascript">
            $(document).ready(function(){
              $(".message_delete_top_destiny").prop("hidden", false);
            });

          </script>

      <?php
      }

      //SI EL VALOR DE LA VARIABLE ES SEND ENVIA LOS CORREOS DE LA NOTICIA CREADA O ACTUALIZADA A LOS SUSCRIPTORES

      if ($sending=="send") {

        require('../phpmailer/PHPMailerAutoload.php');

        $subscriber = $db->query("select email from subscriber");

        foreach ($subscriber as $row20) {
            //A CONTINUACION, ENVIO UN CORREO AL NUEVO SUBSCRIPTOR PARA QUE RECIBA NOTICIAS CADA VEZ QUE SE CREEN

            $host= $_SERVER["HTTP_HOST"];

            $subject = "Boletín de Viajes El Mundo";
            $email = $row20['email'];
            $body = "<html lang='es'>
                  <head>
                    <meta charset='utf-8'>
                  </head>
                  <body>
                    <img src='http://www.viajeselmundo.esy.es/images/mini_logo.png'>
                    <br>
                    <br>
                    " . $_POST['new'] . "
                    <br>
                    <br>
                    <p>Este boletín es voluntario, es decir, si no desea recibir esta información solo ha de seguir este <a href='http://" . $host . "/unscribe.php?user_unscribe=" . $row20['email'] . "'>ENLACE</a></p>
                  </body>
                </html>";


            //CREO UN NUEVA CLASE DE PHPMAILER
            //LE PASO EL CORREO DEL RECEPTOR Y EL NOMBRE DEL REMITENTE
            //LE PASO EL CORREO DEL RECEPTOR
            //LE PONGO EL ASUNTO

            //LE PASO EL CUERPO DEL MENSAJE
            //LE PASO LA CODIFICACION DEL LENGUAJE
            //LE PASO EL TIPO DE MENSAJE
            //Y LO ENVIO

            $mailer = new PHPMailer();
            $mailer->setFrom('no-reply@viajeselmundo.es','Noticias - Viajes El Mundo');
            $mailer->addAddress($row20['email']);
            $mailer->Subject = $subject;
            $mailer->isHTML(true);
            $mailer->Body = $body;
            $mailer->CharSet = 'utf-8';
            $mailer->mailer = 'mail';
        }
      }


?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS LOS DESTINOS TOP DEL INDEX-->

 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Pestaña de Últimas noticias
          <small>Selección de los datos que se posicionarán en la pestaña últimas noticias de la Página Principal.</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="callout callout-success message_info_top_destiny" hidden>
          <h4>¡Noticia añadida!</h4>

          <p>Noticia añadida satisfactoriamente.</p>
        </div>


       <div class="callout callout-success message_delete_top_destiny" hidden>
         <h4>¡Noticia eliminada!</h4>

         <p>Noticia eliminada satisfactoriamente.</p>
       </div>


      <div class="callout callout-success message_update_top_destiny" hidden>
        <h4>¡Noticia corregida!</h4>

        <p>Noticia corregida satisfactoriamente.</p>
      </div>

        <!-- Your Page Content Here -->

        <div class="box">
            <div class="box-header">

            </div><!-- /.box-header -->

            <div class="box-body">

              <form method="get" action="tab_last_news.php">
                  <select class="combo_new form-control" name="combo_news" id="id" onchange="submit()" style="width: 200px; margin-bottom: 10px;">
                      <option value="0">Seleccione una noticia</option>
                      <?php
                      $result = $db->query("select * from tab_last_new order by id");

                          foreach ($result as $fila) {

                            $dateetime = explode(" ", $fila['date']);
                            $datee = explode("-", $dateetime[0]);

                              if ($_GET['combo_news'] == $fila['id']) {
                                echo "<option value='" . $fila['id'] . "' selected>" . $datee[2] . "-" . $datee[1] . "-" . $datee[0] . " " . $dateetime[1] . "</option>";
                              }
                              else {
                                echo "<option value='" . $fila['id'] . "'>" . $datee[2] . "-" . $datee[1] . "-" . $datee[0] . " " . $dateetime[1] . "</option>";
                              }
                          }
                      ?>
                  </select>
              </form>

            <form action="tab_last_news.php" method="post">
              <input style="margin-bottom: 10px;" type="submit" name="add" class="btn btn-info" value="Crear Noticia">
              <input style="margin-bottom: 10px;" type="submit" name="update" class="btn btn-info" value="Actualizar Noticia">
              <input style="margin-bottom: 10px;" type="submit" name="delete" class="btn btn-info" value="Borrar Noticia">

              <input type="text" name="id" value="<?php if (isset($_GET['combo_news'])) {echo $_GET['combo_news'];} ?>" hidden>

              <textarea id="full_ckeditor" class="form-control" style="width: 100%; max-width: 100%; min-width: 100%;" name="new" required>
                  <?php
                    if (isset($_GET['combo_news'])) {
                      $result10 = $db->query("select * from tab_last_new where id = '" . $_GET['combo_news'] . "'");

                      foreach ($result10 as $row30) {
                        echo $row30['news'];
                      }
                    }
                  ?>
              </textarea>
            </form>

            </div>
        </div>


    </section>
</div>

<?php include('scripts.php'); ?>
