<?php

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

    date_default_timezone_set("Europe/Madrid");

?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS TIPOS DE TICKET-->

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Tipos de Entradas</h3>
    </div><!-- /.box-header -->

    <div class="box-body">

        <!--INTRODUZCO EN EL COMBOBOX TODAS LAS CLASES QUE HAY EN LA TABLA-->

        <form method="get" action="subtypes.php">
            <select class="combo_ticket form-control" name="combo_ticket" id="id" onchange="submit()" style="width: 200px;">
                <option value="0">Seleccione una opción</option>
                <?php
                $result  = $db->query("select id, subclass from subtype where id_type=2 order by id");

                    foreach ($result as $fila) {
                        echo "<option value='" . $fila['id'] . "'>" . $fila['subclass'] . "</option>";
                    }
                ?>
            </select>
        </form>


        <?php

            //realizo la consulta a base de datos para obtener los elementos que necesito. por defecto no muestro ninguno, pero despues voy mostrando el que este seleccionado en el combobox.

            if (isset($_GET['combo_ticket'])) {
                $combo_ticket = $_GET['combo_ticket'];

                $result3_ticket = $db->query("select id, subclass from subtype where id=" . $combo_ticket);

                if ($combo_ticket!=0) {
                    foreach ($result3_ticket as $fila_ticket) {
                        $data_ticket[0] = $fila_ticket['id'];
                        $data_ticket[1] = $fila_ticket['subclass'];
                    }
                }
                else {
                    $data_ticket[0] = "";
                    $data_ticket[1] = "";
                }
            }

         ?>

        <!--le paso los valores del get para que cuando cambie el combobox cambie tambien en los inputs-->

        <form class="form-ticket" method="post" action="subtypes.php" name="form1">
            <p style="font-size: 15px;">Subcategoria<input type="text" class="form-control" style="width: 100%;" name="class_ticket" value="<?php if (isset($_GET['combo_ticket'])) {echo $data_ticket[1];} ?>" placeholder="Nueva entrada" required></p>
            <p><input type="text" name="id_ticket" value="<?php if (isset($_GET['combo_ticket'])) {echo $data_ticket[0];} ?>" style="display: none;"></p>

            <input class="btn btn-info add" type="submit" style="background-color: #3c8dbc;" name="add_ticket" value="Nueva Entrada">
            <input class="btn btn-info update" type="submit" style="background-color: #3c8dbc;" name="update_ticket" value="Actualizar">
            <input class="btn btn-info delete" type="submit" style="background-color: #3c8dbc;" name="delete_ticket" value="Borrar">
        </form>

        <?php

        if (isset($_GET['error_ticket'])) {
            ?>
              <script type="text/javascript">
                alert("Error al insertar en la tabla. Revisa los datos introducidos.");
              </script>
            <?php
        }

        if (isset($_POST['update_ticket'])) {

            //THIS OPERATION IS TO FLIP THE FIELD "foreign" FOR ADD IN THE DATABASE

            $now_ticket = date("Y-m-d") . " " . date("G:i:s");

            $result4_ticket = $db->query("update subtype set subclass='" . $_POST['class_ticket'] . "', updated='" . $now_ticket . "' WHERE id=" . $_POST['id_ticket'] . "");

            //TO REFRESH THE PAGE, AND ADMIN CAN SEE HOW CHANGE THE DATABASE WITHOUT PRESS F5

            if (!$result4_ticket) {
                ?>
                    <script language=JavaScript>
                        window.location = "subtypes.php?error_ticket=&message=2";
                    </script>
                <?php
            }
            else {
                ?>
                    <script language=JavaScript>
                      window.location = "subtypes.php?visible_ticket";
                    </script>
                <?php
            }
        }

        if (isset($_POST['delete_ticket'])) {
          $check = $db->query("select * from product_generic where id_subtype = (select id from subtype where subclass= '" . $_POST['class_ticket'] . "')");
          if (mysqli_num_rows($check) > 0) {
            ?>
              <style type="text/css">
                .show_error_delete {
                  display: inherit !important;
                }
              </style>
            <?php
          }
          else{
            $db->query("delete from subtype where subtype.id='" . $_POST["id_ticket"] . "'");

              ?>
                  <script type='text/javascript'>
                    window.location = "subtypes.php?visible_ticket=&message=3";
                  </script>
              <?php
          }
        }

        if (isset($_POST['add_ticket'])) {
            $now_ticket = date("Y-m-d") . " " . date("G:i:s");

            $insert_ticket = $db->query("insert into subtype values (null, 2, '" . $_POST['class_ticket'] . "', '" . $now_ticket . "', '" . $now_ticket . "')");


            if (!$insert_ticket) {
                ?>
                    <script language=JavaScript>
                        window.location = "subtypes.php?error_ticket";
                    </script>
                <?php

            }
            else {
                ?>
                    <script language=JavaScript>
                      window.location = "subtypes.php?visible_ticket=&message=1";
                    </script>
                <?php
            }
        }
                ?>
    </div>
</div>
