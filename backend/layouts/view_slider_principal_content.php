<?php 

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO, PARA MÁS ADELANTE USAR LAS FUNCIONES DATE()

    date_default_timezone_set("Europe/Madrid");


    $host = $_SERVER["HTTP_HOST"];


    //CUANDO HAYA GET BORRO TODOS LOS DATOS DE LAS TABLAS Y LUEGO INSERTO LAS TRES IMAGENES SELECCIONADAS

    if ($_GET) {

      ?>

        <!--SI HAY GET MOSTRARÁ EL MENSAJE OCULTO INDICANDO EL MENSAJE-->

        <script type="text/javascript">
          $(document).ready(function(){
            $(".message_info_slider_principal").prop("hidden", false);
          });
        </script>


      <?php
        $db->query("delete from slider_principal");
        
        foreach ($_GET as $key) {
          $db->query("insert into slider_principal values(NULL, " . $key . ")");
        }
      }      
  
?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS LOS DESTINOS TOP DEL INDEX-->
 
 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Slider Principal
          <small>Selección de las imagenes que se posicionarán en el Slider de la Página Principal.</small>
        </h1>
        <p style="margin-top: 10px;"><em>Para una correcta visualización de las imágenes, insértelas con unas dimensiones similares a 1130px de ancho y 650px de alto.</em></p>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Your Page Content Here -->

        <div class="box">
            <div class="box-header">
          
            </div><!-- /.box-header -->

            <div class="box-body">

                <div class="callout callout-success message_info_slider_principal" hidden>
                  <h4>¡Imágenes cambiadas!</h4>

                  <p>Cambio de imágenes realizado satisfactoriamente.</p>
                </div>

               <form action="view_slider_principal.php" method="get">
                <button class="btn btn-info change_image" disabled>Cambiar imágenes</button>
                <?php 
                    $cont = 1;

                    //CON ESTA CONSULTA MUESTRO TODAS LAS IMAGENES QUE ESTAN METIDAS EN LA BASE DE DATOS

                    $result  = $db->query("select id, name from multimedia");
                    
                    foreach ($result as $row) {
                        echo "<div class='img_for_slider'><img class='img" . $row['id'] . "' src='uploads/" . $row['name'] . "'><input value='" . $row['id'] . "' type='checkbox' name='img" . $cont . "' hidden></div>";
                        $cont++;
                    } 
                ?>
              </form>
            </div>                  
        </div>
    </section>
</div>

  <?php 

    include('scripts.php'); 

    //CON ESTA CONSULTA OBTENGO LAS IMAGENES QUE HAY INSERTADAS EN LA TABLA DE SLIDER_PRINCIPAL, Y ASI PODER DEJAR MARCADAS LAS QUE ESTEN SIENDO USADAS EN EL SLIDER

    $selected_image = $db->query("select id_multimedia from slider_principal");

    foreach ($selected_image as $row) {
      

        echo "<script type='text/javascript'>
                $(document).ready(function(){ 
                  $('.img" . $row['id_multimedia'] . "').css('border', '8px solid green');
                });
              </script>";

    }

 ?>