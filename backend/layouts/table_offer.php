<style type="text/css">
  .message_add_product, .message_update_product, .message_delete_product, .message_add_image, .message_delete_image {
    display: none;
  }
</style>


<?php

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO, PARA MÁS ADELANTE USAR LAS FUNCIONES DATE()

    date_default_timezone_set("Europe/Madrid");


    if (isset($_GET['message'])) {
      if ($_GET['message']=="1") {
        ?>
        <style type="text/css">
          .message_add_product {
            display: block !important;
          }
        </style>
        <?php
      }
      elseif ($_GET['message']=="2") {
        ?>
        <style type="text/css">
          .message_update_product {
            display: block !important;
          }
        </style>
        <?php
      }
      elseif ($_GET['message']=="3") {
        ?>
        <style type="text/css">
          .message_delete_product {
            display: block !important;
          }
        </style>
        <?php
      }
      elseif ($_GET['message']=="4") {
        ?>
        <style type="text/css">
          .message_add_image {
            display: block !important;
          }
        </style>
        <?php
      }
      elseif ($_GET['message']=="5") {
        ?>
        <style type="text/css">
          .message_delete_image {
            display: block !important;
          }
        </style>
        <?php
      }
    }


?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS TICKETS DE LAS OFERTAS-->

 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Tabla Oferta
          <small>Descripción de la venta de diferentes ofertas</small>
        </h1>
        <p style="margin-top: 10px;"><em>Para una correcta visualización de las imágenes, insértelas con unas dimensiones similares a 567px de ancho y 320px de alto o px de ancho y 220px de 143px de alto.</em></p>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Your Page Content Here -->



        <div class="callout callout-success message_add_product">
          <h4>¡Producto añadido!</h4>

          <p>Producto añadido satisfactoriamente.</p>
        </div>


        <div class="callout callout-success message_update_product">
        <h4>¡Producto actualizado!</h4>

        <p>Producto actualizado satisfactoriamente.</p>
        </div>


        <div class="callout callout-success message_delete_product">
        <h4>¡Producto eliminado!</h4>

        <p>Producto eliminado satisfactoriamente.</p>
        </div>


        <div class="callout callout-success message_add_image">
          <h4>¡Imagen añadida!</h4>

          <p>Imagen añadida satisfactoriamente.</p>
        </div>


        <div class="callout callout-success message_delete_image">
          <h4>¡Imagen borrada!</h4>

          <p>Imagen borrada satisfactoriamente.</p>
        </div>





        <div class="box">
            <div class="box-header">

            </div><!-- /.box-header -->

            <div class="box-body">

            <!--CARGO LAS OPCIONES DISPONIBLES DE ESTA TABLA EN EL COMBOBOX MEDIANTE LA CONSUTA MYSQL-->

            <select class="combo form-control" name="id" id="id" onchange="offer_location()" style="width: 200px;">
                <option value="0">Seleccione una opción</option>
                <?php
                    $result  = $db->query("select distinct product_generic_id, title from result_offer order by product_generic_id");

                    foreach ($result as $fila) {
                        echo "<option value='" . $fila['product_generic_id'] . "'>" . $fila['title'] . "</option>";

                    }
                ?>
            </select>

                <?php
                    //REALIZO LA CONSULTA A LA BASE DE DATOS PARA OBTENER LOS ELEMENTOS QUE NECESITO. POR DEFECTO, NO MUESTRO NINGUNO, PERO DESPUÉS VOY MOSTRANDO EL QUE ESTE SELECCIONADO EN EL COMBOBOX.

                    if (isset($_GET['combo'])) {
                        $combo = $_GET['combo'];

                        //SI EL VALOR DEL COMBOBOX ES DISTINTO DE CERO, REALIZARIA LA CONSULTA Y GUARDA LOSRESULTADOSDE LACONSULTA EN VARIABES PARA POSTERIORMENTE PODER TRABAJAR CON ELLAS.

                        if ($combo!=0) {
                            $result3 = $db->query("select link_pdf, link_blog, title, description, price_individual, price_group, `foreign`, tickets_availables, product_generic_id, locate, subtype_id, discount_id from result_offer where product_generic_id=" . $combo);

                            foreach ($result3 as $fila) {
                                $data[0] = $fila['title'];
                                $data[1] = $fila['description'];
                                $data[2] = $fila['price_individual'];
                                $data[3] = $fila['price_group'];
                                if ($fila['foreign']=="1") {
                                    $data[4] = "Si";
                                }
                                else {
                                    $data[4] = "No";
                                }
                                $data[5] = $fila['tickets_availables'];
                                $data[6] = $fila['product_generic_id'];
                                $data[7] = $fila['locate'];
                                $data[8] = $fila['subtype_id'];
                                $data[9] = "1";
                                $data[10] = $fila['discount_id'];
                                $data[11] = $fila['link_blog'];
                                $data[12] = $fila['link_pdf'];
                            }
                        }

                        //EN CASO DE QUE NO HAYA NINGUN VALOR CARGADO EN EL COMBOBOX, DEJARA LOS INPUTS VACIOS

                        else {
                            $data[0] = "";
                            $data[1] = "";
                            $data[2] = "";
                            $data[3] = "";
                            $data[4] = "";
                            $data[5] = "";
                            $data[6] = "";
                            $data[7] = "";
                            $data[8] = "";
                            $data[9] = "";
                            $data[10] = "";
                            $data[11] = "";
                            $data[12] = "";
                        }
                    }

                 ?>

                <!--LE PASO LOS VALORES DEL GET PARA QUE CUENDO CAMBIE EL COMBOBOX CAMBIE TAMBIEN EN LOS INPUTS-->

                <form class="form-ticket" method="post" action="offer.php" enctype="multipart/form-data">
                    <p style="font-size: 15px;">Título<input maxlength="55" type="text" class="form-control" style="width: 100%;" name="title" value="<?php if (isset($_GET['combo'])) {echo $data[0];} ?>" placeholder="Festival de Eurovisión" max="60" required></p>
                    <p style="font-size: 15px;">Descripción<textarea id="ckeditor" class="form-control" style="width: 100%; max-width: 100%; min-width: 100%;" name="description" required><?php if (isset($_GET['combo'])) {echo $data[1];} ?></textarea></p>
                    <p style="font-size: 15px;">Precio Individual<input maxlength="7" type="text" class="form-control" style="width: 100%;" name="priceIndividual" value="<?php if (isset($_GET['combo'])) {echo $data[2];} ?>" placeholder="200.90" required></p>
                    <p style="font-size: 15px;">Precio Grupo<input maxlength="7" type="text" class="form-control" style="width: 100%;" name="priceGroup" value="<?php if (isset($_GET['combo'])) {echo $data[3];} ?>" placeholder="150.20" required></p>
                    <p style="font-size: 15px;">Extranjero<input maxlength="2" type="text" class="form-control" style="width: 100%;" name="foreign" value="<?php if (isset($_GET['combo'])) {echo $data[4];} ?>" placeholder="Si" required></p>
                    <p style="font-size: 15px;">Tickets Disponibles<input maxlength="4" type="text" class="form-control" style="width: 100%;" name="ticketsAvailables" value="<?php if (isset($_GET['combo'])) {echo $data[5];} ?>" placeholder="155" required></p>
                    <p style="font-size: 15px;">Localización<textarea class="form-control" style="width: 100%; max-width: 100%; min-width: 100%;" name="locate" required><?php if (isset($_GET['combo'])) {echo $data[7];} ?></textarea></p>
                    <p style="font-size: 15px;">Link hacia el Blog<input type="text" class="form-control" style="width: 100%;" name="link_blog" value="<?php if (isset($_GET['combo'])) {echo $data[11];} ?>" placeholder="http://worldtrravel.blogspot.com.es/"></p>
                    <p>Archivo pdf<input type="text" class="form-control" name="pdf_name" style="width: 100%; margin-bottom: 5px;" value="<?php if (isset($_GET['combo'])) {echo $data[12];} ?>"  onfocus = "this.blur()"><input class="pdf" type="file" name="pdf" style="margin-bottom: 11px;" max="5"></p>
                    <p style="font-size: 15px;">Subcategoría
                        <select class="form-control combobox" style="width: 100%;" name="availableOption" required>

                            <!--CARGO LOS DATOS EN EL COMBOBOX, LOS CUALES ESTAN EN LA TABLA SUBTYPE-->

                            <option>Opciones Disponibles</option>
                            <?php

                                $select2 = $db->query("select id, subclass from subtype where id_type=5");

                                foreach ($select2 as $row) {
                                    echo "<option value='" . $row['id'] . "'>" . $row['subclass'] ."</option>";
                                }

                             ?>
                        </select>
                    </p>
                    <p style="font-size: 15px;">Descuento
                        <select class="form-control combobox_descuento" style="width: 100%;" name="availableOption2" required>

                            <!--CARGO LOS DATOS EN EL COMBOBOX, LOS CUALES ESTAN EN LA TABLA DISCOUNT-->

                            <option>Opciones Disponibles</option>
                            <?php

                                $select2 = $db->query("select id, name from discount");

                                foreach ($select2 as $row) {
                                    echo "<option value='" . $row['id'] . "'>" . $row['name'] ."</option>";
                                }

                             ?>
                        </select>
                    </p>

                        <p>Imágenes<input class="file" type="file" name="archive[]" style="margin-bottom: 11px;" enctype="multipart/form-data" max="5" multiple required></p>

                        <?php

                            if (isset($_GET['combo'])) {

                                echo "<div class='img_container' style='width: 167;'><i class='fa fa-plus new_image' aria-hidden='true'></i></div>";

                                //CON LA CONSULTA OBTENGO LOS DATOS DE LAS IMAGENES PARA MOSTRAR SUS DATOS Y PODER VISIONARLAS

                                $images = $db->query("select multimedia.id as multimedia_id, name from multimedia inner join product_generic on multimedia.id_product_generic=product_generic.id where product_generic.id='" . $data[6] . "'");

                                foreach ($images as $img) {

                                    //EN LOS INPUTS TEXT LOS DEJO EN OCULTO Y GUARDO EN ELLOS EL ID Y E NOMBRE QUE TIENE LA IMAGEN EN LA TABLA MULTIMEDIA

                                    echo "<div class='img_container' style='width: 167;'>
                                            <input type='text' name='id_multimedia' value='" . $img['multimedia_id'] . "' style='display: none;' disabled>
                                            <input type='text' name='image' value='" . $img['name'] . "' style='display: none;' disabled>
                                            <img class='image2' src='uploads/" . $img['name'] . "'>
                                        </div>";
                                }

                            }

                         ?>


                    <!--EN EL INPUT TEXT GUARDO EL VALOR DEL ID QUE TIENE EL PRODUCTO EN LA TABLA PRODUCT_GENERIC-->

                    <input type="text" name="product_generic_id" value="<?php if (isset($_GET['combo'])) {echo $data[6];} ?>" style="display: none;">

                    <div class="button_container">
                        <input class="btn btn-info options add" type="submit" style="background-color: #3c8dbc;" name="add" value="Nuevo Producto">
                        <input class="btn btn-info options" type="submit" style="background-color: #3c8dbc;" name="delete" value="Borrar Producto">
                        <input class="btn btn-info options" type="submit" style="background-color: #3c8dbc;" name="update" value="Actualizar">
                        <input class="btn btn-info del_img options" type="submit" style="background-color: #3c8dbc; display: none;" name="delete_image" value="Borrar Imagen">
                        <input class="btn btn-info add_img options" type="submit" style="background-color: #3c8dbc; display: none;" name="add_image" value="Añadir Imagen">
                    </div>
                </form>


<?php

    if (isset($_POST['update'])) {

        //REALIZO ESTO PARA PONER ESTE CAMPO CON UN VALOR ACEPTADO POR LA BASE DE DATOS

        if ($_POST['foreign']=="Si" || $_POST["foreign"]=="si") {
            $foreign = "1";
        }
        else {
            $foreign = "0";
        }

        //GUARDO LA FECHA Y LA HORA ACTUAL EN UNA VARIABLE, PARA LUEGO INSERTAR ESA VARIABLEEN LA BASEDE DATOS

        $now = date("Y-m-d") . " " . date("G:i:s");


        $result4 = $db->query("update product_generic set link_blog='" . $_POST['link_blog'] . "', title='" . str_replace(",", "", $_POST['title']) . "', description='" . $_POST['description'] . "', `foreign`='" . $foreign . "', price_individual='" . $_POST['priceIndividual'] . "', price_group='" . $_POST['priceGroup'] . "', tickets_availables='" . $_POST['ticketsAvailables'] . "', locate='" . $_POST['locate'] . "', updated='" . $now . "' WHERE id=" . $_POST['product_generic_id'] . "");


        $result5 = $db->query("update product_generic set id_subtype=" . $_POST['availableOption'] . " where id=" . $_POST['product_generic_id'] . "");

        $result6 = $db->query("update product_generic set id_discount=" . $_POST['availableOption2'] . " where id=" . $_POST['product_generic_id'] . "");




        //CARGO EN UNA VARIABLE LA RUTA DE LA IMAGEN PARA BORRARLA SI EXISTE

        $file_pdf = "uploads/" . $_POST['pdf_name'];

        //SI EL ARCHIVO EXISTE LO ELIMINA

        if (file_exists($file_pdf)){
            unlink($file_pdf);
        }

        //COPIO EL ARCHIVO SELECCIONADO PARA CAMIARLO POR EL QUE ACABO DE BORRAR DE LA CARPETA DEL SERVIDOR

        move_uploaded_file($_FILES['pdf']['tmp_name'],"uploads/".$_FILES['pdf']['name']);
        $pdf_name=$_FILES['pdf']['name'];

        $result7 = $db->query("update product_generic set link_pdf='" . $_FILES['pdf']['name'] . "' WHERE id=" . $_POST['product_generic_id'] . "");




        //SI LA IMAGEN CARGADA DA ALGUN ERROR SE MUESTRA UN ERROR

        if($_FILES['archive']["error"][0] > 0){
        ?>
            <script type="text/javascript">
                window.location = "offer.php?error=1.1";
            </script>
        <?php
        }
        else {

            //CARGO EN UNA VARIABLE LA RUTA DE LA IMAGEN

            $file = "uploads/" . $_POST['image'];

            //SI EL ARCHIVO EXISTE LO ELIMINA

            if (file_exists($file)){
                unlink($file);
            }

            //COPIO EL ARCHIVO SELECCIONADO PARA CAMIARLO POR EL QUE ACABO DE BORRAR DE LA CARPETA DEL SERVIDOR

            move_uploaded_file($_FILES['archive']['tmp_name'][0],"uploads/".$_FILES['archive']['name'][0]);
            $archive_name=$_FILES['archive']['name'][0];
        }

        //ACTUALIZO EN LA TABLA MULTIMEDIA EL NOMBRE DEL ARCHIVO QUE ACABO DE ELIMINAR POR EL QUE ACABADE SUBIR EL USUARIO

        $update_multimedia = $db->query("update multimedia set name='" . $_FILES['archive']['name'][0] . "', updated='" . $now . "' where name='" . $_POST['image'] . "'");

        //OBTENGO EL ULTIMO ID DE LA IMAGEN QUE ACABO DE INSERTAR

        $select = $db->query("select id from multimedia where name='" . $_FILES['archive']['name'][0] . "'");

        foreach ($select as $row) {
            $id_multimedia = $row['id'];
        }

        //ACTUALIZO LOS NOMBRES DE LA TABLA PICTURE QUE COINCIDAN CON EL ID_MULTIMEDIA OBTENIDO EN LACONSULTA ANTERIOR

        $update_picture = $db->query("update picture set slug='" . $_FILES['archive']['name'][0] . "', Alt='" . $_FILES['archive']['name'][0] . "', updated='" . $now . "' where id_multimedia='" . $_POST['id_multimedia'] . "'");


        //SI ALGUNA DE LAS ACTUALIZACIONES O INSERCIONES DA ERROR MUESRTA UN MENSAJE DE ERROR

        if (!$result4 || !$result5 || !$result6 || !$update_multimedia || !$update_picture) {
            ?>
                <script language=JavaScript>
                    window.location = "offer.php?error=0";
                </script>
            <?php
        }

        //EN CASO CONTRARIO, REFRESCA LA PAGINA Y EL ADMIN PUEDA VER LOS CABMBIOS REALIZADOS

        else {
        ?>
            <script language=JavaScript>
                window.location = "offer.php?message=2";
            </script>
        <?php
        }
    }




    if (isset($_POST['delete'])) {

        //BORRO TODO LO RELACIONADO CON EL PRODUCTO QUE HE SELECCCIONADO, INCLUYENDO LAS IMAGENES GUARDADAS EN EL SERVIDOR Y EL PDF DE ESE PRODUCTO

        $select_pdf_to_delete = $db->query("select link_pdf from product_generic where id=" . $_POST["product_generic_id"]);

        foreach ($select_pdf_to_delete as $delete_pdf) {
            $pdf_del = "uploads/" . $delete_pdf['link_pdf'];
            unlink($pdf_del);
        }

        $select_images_to_delete = $db->query("select id, name from multimedia where id_product_generic=" . $_POST["product_generic_id"]);

        foreach ($select_images_to_delete as $delete_image) {
            $db->query("delete from multimedia where id=" . $delete_image['id']);
            $db->query("delete from picture where Alt='" . $delete_image['name'] . "'");

            $img_del = "uploads/" . $delete_image['name'];
            unlink($img_del);
        }

        $db->query("delete from offer where id_product_generic='" . $_POST["product_generic_id"] . "'");
        $db->query("delete from product_generic where id='" . $_POST["product_generic_id"] . "'");


        ?>
            <script type='text/javascript'>
                window.location = "offer.php?message=3";
            </script>

        <?php
    }




    if (isset($_POST['add'])) {

        //SI EL NUMERO DE ARCHIVOS QUE SE INTENTA SUBIR ES MAYOR DE 30 PRODUCIRA UN MENSAJE DE ERROR Y EVITARA LA INSERCION DE LOS DATOS Y LAS IMAGENES

        if (count($_FILES['archive']['size']) > 30) {
            ?>
                <script language=JavaScript>
                    window.location = "offer.php?error=1";
                </script>
            <?php
        }

        //SI EL NUMERO DE ARCHIVOS ES INFERIOR REALIZALAS CONSULTAS

        else{

            //GUARDO LA FECHA Y LA HORA ACTUAL EN UNA VARIABL,PARA USARLO EN LAS INSERCIONES

            $now = date("Y-m-d") . " " . date("G:i:s");

            //REALIZO ESTO PARA PONER ESTE CAMPO CON UN VALOR ACEPTADO POR LA BASE DE DATOS

            if ($_POST['foreign']=="Si" || $_POST["foreign"]=="si") {
                $foreign = "1";
            }
            else {
                $foreign = "0";
            }

            //REALIZO LA INSERCION DE LOS DATOS INTRODUCIDOS EN EL FORMULARIO

            $insert = $db->query("insert into product_generic values (NULL, '" . $_POST['availableOption'] . "', '" . $_POST['availableOption2'] . "', '" . str_replace(",", "", $_POST['title']) . "', '" . $_POST['description'] . "', '" . $foreign . "', '" . $_POST['locate'] . "', '" . $_POST['priceIndividual'] . "', '" . $_POST['priceGroup'] . "', '" . $_POST['ticketsAvailables'] . "', '" . $_POST['link_blog'] . "', '" . $_FILES['pdf']['name'] . "', '" . $now . "', '" . $now . "')");


            move_uploaded_file($_FILES['pdf']['tmp_name'],"uploads/".$_FILES['pdf']['name']);


            //OBTENGO EL ULTIMO ID INSERTADO EN LA TABLA PRODUCT_GENERIC

            $select_id_product = $db->query("select id from product_generic order by id desc limit 1");

            foreach ($select_id_product as $result) {
                $id_product = $result['id'];
            }

            //INSERTO EN LA TABLA OFFER LA RELACIÓN CON PRODUCT_GENERIC

            $insert_offer = $db->query("insert into offer values (NULL, '" . $id_product . "', '" . $now . "', '" . $now . "')");


            //AQUI REALIZO EL PROCESO DE GUARDAR LA IMAGEN EN EL SERVIDOR
            //CUENTO EL NUMERO DE ARCHIVOS QUE HAY, PARA REALIZAR ESE NUMERO MAXIMO DE INSERCIONES EN LAS TABLAS

            for ($i=0; $i < count($_FILES['archive']['name']) ; $i++) {

                //GUARDO EL NOMBRE DEL ARCHIVO INTRODUCIDO Y ELNOMBRE DESU ARCHIVO TEMPORAL

                $archive_name = $_FILES['archive']['name'][$i];
                $temp_archive = $_FILES['archive']['tmp_name'][$i];

                //COPIO EL ARCHIVO SELECCIONADO A LA CARPETA DEL SERVIDOR

                move_uploaded_file($temp_archive,"uploads/" . $archive_name);

                //INSERTO LOS DATOS DE LA IMAGEN EN LA TABLA MULTIMEDIA

                $insert_multimedia = $db->query("insert into multimedia values(null, '" . $id_product . "', '". $archive_name . "', 'backend/uploads/', '" . $now . "', '" . $now . "')");

                //OBTENGO EL ULTIMO ID INSERTADO EN LA TABLA MULTIMEDIA

                $select_id_multimedia = $db->query("select id from multimedia order by id desc limit 1");

                foreach ($select_id_multimedia as $result) {
                    $id_multimedia = $result['id'];
                }

                //INSERTO LOS DATOS DE LA IMAGEN EN LA TABLA PICTURE

                $insert_picture = $db->query("insert into picture values(null, '". $id_multimedia . "', '" . $archive_name . "', '" . $archive_name . "', '" . $now . "', '" . $now . "')");

                //SI HAY ALGUN ERROR EN LAS INSERCIONES ANTERIROES, SE MUESTRA UN MENSAJE DE ERROR

                if (!$insert || !$insert_picture || !$insert_multimedia || !$insert_offer) {
                    ?>
                        <script language=JavaScript>
                            window.location = "offer.php?error=0";
                        </script>
                    <?php

                }


                //SI NO HAY NINGUN ERROR REDIRECCIONA

                else {
                    ?>
                        <script language=JavaScript>
                            window.location = "offer.php?message=1";
                        </script>
                    <?php
                }
            }

        }
    }


    if (isset($_POST['add_image'])) {

        //CUENTO EL NUMERO DE IMAGENES QUE HAY EN LA TABLA MULTIMEDIA QUE ESTE RELACIONADO CON ESTE PRODUCTO

        $query_count_image = $db->query("select count(id) as num from multimedia where id_product_generic=" . $_POST['product_generic_id']);

        foreach ($query_count_image as $row) {
            $quantity = $row['num'];
        }

        //SI LA CANTIDAD QUE DEVUELVE LA CONSULTA ES SUPERIOR A 30 REENVIA A UN MENSAJE DE ERROR

        if ($quantity > 30) {
            ?>
                <script language=JavaScript>
                    window.location = "offer.php?error=1";
                </script>
            <?php
        }

        //SI ES MENOR DE 30 REALIZA LAS CONSULTAS

        else {

            //GUARDO LA FECHA Y LA HORA ACTUAL EN UNA VARIABL,PARA USARLO EN LAS INSERCIONES

            $now = date("Y-m-d") . " " . date("G:i:s");

            //EN FUNCION DE LA CANTIDAD DE ARCHIVOS QUE SE HAYAN SELECCIONADO REALIZARA ESE NUMERO DE VUELTAS EL BUCLE

            for ($i=0; $i < count($_FILES['archive']['name']) ; $i++) {

                //GUARDO EL NOMBRE DEL ARCHIVO INTRODUCIDO Y ELNOMBRE DESU ARCHIVO TEMPORAL

                $archive_name = $_FILES['archive']['name'][$i];
                $temp_archive = $_FILES['archive']['tmp_name'][$i];

                //COPIO EL ARCHIVO SELECCIONADO A LA CARPETA DEL SERVIDOR

                move_uploaded_file($temp_archive,"uploads/" . $archive_name);

                //INSERTO LOS DATOS DE LA IMAGEN EN LA TABLA MULTIMEDIA

                $insert_multimedia2 = $db->query("insert into multimedia values(null, '" . $_POST['product_generic_id'] . "', '". $archive_name . "', 'backend/uploads/', '" . $now . "', '" . $now . "')");

                //OBTENGO EL ULTIMO ID INSERTADO EN LA TABLA MULTIMEDIA

                $select_id_multimedia = $db->query("select id from multimedia order by id desc limit 1");

                foreach ($select_id_multimedia as $result) {
                    $id_multimedia = $result['id'];
                }

                //INSERTO LOS DATOS DE LA IMAGEN EN LA TABLA PICTURE

                $insert_picture2 = $db->query("insert into picture values(null, '". $id_multimedia . "', '" . $archive_name . "', '" . $archive_name . "', '" . $now . "', '" . $now . "')");

                //SI HAY ALGUN ERROR EN LAS INSERCIONES ANTERIROES, SE MUESTRA UN MENSAJE DE ERROR

                if (!$insert_multimedia2 || !$insert_picture2) {
                    ?>
                        <script language=JavaScript>
                            window.location = "offer.php?error=0";
                        </script>
                    <?php
                }

                //SI NO HAY NINGUN ERROR REDIRECCIONA

                else {
                    ?>
                        <script language=JavaScript>
                            window.location = "offer.php?message=4";
                        </script>
                    <?php
                }
            }
        }


    }




    if (isset($_POST['delete_image'])) {

        //CUENTO EL NUMERO DE IMAGENES QUE HAY EN LA TABLA MULTIMEDIA QUE ESTE RELACIONADO CON ESTE PRODUCTO PARA DETERMINAR QUE SI SOLO QUEDA UNA IMAGEN NO SE PUEDA BORRAR LA IMAGEN

        $query_count_image2 = $db->query("select count(id) as num from multimedia where id_product_generic=" . $_POST['product_generic_id']);

        foreach ($query_count_image2 as $row) {
            $quantity2 = $row['num'];
        }

        //SI EL RESULTADO DEVUELTO ES UN 1 NO SE PUEDE BORRAR, POR LOQUE MUESTRAMENSAJE DE ERROR

        if ($quantity2==1) {
            ?>
                <script language=JavaScript>
                    window.location = "offer.php?error=3";
                </script>
            <?php
        }

        //SI EL RESULTADO ES SUPERIRO A UNO LA BORRA

        else {
            //BORRO SOLO LA IMAGEN SELECCIONADA Y LOS DATOS QUE ESTEN RELACIONADOS CON ELLA, ADEMAS DE ELIMINARLA DEL SERVIDOR

            $db->query("delete from picture where id_multimedia='" . $_POST['id_multimedia'] . "'");
            $db->query("delete from multimedia where name='" . $_POST['image'] . "'");

            $file = "uploads/" . $_POST['image'];

            if (file_exists($file)){
                unlink($file);
            }

            ?>
                <script language=JavaScript>
                    window.location = "offer.php?message=5";
                </script>
            <?php
        }
    }
 ?>
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php include('scripts.php'); ?>
