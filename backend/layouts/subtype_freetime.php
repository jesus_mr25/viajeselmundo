<?php

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

    date_default_timezone_set("Europe/Madrid");

?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS TIPOS DE OCIO-->

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Tipos de Ocio</h3>
    </div><!-- /.box-header -->

    <div class="box-body">

    <form method="get" action="subtypes.php">

        <!--INTRODUZCO EN EL COMBOBOX TODAS LAS CLASES QUE HAY EN LA TABLA-->

        <select class="combo_freetime form-control" name="combo_freetime" id="id" onchange="submit()" style="width: 200px;">
            <option value="0">Seleccione una opción</option>
            <?php
                $result  = $db->query("select id, subclass from subtype where id_type=3 order by id");

                foreach ($result as $fila) {
                    echo "<option value='" . $fila['id'] . "'>" . $fila['subclass'] . "</option>";
                }
            ?>
        </select>
    </form>


        <?php

            //realizo la consulta a base de datos para obtener los elementos que necesito. por defecto no muestro ninguno, pero despues voy mostrando el que este seleccionado en el combobox.

            if (isset($_GET['combo_freetime'])) {
                $combo_freetime = $_GET['combo_freetime'];

                $result3_freetime = $db->query("select id, subclass from subtype where id=" . $combo_freetime);

                if ($combo_freetime!=0) {
                    foreach ($result3_freetime as $fila_freetime) {
                        $data_freetime[0] = $fila_freetime['id'];
                        $data_freetime[1] = $fila_freetime['subclass'];
                    }
                }
                 else {
                    $data_freetime[0] = "";
                    $data_freetime[1] = "";
                }

            }

         ?>

        <!--le paso los valores del get para que cuando cambie el combobox cambie tambien en los inputs-->

        <form class="form-ticket" method="post" action="subtypes.php" name="form1">
            <p style="font-size: 15px;">Subcategoria<input type="text" class="form-control" style="width: 100%;" name="class_freetime" value="<?php if (isset($_GET['combo_freetime'])) {echo $data_freetime[1];} ?>" placeholder="Nueva entrada" required></p>
            <p><input type="text" name="id_freetime" value="<?php if (isset($_GET['combo_freetime'])) {echo $data_freetime[0];} ?>" style="display: none;"></p>

            <input class="btn btn-info add" type="submit" style="background-color: #3c8dbc;" name="add_freetime" value="Nueva Entrada">
            <input class="btn btn-info update" type="submit" style="background-color: #3c8dbc;" name="update_freetime" value="Actualizar">
            <input class="btn btn-info delete" type="submit" style="background-color: #3c8dbc;" name="delete_freetime" value="Borrar">
        </form>

        <?php

        if (isset($_GET['error_freetime'])) {
            ?>
              <script type="text/javascript">
                alert("Error al insertar en la tabla. Revisa los datos introducidos.");
              </script>
            <?php
        }

        if (isset($_POST['update_freetime'])) {

            //THIS OPERATION IS TO FLIP THE FIELD "foreign" FOR ADD IN THE DATABASE

            $now_freetime = date("Y-m-d") . " " . date("G:i:s");

            $result4_freetime = $db->query("update subtype set subclass='" . $_POST['class_freetime'] . "', updated='" . $now_freetime . "' WHERE id=" . $_POST['id_freetime'] . "");

            //TO REFRESH THE PAGE, AND ADMIN CAN SEE HOW CHANGE THE DATABASE WITHOUT PRESS F5

            if (!$result4_freetime) {
                ?>
                    <script language=JavaScript>
                        window.location = "subtypes.php?error_freetime";
                    </script>
                <?php
            }
            else {
                ?>
                    <script language=JavaScript>
                      window.location = "subtypes.php?visible_freetime=&message=2";
                    </script>
                <?php
            }
        }

        if (isset($_POST['delete_freetime'])) {
          $check = $db->query("select * from product_generic where id_subtype = (select id from subtype where subclass= '" . $_POST['class_freetime'] . "')");
          if (mysqli_num_rows($check) > 0) {
            ?>
              <style type="text/css">
                .show_error_delete {
                  display: inherit !important;
                }
              </style>
            <?php
          }
          else{
            $db->query("delete from subtype where subtype.id='" . $_POST["id_freetime"] . "'");

              ?>
                  <script type='text/javascript'>
                    window.location = "subtypes.php?visible_freetime=&message=3";
                  </script>
              <?php
          }
        }

        if (isset($_POST['add_freetime'])) {
            $now_freetime = date("Y-m-d") . " " . date("G:i:s");

            $insert_freetime = $db->query("insert into subtype values (null, 3, '" . $_POST['class_freetime'] . "', '" . $now_freetime . "', '" . $now_freetime . "')");


            if (!$insert_freetime) {
                ?>
                    <script language=JavaScript>
                        window.location = "subtypes.php?error_freetime";
                    </script>
                <?php

            }
            else {
                ?>
                    <script language=JavaScript>
                      window.location = "subtypes.php?visible_freetime=&message=1";
                    </script>
                <?php
            }
        }
                ?>
    </div>
</div>
