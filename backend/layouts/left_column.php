<?php

  //CON ESTAS CONDICIONES DEPENDIENDO DE LA PAGINA EN LA QUE ESTE POSICIONADO, MANTENDRE ACTIVA UNOS ENLACE U OTROS



  if (basename($_SERVER['PHP_SELF']) == "admin.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_admin").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "tickets.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_tickets").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "campus.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_campus").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "sports.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_sports").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "discounts.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_discounts").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "freetime.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_freetime").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "offer.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_offer").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "subtypes.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_subtypes").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "user.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_user").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "buy.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_buy").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "view_top_destiny.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview2").addClass("active");
            $("#td_view_top_destiny").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "view_slider_principal.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview2").addClass("active");
            $("#td_view_slider_principal").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "tab_last_minute.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview2").addClass("active");
            $("#td_tab_last_minute").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "tab_best_offer.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview2").addClass("active");
            $("#td_tab_best_offer").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "tab_full_include.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview2").addClass("active");
            $("#td_tab_full_include").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "tab_last_news.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview2").addClass("active");
            $("#td_tab_last_news").addClass("active");
          });
        </script>
     <?php
  }elseif (basename($_SERVER['PHP_SELF']) == "token_discount.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_token_discount").addClass("active");
          });
        </script>
     <?php
  }
  elseif (basename($_SERVER['PHP_SELF']) == "suscriber.php") {
     ?>
        <script type="text/javascript">
          $(document).ready(function(){
            $("#treeview").addClass("active");
            $("#td_suscriber").addClass("active");
          });
        </script>
     <?php
  }


   ?>




<!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="dist/img/user-men-2-5121.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php get_name_admin($admin, $db); ?></p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <li class="header">Opciones</li>
            <!-- Optionally, you can add icons to the links -->
              <li id="treeview" class="treeview">
                <a href="#"><i class="fa fa-table"></i> <span>Menús</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li id="td_admin"><a href="admin.php">Administrador</a>
                  <li id="td_tickets"><a href="tickets.php">Entradas</a></li>
                  <li id="td_campus"><a href="campus.php">Campus</a></li>
                  <li id="td_token_discount"><a href="token_discount.php">Códigos Promocionales</a></li>
                  <li id="td_sports"><a href="sports.php">Deportes</a></li>
                  <li id="td_discounts"><a href="discounts.php">Descuentos</a></li>
                  <li id="td_freetime"><a href="freetime.php">Ocio</a></li>
                  <!--<li id="td_offer"><a href="offer.php">Ofertas</a></li>-->
                  <li id="td_suscriber"><a href="suscriber.php">Suscriptores</a></li>
                  <li id="td_subtypes"><a href="subtypes.php">Tipos de Productos</a></li>
                  <li id="td_user"><a href="user.php">Usuario</a></li>
                  <li id="td_buy"><a href="buy.php">Ventas Realizadas</a></li>
                </ul>
              </li>
              <li id="treeview2" class="treeview">
                <a href="#"><i class="fa fa-cogs" aria-hidden="true"></i><span>Modificaciones Index </span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  <li id="td_view_slider_principal"><a href="view_slider_principal.php"><span>Galería Slider Principal</span></a></li>
                  <li id="td_view_top_destiny"><a href="view_top_destiny.php"><span>Productos Destinos Top</span></a></li>
                  <li id="td_tab_last_minute"><a href="tab_last_minute.php"><span>Pestaña Último Minuto</span></a></li>
                  <li id="td_tab_best_offer"><a href="tab_best_offer.php"><span>Pestaña Mejores Ofertas</span></a></li>
                  <li id="td_tab_full_include"><a href="tab_full_include.php"><span>Pestaña Todo Incluido</span></a></li>
                  <li id="td_tab_last_news"><a href="tab_last_news.php"><span>Pestaña Últimas Noticias</span></a></li>
                </ul>
              </li>
              <li><a href="mailbox.php"><i class="fa fa-envelope"></i> <span>Servidor de Correo</span></a></li>-->
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>
