<?php 

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO, PARA MÁS ADELANTE USAR LAS FUNCIONES DATE()

    date_default_timezone_set("Europe/Madrid");

    //CARGO EN UN ARRAY LOS TIPOS DE PRODUCTOS QUE HAY

    $table = "";
    $tables = array("ticket", "camp", "sport", "freetime", "offer");
    $host = $_SERVER["HTTP_HOST"];


    //SI HAY GET DE TOP1 CARGARE OBTENDRE DE LA CONSULTA TODOS LOS IDS DE LAS TABLAS QUE COINCIDAN CON EL GET DEL ID DEL PRODUCTO. EN EL MOMENTO EN EL QUE ENCUENTRE LA TABLA EN LA QUE ESTA EL PRODUCTO, SALDRA DEL BUCLE, PUES UN PRODUCTO NO ESTA REPETIDO EN LAS OTRAS TABLAS

    if (isset($_GET['top1'])) {

      ?>

        <!--SI HAY GET MOSTRARÁ EL MENSAJE OCULTO INDICANDO EL MENSAJE-->

        <script type="text/javascript">
          $(document).ready(function(){
            $(".message_info_top_destiny").prop("hidden", false);
          });
          
        </script>

      <?php

      for ($i=0; $i < 5; $i++) { 
        $select_table1 = $db->query("select id from " . $tables[$i] . " where id_product_generic=" . $_GET['top1']);

        if (mysqli_num_rows($select_table1) > 0) {
            $table = $tables[$i];

            break;
        }
      }
   
      //OBTENGO EL TITULO Y LA DESCRIPCION DEL PRODUCTO QUE COINCIDA CON EL GET DE TOP1

      $select = $db->query("select title, description from product_generic where id=" . $_GET['top1']);

      foreach ($select as $row1) {
        $title1 = $row1['title'];
        $description1 = $row1['description'];
      }

      //Y MODIFICO EL PRODUCTO DE LA TABLA TOP_DESTINY QUE SERA EL QUE TIENE EL ID 1

      $db->query("update top_destiny set id_product_generic=" . $_GET['top1'] . ", link_product='http://" . $host . "/show_product.php?product_generic_id=" . $_GET['top1'] . "&table=" . $table . "' where id=1");
    }



    //SI HAY GET DE TOP2 CARGARE OBTENDRE DE LA CONSULTA TODOS LOS IDS DE LAS TABLAS QUE COINCIDAN CON EL GET DEL ID DEL PRODUCTO. EN EL MOMENTO EN EL QUE ENCUENTRE LA TABLA EN LA QUE ESTA EL PRODUCTO, SALDRA DEL BUCLE, PUES UN PRODUCTO NO ESTA REPETIDO EN LAS OTRAS TABLAS


    if (isset($_GET['top2'])) {

      for ($i=0; $i < 5; $i++) { 
        $select_table2 = $db->query("select id from " . $tables[$i] . " where id_product_generic=" . $_GET['top2']);

        if (mysqli_num_rows($select_table2) > 0) {
            $table = $tables[$i];

            break;
        }
      }
   
      //OBTENGO EL TITULO Y LA DESCRIPCION DEL PRODUCTO QUE COINCIDA CON EL GET DE TOP2

      $select = $db->query("select title, description from product_generic where id=" . $_GET['top2']);

      foreach ($select as $row1) {
        $title1 = $row1['title'];
        $description1 = $row1['description'];
      }

      //Y MODIFICO EL PRODUCTO DE LA TABLA TOP_DESTINY QUE SERA EL QUE TIENE EL ID 2

      $db->query("update top_destiny set id_product_generic=" . $_GET['top2'] . ", link_product='http://" . $host . "/show_product.php?product_generic_id=" . $_GET['top2'] . "&table=" . $table . "' where id=2");
    }



    //SI HAY GET DE TOP3 CARGARE OBTENDRE DE LA CONSULTA TODOS LOS IDS DE LAS TABLAS QUE COINCIDAN CON EL GET DEL ID DEL PRODUCTO. EN EL MOMENTO EN EL QUE ENCUENTRE LA TABLA EN LA QUE ESTA EL PRODUCTO, SALDRA DEL BUCLE, PUES UN PRODUCTO NO ESTA REPETIDO EN LAS OTRAS TABLAS


    if (isset($_GET['top3'])) {

      for ($i=0; $i < 5; $i++) { 
        $select_table3 = $db->query("select id from " . $tables[$i] . " where id_product_generic=" . $_GET['top3']);

        if (mysqli_num_rows($select_table3) > 0) {
            $table = $tables[$i];

            break;
        }
      }
   
      //OBTENGO EL TITULO Y LA DESCRIPCION DEL PRODUCTO QUE COINCIDA CON EL GET DE TOP3

      $select = $db->query("select title, description from product_generic where id=" . $_GET['top3']);

      foreach ($select as $row1) {
        $title1 = $row1['title'];
        $description1 = $row1['description'];
      }

      //Y MODIFICO EL PRODUCTO DE LA TABLA TOP_DESTINY QUE SERA EL QUE TIENE EL ID 3

      $db->query("update top_destiny set id_product_generic=" . $_GET['top3'] . ", link_product='http://" . $host . "/show_product.php?product_generic_id=" . $_GET['top3'] . "&table=" . $table . "' where id=3");
    }
 

?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS LOS DESTINOS TOP DEL INDEX-->
 
 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Destinos Top
          <small>Selección de los datos que se posicionarán en los destinos Top de la Página Principal.</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="callout callout-success message_info_top_destiny" hidden>
          <h4>¡Imagen cambiada!</h4>

          <p>Cambio de imagen realizado satisfactoriamente.</p>
        </div>

        <!--DESTINOS TOP PARTE IZQUIERDA-->

        <!-- Your Page Content Here -->

        <div class="box">
            <div class="box-header">
              <h4>Destino Top Izquierdo</h4>
            </div><!-- /.box-header -->

            <div class="box-body">

            <!--CARGO TODOS LOS PRODUCTOS EN EL COMBOBOX-->
            
              <form action="view_top_destiny.php" method="get">
                <select class="form-control top1" name="top1" onchange="view_top_destiny_location()" style="width: 200px;">
                  <?php 
                      $result  = $db->query("select id, title from product_generic");
                      
                      foreach ($result as $fila) {
                          echo "<option value='" . $fila['id'] . "'>" . $fila['title'] . "</option>";
                          
                      } 
                  ?>
                </select>
              </form>

            </div>
        </div>





        <!--DESTINOS TOP PARTE CENTRAL-->


        <div class="box">
            <div class="box-header">
              <h4>Destino Top Central</h4>
            </div><!-- /.box-header -->

            <div class="box-body">

            <!--CARGO TODOS LOS PRODUCTOS EN EL COMBOBOX-->
            
              <form action="view_top_destiny.php" method="get">
                <select class="form-control top2" name="top2" onchange="view_top_destiny_location()" style="width: 200px;">
                  <?php 
                      $result  = $db->query("select id, title from product_generic");
                      
                      foreach ($result as $fila) {
                          echo "<option value='" . $fila['id'] . "'>" . $fila['title'] . "</option>";
                          
                      } 
                  ?>
                </select>
              </form>

            </div>
        </div>

        <div class="box">
            <div class="box-header">
              <h4>Destino Top Derecho</h4>
            </div><!-- /.box-header -->




        <!--DESTINOS TOP PARTE DERECHA-->


        <div class="box-body">

            <!--CARGO TODOS LOS PRODUCTOS EN EL COMBOBOX-->
            
              <form action="view_top_destiny.php" method="get">
                <select class="form-control top3" name="top3" onchange="view_top_destiny_location()" style="width: 200px;">
                  <?php 
                      $result  = $db->query("select id, title from product_generic");
                      
                      foreach ($result as $fila) {
                          echo "<option value='" . $fila['id'] . "'>" . $fila['title'] . "</option>";
                          
                      } 
                  ?>
                </select>
              </form>

            </div>                  
        </div>
    </section>
</div>

<?php include('scripts.php'); ?>