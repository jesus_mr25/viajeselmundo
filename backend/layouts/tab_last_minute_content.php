<?php 

    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO, PARA MÁS ADELANTE USAR LAS FUNCIONES DATE()

    date_default_timezone_set("Europe/Madrid");

    //CARGO EN UN ARRAY LOS TIPOS DE PRODUCTOS QUE HAY

    $table = "";
    $tables = array("ticket", "camp", "sport", "freetime", "offer");
    $host = $_SERVER["HTTP_HOST"];


    //SI HAY GET DE TOP1 CARGARE OBTENDRE DE LA CONSULTA TODOS LOS IDS DE LAS TABLAS QUE COINCIDAN CON EL GET DEL ID DEL PRODUCTO. EN EL MOMENTO EN EL QUE ENCUENTRE LA TABLA EN LA QUE ESTA EL PRODUCTO, SALDRA DEL BUCLE, PUES UN PRODUCTO NO ESTA REPETIDO EN LAS OTRAS TABLAS

    if (isset($_GET['last_minute1'])) {

      ?>

        <!--SI HAY GET MOSTRARÁ EL MENSAJE OCULTO INDICANDO EL MENSAJE-->

        <script type="text/javascript">
          $(document).ready(function(){
            $(".message_info_top_destiny").prop("hidden", false);
          });
          
        </script>

      <?php

      for ($i=0; $i < 5; $i++) { 
        $select_table1 = $db->query("select id from " . $tables[$i] . " where id_product_generic=" . $_GET['last_minute1']);

        if (mysqli_num_rows($select_table1) > 0) {
            $table = $tables[$i];

            break;
        }
      }


      //Y MODIFICO EL PRODUCTO DE LA TABLA TOP_DESTINY QUE SERA EL QUE TIENE EL ID 1

      $db->query("update tab_last_minute set id_product_generic=" . $_GET['last_minute1'] . ", link_product='http://" . $host . "/show_product.php?product_generic_id=" . $_GET['last_minute1'] . "&table=" . $table . "' where id=1");
    }




    //SI HAY GET DE TOP2 CARGARE OBTENDRE DE LA CONSULTA TODOS LOS IDS DE LAS TABLAS QUE COINCIDAN CON EL GET DEL ID DEL PRODUCTO. EN EL MOMENTO EN EL QUE ENCUENTRE LA TABLA EN LA QUE ESTA EL PRODUCTO, SALDRA DEL BUCLE, PUES UN PRODUCTO NO ESTA REPETIDO EN LAS OTRAS TABLAS


    if (isset($_GET['last_minute2'])) {

      for ($i=0; $i < 5; $i++) { 
        $select_table2 = $db->query("select id from " . $tables[$i] . " where id_product_generic=" . $_GET['last_minute2']);

        if (mysqli_num_rows($select_table2) > 0) {
            $table = $tables[$i];

            break;
        }
      }
   
      //Y MODIFICO EL PRODUCTO DE LA TABLA TOP_DESTINY QUE SERA EL QUE TIENE EL ID 2

      $db->query("update tab_last_minute set id_product_generic=" . $_GET['last_minute2'] . ", link_product='http://" . $host . "/show_product.php?product_generic_id=" . $_GET['last_minute2'] . "&table=" . $table . "' where id=2");
    }
 

?>

<!--ESTA ES LA PAGINA DONDE ESTAN LAS OPCIONES PARA EDITAR LOS LOS DESTINOS TOP DEL INDEX-->
 
 <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          Pestaña de Último minuto
          <small>Selección de los datos que se posicionarán en la pestaña último minuto de la Página Principal.</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="callout callout-success message_info_top_destiny" hidden>
          <h4>¡Producto cambiado!</h4>

          <p>Cambio de producto realizado satisfactoriamente.</p>
        </div>



        <!--PESTAÑA PRODUCTO SUPERIOR-->



        <!-- Your Page Content Here -->

        <div class="box">
            <div class="box-header">
              <h4>Producto Superior</h4>
            </div><!-- /.box-header -->

            <div class="box-body">

            <!--CARGO TODOS LOS PRODUCTOS EN EL COMBOBOX-->
            
              <form action="tab_last_minute.php" method="get">
                <select class="form-control last_minute1" name="last_minute1" onchange="tab_last_minute()" style="width: 200px;">
                  <?php 
                      $result  = $db->query("select id, title from product_generic");
                      
                      foreach ($result as $fila) {
                          echo "<option value='" . $fila['id'] . "'>" . $fila['title'] . "</option>";
                          
                      } 
                  ?>
                </select>
              </form>

            </div>
        </div>





        <!--PESTAÑA PRODUCTO INFERIOR-->


        <div class="box">
            <div class="box-header">
              <h4>Producto Inferior</h4>
            </div><!-- /.box-header -->

            <div class="box-body">

            <!--CARGO TODOS LOS PRODUCTOS EN EL COMBOBOX-->
            
              <form action="tab_last_minute.php" method="get">
                <select class="form-control last_minute2" name="last_minute2" onchange="tab_last_minute()" style="width: 200px;">
                  <?php 
                      $result  = $db->query("select id, title from product_generic");
                      
                      foreach ($result as $fila) {
                          echo "<option value='" . $fila['id'] . "'>" . $fila['title'] . "</option>";
                          
                      } 
                  ?>
                </select>
              </form>

            </div>
        </div>

      
    </section>
</div>

<?php include('scripts.php'); ?>