                   </tbody>
                </table>
                <!-- /.table -->
              </div>
              <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    </section>
    <!-- /.content -->
  </div>

<script type="text/javascript">
  $(document).ready(function(){
    $('.checkbox-toggle').click(function(){
      if($('input').is(':checked')){
        $('input').prop('checked', false);
        $('.fa-check-square-o').prop('hidden', true);
        $('.fa-square-o').prop('hidden', false);
      }
      else {
        $('input').prop('checked', true);
        $('.fa-check-square-o').prop('hidden', false);
        $('.fa-square-o').prop('hidden', true);
      }
    });
  });
</script>
