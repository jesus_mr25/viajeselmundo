<?php

  //ESTO ES PARA AUMENTAR EL TIEMPO DE EJECUCION DE LOS SCRIPTS Y NO DEN ERRORES DE TIEMPO EXCEDIDO EN LA EJECUCION

  ini_set('max_execution_time', 300);


  if (basename($_SERVER['PHP_SELF']) == "mailbox.php") {
     $hostname = '{imap-mail.outlook.com:993/imap/ssl}INBOX';
     $name = "Bandeja de Entrada";
     $box = "INBOX";
  }
  elseif (basename($_SERVER['PHP_SELF']) == "sent.php") {
     $hostname = '{imap.gmail.com:993/imap/ssl}[Gmail]/Enviados';
     $name = "Mensajes Enviados";
     $box = "Sent";
  }
  elseif (basename($_SERVER['PHP_SELF']) == "draft.php") {
     $hostname = '{imap.gmail.com:993/imap/ssl}[Gmail]/Borradores';
     $name = "Borradores";
     $box = "Drafts";
  }
  elseif (basename($_SERVER['PHP_SELF']) == "trash.php") {
     $hostname = '{imap.gmail.com:993/imap/ssl}[Gmail]/Papelera';
     $name = "Mensajes Eliminados";
     $box = "Deleted";
  }
  elseif (basename($_SERVER['PHP_SELF']) == "junk.php") {
     $hostname = '{imap.gmail.com:993/imap/ssl}[Gmail]/Destacados';
     $name = "Mensajes Destacados";
     $box = "Junk";
  }


  $username = 'jesus.martin.ruiz@hotmail.com';
  //$password = 'qwertyuiop1234567890..';
  $password = 'Vaneyjesus251109.';

  //ABRO LA CONEXION DEL SERVIDOR DE CORREO IMAP

  $inbox = imap_open($hostname,$username,$password);

  //ORDENO LOS CORREOS POR FECHA DE LLEGADA ESTADO LOS MAS RECIENTE AL PRINCIPIO

  $emails = imap_sort($inbox, SORTARRIVAL, 1);

  //CUENTO EL NUMERO DE CORREOS QUE TENGO EN EL BUZON ACTUAL

  $num_emails = imap_num_msg($inbox);


?>

<div style="min-height: 946px;" class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Servidor de correo
        <small>Hay <?php echo $num_emails; ?> correos en <?php echo $name; ?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <a href="new_message.php" class="btn btn-primary btn-block margin-bottom">Redactar Mensaje</a>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Carpetas</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li class="mailbox"><a href="mailbox.php"><i class="fa fa-inbox"></i> Bandeja de entrada</a></li>
                <li class="sent"><a href="sent.php"><i class="fa fa-envelope-o"></i> Enviados</a></li>
                <li class="drafts"><a href="draft.php"><i class="fa fa-file-text-o"></i> Borradores</a></li>
                <li class="junk"><a href="#"><i class="fa fa-filter"></i> Marcados</a>
                </li>
                <li class="trash"><a href="trash.php"><i class="fa fa-trash-o"></i> Eliminados</a></li>
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $name; ?></h3>

            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-controls">
                <!-- Check all button -->
                <div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm checkbox-toggle" disabled><i class=" fa fa-square-o"></i><i class=" fa fa-check-square-o" hidden></i></button>
                  <button type="button" class="btn btn-default btn-sm button-delete" disabled><i class="fa fa-trash-o"></i></button>
                  <button type="button" onclick="window.location.reload()" class="btn btn-default btn-sm button-reload" disabled><i class="fa fa-refresh"></i></button>
                  <div class="dropdown" id="button_menu">
                   <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" disabled>Mover Mensaje
                   <span class="caret"></span></button>
                   <ul class="dropdown-menu">
                     <li <?php if($box=="INBOX"){echo "hidden";} ?>><a href="#">Bandeja de Entrada</a></li>
                     <li <?php if($box=="Sent"){echo "hidden";} ?>><a href="#">Enviados</a></li>
                     <li <?php if($box=="Drafts"){echo "hidden";} ?>><a href="#">Borradores</a></li>
                     <li <?php if($box=="Junk"){echo "hidden";} ?>><a href="#">Marcados</a></li>
                   </ul>
                  </div>
                </div>
                <!-- /.btn-group -->
                <div class="pull-right">
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm prev" disabled><i class="fa fa-chevron-left"></i></button>
                    <button type="button" class="btn btn-default btn-sm next" disabled><i class="fa fa-chevron-right"></i></button>
                  </div>
                  <!-- /.btn-group -->
                </div>
                <!-- /.pull-right -->
              </div>
              <div class="table-responsive mailbox-messages">
                <table class="table table-hover table-striped">
                  <tbody>

<?php

if (basename($_SERVER['PHP_SELF']) == "mailbox.php") {
  ?>
    <script type="text/javascript">
      $(".mailbox").addClass("active");
    </script>
  <?php
}
elseif (basename($_SERVER['PHP_SELF']) == "sent.php") {
  ?>
    <script type="text/javascript">
      $(".sent").addClass("active");
    </script>
  <?php
}
elseif (basename($_SERVER['PHP_SELF']) == "draft.php") {
  ?>
    <script type="text/javascript">
      $(".drafts").addClass("active");
    </script>
  <?php
}
elseif (basename($_SERVER['PHP_SELF']) == "trash.php") {
  ?>
    <script type="text/javascript">
      $(".trash").addClass("active");
    </script>
  <?php
}
elseif (basename($_SERVER['PHP_SELF']) == "junk.php") {
  ?>
    <script type="text/javascript">
      $(".junk").addClass("active");
    </script>
  <?php
}

 ?>
