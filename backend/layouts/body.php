<!--ESTA ES LA PAGINA DONDE ESTA LA PAGINA PRINCIPAL DEL PANEL DE CONTROL-->


<?php

  if (isset($_POST['subject'])) {

    //A CONTINUACION, ENVIO UN CORREO AL DESARROLLADOR PARA SOLUCIONAR CUALQUIER DUDA O PROBLEMA REFERENTE A UN ADMINISTRADOR

    require('../phpmailer/PHPMailerAutoload.php');

    $subject = $_POST['subject'];
    $body = $_POST['body'];


    //CREO UN NUEVA CLASE DE PHPMAILER
    //LE PASO EL CORREO DEL RECEPTOR Y EL NOMBRE DEL REMITENTE
    //LE PASO EL CORREO DEL RECEPTOR
    //LE PONGO EL ASUNTO

    //LE PASO EL CUERPO DEL MENSAJE
    //LE PASO LA CODIFICACION DEL LENGUAJE
    //LE PASO EL TIPO DE MENSAJE
    //Y LO ENVIO

    $mailer = new PHPMailer();
    $mailer->setFrom('no-reply@viajeselmundo.es','Panel Administracion - Viajes El Mundo');
    $mailer->addAddress('jesus.martin.ruiz@hotmail.com');
    $mailer->Subject = $subject;

    $mailer->Body = $body;
    $mailer->CharSet = 'utf-8';
    $mailer->mailer = 'mail';

    //SI NO SE ENVIA EL MENSSAJE SE MOSTRARA UN MENSAJE DE DISCONFORMIDAD

    if (!$mailer->send()) {
      ?>

        <style type="text/css">
          .error_pass {
            display: inherit !important;
          }
        </style>

      <?php
    }
    else {
      ?>

        <style type="text/css">
          .message_ok {
            display: inherit !important;
          }
        </style>

      <?php
    }
  }
?>




<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Bienvenido al panel de administración de Viajes El Mundo.
            <small>Si necesita contactar con el desarrollador de la web pulse <a style="text-decoration: none; cursor: pointer;" data-toggle='modal' data-target='#myModal'>AQUI</a></small>
          </h1>

        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Your Page Content Here -->

          <div class="callout callout-success message_ok" style="display: none;">
            <h4>¡Estupendo!</h4>
            <p>Su mensaje ha sido enviado correctamente al desarrollador de la Web. Se pondrán en contacto con usted lo más pronto posible.</p>
          </div>

          <div class="callout callout-danger error_pass" style="display: none;">
            <h4>¡Ups, algo salió mal!</h4>
            <p>El email no se ha podido enviar debido a un error inesperado, inténtelo más tarde.</p>
          </div>

          <div class="modal fade" id="myModal" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                  <div class="modal-content" style="padding: 22px;">

                      <div class="modal-header">
                          <h2>Nuevo Mensaje de Correo
                         <button type="button" class="close" data-dismiss="modal">X</button></h2>
                      </div>
                      <div class="modal-body">

                      <form action="index.php" method="post">
                        <p><input type="text" class="form-control" placeholder="Asunto" name="subject" required></p>
                        <p><textarea class="form-control" style="width: 100%; max-width: 100%; min-width: 100%;" placeholder="Mensaje" name="body" required></textarea></p>
                        <p><input class="add btn btn-info" type="submit" name="add" value="Enviar" style="background-color: #3c8dbc;"></p>
                      </form>

                      </div>
                  </div>
              </div>
          </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
