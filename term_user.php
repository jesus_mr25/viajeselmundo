<!--AQUI ES DONDE TENGO CONTEMPLADO LOS TERMINOS DEL AVISO LEGAL DE RECOGIDA DE LOS DTOS DEL USUARIO-->



<!================= HEADER ====================>

	<?php include('layouts/header.php'); ?>

	<div class="main" style="padding: 50px;">
		<h1 style="font-size: 35px;">Aviso Legal</h1><br>
		<h1 style="font-size: 25px;">Normas generales</h1><br>

		<p>Por el mero uso de esta web, el usuario manifiesta su aceptación sin reservas de las presentes condiciones generales.</p><br>
		<p>El usuario se compromete a hacer un uso adecuado de los contenidos y servicios de esta página web y a no emplearlos para incurrir en actividades ilícitas, ilegales o contrarias a la buena fe y al orden público; difundir contenidos o propaganda de carácter racista, xenófobo, pornográfico-ilegal, de apología del terrorismo o atentatorio contra los derechos humanos.</p><br>
		<p>Todos los derechos de propiedad intelectual del contenido de esta página web, su diseño gráfico y sus códigos fuente, son titularidad exclusiva de Viajes El Mundo o de la Viajes El Mundo que presta servicios a esta. Por tanto queda prohibida su reproducción, distribución, comunicación pública y transformación, total o parcial, sin la autorización expresa del propietario.</p><br>
		<p>Viajes El Mundo no será responsable de los fallos que pudieran producirse en las comunicaciones, incluido el borrado, transmisión incompleta o retrasos en la remisión, no comprometiéndose tampoco a que la red de transmisión esté operativa en todo momento.</p><br><br>

		<h1 style="font-size: 25px;">Ley de protección de datos</h1><br>
		<p>El objetivo de nuestra política de privacidad es proteger la calidad y la integridad de sus datos personales.</p><br>
		<p>Los datos serán objeto del tratamiento automatizado e incorporados a nuestra base de datos, de los que Viajes El Mundo será titular y único responsable.</p><br>
		<p>Toda la información que usted nos proporcione será reservada exclusivamente para uso de las actividades mercantiles de Viajes El Mundo. Ninguna persona ajena podrá tener acceso a los datos.</p><br>
		<p>En ningún supuesto Viajes El Mundo dará a conocer a ningún tercero información que identifique a sus usuarios individualmente, sin autorización expresa de dichos usuarios.</p><br>
		<p>De conformidad con la Ley Orgánica 15/1999 de 13 de diciembre de Protección de Datos de Carácter Personal (LOPD), los datos suministrados por el Usuario quedarán incorporados en un fichero automatizado, el cual será procesado exclusivamente para la finalidad descrita. Estos se recogerán a través de los mecanismos correspondientes, los cuales solo contendrán los campos imprescindibles para poder prestar el servicio requerido por el Usuario.</p><br>
		<p>Los datos de carácter personal serán tratados con el grado de protección adecuado, según el Real Decreto 994/1999 de 11 de junio, tomándose las medidas de seguridad necesarias para evitar su alteración, pérdida, tratamiento o acceso no autorizado por parte de terceros que lo puedan utilizar para finalidades distintas para las que han sido solicitados al Usuario. Los datos de carácter personal objeto del tratamiento automatizado sólo podrán ser cedidos, según consta en el artículo 11 de la Ley Orgánica 15/1999 de 13 de diciembre, para el cumplimiento de fines directamente relacionados con las funciones legítimas del cedente y del cesionario con el previo consentimiento del afectado.</p>
	</div>


	<!================ BLUE FOOTER ===================>

	<?php include('layouts/blue_footer.php'); ?>

	<!================ SOCIAL FOOTER ===================>

	<?php include('layouts/social_footer.php'); ?>

	<!================ BUTTON FOOTER ===================>

	<?php include('layouts/button_bottom.php'); ?>

</body>
</html>

<?php

	$db->close();

 ?>
