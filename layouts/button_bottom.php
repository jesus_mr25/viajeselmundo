<!--ESTE ES EL BOTON PARA HACER SCROLL A LA PARTE DE ARRIBA DE LA PAGINA-->


<div class='goTop'>
	<a href='index.php#Top'><span><span/></a>
</div>

<?php

if (isset($_GET['cookie'])) {
	$_SESSION['CookieAccept'] = "CookieAccept";
}

if (isset($_SESSION['CookieAccept'])) {
	?>
	<style type="text/css">
		#overbox3 {
			display: none;
		}
	</style>
	<?php
}
 ?>

	<div id="overbox3">
	    <div id="infobox3">
	        <p>Esta web utiliza cookies para obtener datos estadísticos de la navegación de sus usuarios. Si continúas navegando consideramos que aceptas su uso.
	        <a href="cookies.php">Más información</a>
	        <a class="cookie_accept" style="cursor:pointer;">Cerrar</a></p>
	    </div>
	</div>



<!--CON ESTO AL HACER CLICK EN CERRAR EL MENSAJE DE CCOKIES NO SE CERRARA MIENTRA HAYA SESION-->




<!--CON pageYOffset COMPRUEBO EN QUE LUGAR DEL EJE Y ESTA LA PAGINA. ESTO ES PARA QUE EN EL MOMENTO EN EL QUE EL VALOR DEL pageYOffset SEA SUPERIOR A 500 PX APARECE EL BOTONCITO Y CUANDO SEA MENOR DESAPARECE. ADEMAS, AL HACER CLICK IRA A LA PARTE DE ARRIBA DELA WEB CON UNA TRANSICION DE 600-->

<script type='text/javascript'>
	$(window).scroll(function(){
	    if (window.pageYOffset <= 500) {
	        $('.goTop').css("display","none");
	    } else {
	        $('.goTop').css("display","block");
	    }
	});

	$('.goTop').click(function(){
	    $('body,html').animate({scrollTop : 0}, 600);
	    return false;
	});

</script>



<!--ESTE SCRIPT ES PARA OCULTAR EL MENSAJE DE LAS COOKIES-->

<script type="text/javascript">

    $('.cookie_accept').click(function(){
      $('#overbox3').css("display", "none");
			location.href="<?php echo basename($_SERVER['PHP_SELF']); ?>?cookie=ok";
    });

</script>
