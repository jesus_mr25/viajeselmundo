<?php

	if ($_POST) {

		//SI HAY POST Y ADEMAS EL POST ES USERNAME Y PASSWORD CUMPLE LA CONDICION

		if ($_POST['username'] && $_POST['password']) {

			//OBTENGO USUARIOS Y CONTRASEÑAS DE USUARIOS Y ADMINISTRADORES

			$query = "select username, pass from user where active=1 and username='" . $_POST['username'] . "' and pass='" . hash("sha256" , $_POST['password']) . "'";
			$query2 = "select username, pass from admin where active=1 and username='" . $_POST['username'] . "' and pass='" . hash("sha256" , $_POST['password']) . "'";

			//REALIZO LAS DOS CONSULTAS

			$result = $db->query($query);
			$result2 = $db->query($query2);

			//SI EL RESULTADO DE LA CONSULTA DE COMPROBAR SI ES UN USUARIO DEVUELVE ALGUN RESULTADO, CUMPLE LA CONDICION Y REDIRECCIONA AL INDEX

			if (mysqli_num_rows($result) > 0) {
				$_SESSION['username'] = $_POST['username'];
				?>
					<script type="text/javascript">
						window.location.href = "index.php";
					</script>
				<?php
			}

			//SI EL RESULTADO DE LA CONSULTA DE COMPROBAR SI ES UN ADMIN DEVUELVE ALGUN RESULTADO, CUMPLE LA CONDICION Y REDIRECCIONA AL INDEX

			elseif (mysqli_num_rows($result2) > 0) {
				$_SESSION['username'] = $_POST['username'];
				?>
					<script type="text/javascript">
						window.location.href = "index.php";
					</script>
				<?php
			}

			//EN CASO DE QUE NO SE CUMPLA NINGUNO DE LOS DOS CASOS ANTERIORES MOSTRARA EL SIGUIENTE MENSAJE

			else {
				?>

				<!--EN CASO DE QUE NO ESTE EL USUARIO INTRODUCIDO NI EN LATABLA DE LOS USUARIOS NI EN LA TABLA DE LOS ADMINISTRADORES, RETORNARA UN MENSAJE DE DISCONFORMIDAD-->

				<style type="text/css">
					.callout-warning {
						display: inherit !important;
					}
				</style>

				<?php
			}
    	}
	}

 ?>

 <!--ESTO ES EL CONTENEDOR DEL LOGIN-->

<div class="site-login">
	<div class="callout callout-warning" style="display: none;">
		<p>Usuario o contraseña incorrecto, por favor vuelva a intentarlo.</p>
	</div>
	<h1 class="session">Inicio de sesión</h1>
	<p>Por favor, rellena este formulario para iniciar sesión o regístrese <a href="register.php" style="text-decoration: none; cursor: pointer; color: #BED8F1;"><b>AQUÍ</b></a>:</p>

	<form id="login-form" class="form-horizontal" action="login.php" method="post">

	    <div class="form-group field-loginform-username">
			<label class="col-lg-1 control-label" for="loginform-username">Usuario</label>
			<div class="col-lg-3 login_form">
				<input id="loginform-username" class="form-control" name="username" type="text" required>
			</div>
		</div>

		<div class="form-group field-loginform-password">
			<label class="col-lg-1 control-label" for="loginform-password">Contraseña</label>
			<div class="col-lg-3 login_form">
				<input id="loginform-password" class="form-control" name="password" type="password" required>
			</div>
		</div>

	    <div class="form-group">
	        <div class="col-lg-offset-1 col-lg-11">

	            <button type="submit" class="btn btn-primary button" name="login-button">Iniciar Sesión</button>
	            <p class="incorrect"><a href="renew_pass.php">¿Has olvidado tus credenciales?</a></p>
	        </div>
	    </div>
	</form>
</div>
