<?php
//INICIO LA SESION Y HAGO LA CONEXION A LA BASE DE DATOS
	session_start();


	include('backend/conexionDB.php');

 ?>


<!DOCTYPE html>
<html lang="es">
<head>
	<title>Viajes El Mundo</title>
	<meta charset="utf-8">

<!--===============INCLUYO LOS ARCHIVOS CSS Y LOS ARCHIVOS JS ==================-->

	<?php include('everycss.php'); ?>
	<?php include('everyjs.php'); ?>

</head>
<body class="page1">
<header>

    <!--MENU IZQUIERDO PARA MOSTRAR EL MENU-->

	<ul class="menu2 cleafix2">
      	<li class='parent2'> <a class="fa fa-bars"></a>
        	<div class="sublist2">
            	<ul id="active_web_edition2" class='children2'>
	            	<li><a href="index.php">Inicio</a></li>
	            	<li><a href="tickets.php">Entradas</a></li>
	            	<li><a href="sports.php">Deportes</a></li>
	            	<li><a href="freetime.php">Ocio</a></li>
					<li><a href="campus.php">Campus</a></li>
					<li><a href="travel.php">Viajes</a></li>
					<li><a href="http://worldtrravel.blogspot.com.es/" target="_blank">Blog</a></li>
					<li class="two_lines"><a href="organize.php">Nosotros te lo organizamos</a></li>
            	</ul>
        	</div>
    	</li>
    </ul>

    <!--MENU DERECHO PARA INICIAR SESION-->

	<ul class="menu cleafix">
      <li class='parent'> <a class="glyphicon glyphicon-user"></a>
        <div class="sublist">
            <ul class='children'>
            <?php

            //SI NO HAY NINGUNA SESION INICIADA MUESTRA INICIAR SESION

				if (!isset($_SESSION['username'])) {
					?>
		            <li><a href="login.php">Iniciar Sesión</a></li>
								<li><a href="register.php">Regístrate</a></li>
		        	<?php


		        	//SI NO HAY SESION ACTIVA EL ID DEL USUARIO AL COMPRAR SERA NULL, ESTO SERA USADO PARA PASAR EL ID DEL USUARIO PARA EL TICKET DE LA COMPRA

		        	$id_user = "NULL";


				}

				//SI HAY SESION MUESTRA CERRAR SESION. ADEMAS, SI EL USUARIO LOGEADO ES UN ADMIN, TAMBIEN APARECE LA OPCION DE ABRIR EL PANEL DE CONTROL

				else {

					//CON LA SIGUIENTE CONSULTA OBTENGO EL ID DEL USUARIO PARA AÑADIRLO EN LA TABLA BUY CUANDO ESTE HAYA COMPRADO

					$result2 = $db->query("select id from user where active=1 and username='" . $_SESSION['username'] . "'");

					foreach ($result2 as $row) {
						$id_user = $row['id'];
					}

					if ($result2->num_rows > 0) {
						?>
			            <li class="backend_user"><a href="backend_user/index.php">Panel de Usuario</a></li>
						<?php
					}


					$result = $db->query("select username from admin where active=1 and username='" . $_SESSION['username'] . "'");

					if ($result->num_rows > 0) {
						?>
			            <li class="backend"><a href="backend/index.php">Panel de Control</a></li>
						<?php
					}


					?>
					<li><a href="logout.php">Cerrar Sesión</a></li>
					<?php

				}
				?>
            </ul>
		</div>
      </li>
    </ul>

	<!--ENLACES PARA NNAVEGAR POR LOS DISTINTOS SITIOS DE LA WEB-->

	<div class="container_99">
		<div class="menu_block">
			<ul id="active_web_edition" class="sf-menu">

				<li class="current"><a href="index.php">Inicio</a></li>
				<li><a href="tickets.php">Entradas</a></li>
				<li><a href="sports.php">Deportes</a></li>
				<li><a href="travel.php">Viajes</a></li>
				<li style="border-radius: 100%;"><a href="index.php"><img class="image_logo" src="images/logo.png" alt="Viajes El Mundo"></a></li>
				<li><a href="campus.php">Campus</a></li>
				<li><a href="freetime.php">Ocio</a></li>
				<li><a class="more-product" style="cursor: pointer;">Otros Productos</a>
					<ul class="sub_sfmenu" style="display: none;">
						<li><a href="http://worldtrravel.blogspot.com.es/" target="_blank">Blog</a></li>
						<li><a href="organize.php">Nosotros te lo organizamos</a></li>
					</ul>
				</li>
				<li><a href="contact.php">Contacto</a></li>
			</ul>
		</div>

		<div class="clear"></div>
	</div>
</header>


<!--ESTE SCRIPT ES PARA MOSTRAR Y OCULTAR EL SUBMENU QUE TIENE EL MENU PRINCIPAL  (SI SE QUITA DE AQUI NO FUNCIONA)-->

<script type="text/javascript">
  $(".more-product").mouseover(function(){
    $(".sub_sfmenu").css("display", "");
  });

  $("html").click(function(){
    $(".sub_sfmenu").css("display", "none");
  });
</script>
