<!--ESTAS SERIA LA VENTANA DONDE APARECE EL TIPO DE AYUDA QUE SE OFRECE-->

<div class="grid_9 tickets">
	<h3>Te ayudamos con tu viaje</h3>
	<div class="tours">

		<p>En Viajes El Mundo somos especialistas en ofrecer las mejores opciones de entretenimiento para grupos, empresas o individuales.</p>

		<p>Sabemos lo difícil que es organizar un viaje en grupo o elegir el destino que encaja con tus posibilidades, por eso, nuestro personal especializado le aconsejará la mejor forma de realizar el viaje y le ayudará a planificarlo como usted quiere, de forma que sea un rotundo éxito. Y, por supuesto, todo preparado hasta el último detalle, con una atención personalizada y con la confianza, garantía y calidad que necesitas.</p>

		<p>Para poder llegar a realizar un viaje de empresa, grupo de amigos o individuales, en Viajes El Mundo no sólo le ayudamos en su planificación una vez aprobado el mismo, sino que ayudamos a analizar a priori la situación inicial nuestra labor será:</p>

		<p>- Ayudar a  rentabilizar al máximo el viaje.</p>

		<p>- Diseñar y estructurar el viaje.</p>

		<p>- Definir nuestras funciones como organizadores y las de la empresa.</p>

		<p>Y finalmente organización y desarrollo del viaje.</p>

		 <p>Pero lo más importante para nosotros es conseguir que el viaje cumpla su objetivo de eficiencia y eficacia como herramienta de labor comercial.</p>

		 <br><br>

		<h3 style="font-size: 25px;">PARA EMPRESAS</h3>

		<p>Nuestro departamento le ahorrará todo ese tiempo en buscar y organizar lo que buscas. Contamos con todas las ideas habidas y por haber para hacer de tu viaje, evento y ocio en general, la mejor desconexión para tus empleados.</p>

		<p>1. Contamos con las mejores salas y teatros de Madrid a coste reducido para organizar tu evento.</p>

		<p>2. Ocio para tus empleados muy distintos al resto de cosas que se organizan.</p>

		<p>3. Organización y planteamiento de eventos deportivos para tus empleados.</p>

		<p>4. Posibilidad de realizar cualquier tipo de reserva: avión, hotel, traslados, entradas, espectáculos, restaurantes, etc.</p>

		<br><br>

		<h3 style="font-size: 25px;">VIAJES PARA GRUPOS E INDIVIDUALES</h3>

		 <p>Contamos con infinidad de ideas de ocio para grupos o individuales. Desde reservas en musicales, teatros, deportes o incluso cualquier viaje que se os ocurra hacer, y con todo lo necesario para su estancia.</p>

		<p>Nuestro personal os aconsejará y organizará todo lo que necesitáis siempre a un coste económico.</p>

		<p>Todo lo hacemos con gusto y sin compromisos. ¿Hablamos?</p>



		<br>

		<a href="contact.php" class="btn bt1">Solicitar información</a>
	</div>
</div>
