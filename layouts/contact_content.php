<!--ESTA ES LA PAGINA QUE COMPRENDE EL CONTACTO-->

<div class="grid_9 stay_here">
    <h3>Aquí estamos</h3>
    <div class="map">
        <figure class="img_inner fleft">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3203.6919748151363!2d-4.536898584716133!3d36.58563997999321!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd72fcecf9d6e003%3A0x51c1ec622ab75c6f!2sCalle+Salinas%2C+7%2C+29630+Benalm%C3%A1dena%2C+M%C3%A1laga!5e0!3m2!1ses!2ses!4v1464390519902" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </figure>


    </div>
</div>


<div class="grid_3 contact">
    <h3>Contáctanos</h3>
    <p>
        Si deseas contactarnos por favor rellena el siguiente formulario, gracias.
    </p>

    <form id="contact-form" action="contact.php#message" method="post" role="form">
        <input name="_csrf" value="SUJhRjB3dnY6MC4kYCFbGQ53LSFmKAI.KykbLEQUNTQaNw5zaQMOLg==" type="hidden">
        <br>
        <div class="form-group field-contactform-email">
            <label class="control-label" for="contactform-email">Tipos de ayuda</label>
            <select name="help_type" class="form-control" required>
                <option></option>
                <option value="Ayuda">Ayuda general</option>
                <option value="Entrada">Entrada</option>
                <option value="Deporte">Deporte</option>
                <option value="Ocio">Ocio</option>
                <option value="Campus">Campus</option>
                <option value="Ofertas">Ofertas</option>
                <option value="Organizamos">Te lo organizamos</option>
            </select>
            <p class="help-block help-block-error"></p>
        </div>

        <div class="form-group field-contactform-name">
            <label class="control-label" for="contactform-name">Nombre</label>
            <input id="contactform-name" class="form-control" name="name" type="text" required>
            <p class="help-block help-block-error"></p>
        </div>

        <div class="form-group field-contactform-email">
            <label class="control-label" for="contactform-email">Email</label>
            <input id="contactform-email" class="form-control" name="email" type="email" required>
            <p class="help-block help-block-error"></p>
        </div>

        <div class="form-group field-contactform-phone">
            <label class="control-label" for="contactform-phone">Teléfono</label>
            <input id="contactform-phone" class="form-control" name="phone" type="text" required>
            <p class="help-block help-block-error"></p>
        </div>

        <div class="form-group field-contactform-body">
            <label class="control-label" for="contactform-body">Mensaje</label>
            <textarea id="contactform-body" class="form-control textarea" name="body" rows="6" required></textarea>
            <p class="help-block help-block-error"></p>
        </div>

        <div class="form-group field-contactform-verifycode">
            <label class="control-label" for="contactform-verifycode">Código de verificación</label>
            <div class="row">
                <div class="col-lg-6">
                    <input id="contactform-verifycode" class="form-control text_captcha" name="captcha" type="text" required>
                </div>

                <div class="col-lg-6">
                    <img id="contactform-verifycode-image" src="images/captcha.png" alt="">
                </div>
            </div>
            <p class="help-block help-block-error"></p>
        </div>

        <div class="form-group">
          <a name="message"></a>
            <button type="submit" class="btn btn-primary send" name="contact-button">Enviar</button>
        </div>
    </form>
</div>



<div class="clear"></div>

<?php

//SI HAY POST CUMPLE LA CONDICION Y ENVIA EL MENSAJE USANDO LA LIBRERIA PHPMAILER

    if ($_POST) {

        //SI EL VALOR INTRODUCIDO EN EL INPUT DEL CAPTCHA ES IGUAL AL DE LA CONDICION, SE CUMPLLE LA CONDICION Y ENVIARIA EL MENSAJE

        if ($_POST['captcha'] == "rugifud") {
            require('phpmailer/PHPMailerAutoload.php');

            $sms="Mensaje del formulario de contacto de Viajes El Mundo. \tNombre:" . $_POST['name'] . "\tEmail:" .  $_POST['email'] . "\tTelefono:" . $_POST['phone'] . "\nMensaje sobre" . $_POST['help_type'] . ": \n\t " . $_POST['body'];

            $matter = "Mensaje enviado por: " . $_POST['name'];

            $mailer = new PHPMailer();
            $mailer->setFrom('no-reply@viajeselmundo.com','Contacto - Viajes El Mundo');
            $mailer->addAddress('jesus.martin.ruiz@hotmail.com');
            $mailer->Subject = $matter;

            $mailer->Body = $sms;
            $mailer->CharSet = 'utf-8';
            $mailer->mailer = 'mail';

            if($mailer->send()) {
                ?>
                <h1 class="notice" style="font-size: 25px">Mensaje enviado</h1>
                <?php
            }
            else {
                ?>
                <h1 class="notice" style="font-size: 25px">Mensaje no enviado</h1>
                <?php
            }
        }
        else {
            ?>
            <h1 class="notice" style="font-size: 25px">Captcha erróneo</h1>
            <?php
        }
    }

 ?>
