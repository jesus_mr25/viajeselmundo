<div class="grid_9 tickets">
  <h3>Campus</h3>
  <div class="tours">

  <?php

  //SI EL VALOR DEVUELTO POR GET ES UNA DE LAS DOS CUMPLE LA CONDICION

  $select = "";

  if (isset($_GET['foreign']) || isset($_GET['price']) || isset($_GET['subcategory']) ) {

    //SI GET FOREIGN DEVUELVE UN VALOR VACIO Y HAY GET PRICE CUMPLE LA CONDICION

    if ($_GET['foreign']=="" && $_GET['price']=="" && $_GET['subcategory']=="") {

      //SI TODOS LOS CAMPOS ESTAN VACIOS LO MOSTRARA TODO

      $select = $db->query("select distinct product_generic.id as product_generic_id, subtype.id as subtype_id, title, description, `foreign`, locate, price_individual, price_group, tickets_availables, discount.id as discount_id, subclass, discount.name as discount_name, discount.discount as discount_discount from camp inner join product_generic on camp.id_product_generic=product_generic.id inner join multimedia on product_generic.id=multimedia.id_product_generic inner join picture on multimedia.id=picture.id_multimedia inner join discount on product_generic.id_discount=discount.id inner join subtype on product_generic.id_subtype=subtype.id order by product_generic.updated desc");
    }


    elseif ($_GET['foreign']=="" && $_GET['price']=="" && $_GET['subcategory']!="") {

      //SI CUMPLE LA CONDICION HACE EL SELECT CON LA CONDICION DE LA SUBCATEGORIA

      $select = $db->query("select distinct product_generic.id as product_generic_id, subtype.id as subtype_id, title, description, `foreign`, locate, price_individual, price_group, tickets_availables, discount.id as discount_id, subclass, discount.name as discount_name, discount.discount as discount_discount from camp inner join product_generic on camp.id_product_generic=product_generic.id inner join multimedia on product_generic.id=multimedia.id_product_generic inner join picture on multimedia.id=picture.id_multimedia inner join discount on product_generic.id_discount=discount.id inner join subtype on product_generic.id_subtype=subtype.id where subtype.id=" . $_GET['subcategory']);
    }


    elseif ($_GET['foreign']=="" && $_GET['price']!="" && $_GET['subcategory']=="") {

      //SI CUMPLE LA CONDICION HACE EL SELECT CON LA CONDICION DEL EXTRANJERO

      $select = $db->query("select distinct product_generic.id as product_generic_id, subtype.id as subtype_id, title, description, `foreign`, locate, price_individual, price_group, tickets_availables, discount.id as discount_id, subclass, discount.name as discount_name, discount.discount as discount_discount from camp inner join product_generic on camp.id_product_generic=product_generic.id inner join multimedia on product_generic.id=multimedia.id_product_generic inner join picture on multimedia.id=picture.id_multimedia inner join discount on product_generic.id_discount=discount.id inner join subtype on product_generic.id_subtype=subtype.id " . $_GET['price']);
    }


    elseif ($_GET['foreign']!="" && $_GET['price']=="" && $_GET['subcategory']=="") {

      //SI CUMPLE LA CONDICION HACE EL SELECT CON LA CONDICION DEL EXTRANJERO

      $select = $db->query("select distinct product_generic.id as product_generic_id, subtype.id as subtype_id, title, description, `foreign`, locate, price_individual, price_group, tickets_availables, discount.id as discount_id, subclass, discount.name as discount_name, discount.discount as discount_discount from camp inner join product_generic on camp.id_product_generic=product_generic.id inner join multimedia on product_generic.id=multimedia.id_product_generic inner join picture on multimedia.id=picture.id_multimedia inner join discount on product_generic.id_discount=discount.id inner join subtype on product_generic.id_subtype=subtype.id where `foreign`=" . $_GET['foreign']);

    }


    elseif ($_GET['foreign']=="" && $_GET['price']!="" && $_GET['subcategory']!="") {

      //SI CUMPLE LA CONDICION HACE EL SELECT SIN LA CONDICION DEL EXTRANJERO

      $select = $db->query("select distinct product_generic.id as product_generic_id, subtype.id as subtype_id, title, description, `foreign`, locate, price_individual, price_group, tickets_availables, discount.id as discount_id, subclass, discount.name as discount_name, discount.discount as discount_discount from camp inner join product_generic on camp.id_product_generic=product_generic.id inner join multimedia on product_generic.id=multimedia.id_product_generic inner join picture on multimedia.id=picture.id_multimedia inner join discount on product_generic.id_discount=discount.id inner join subtype on product_generic.id_subtype=subtype.id where subtype.id=" . $_GET['subcategory'] . " " . $_GET['price']);
    }


    elseif ($_GET['foreign']!="" && $_GET['price']=="" && $_GET['subcategory']!="") {

      //SI CUMPLE LA CONDICION HACE EL SELECT SIN LA CONDICION DEL PRECIO

      $select = $db->query("select distinct product_generic.id as product_generic_id, subtype.id as subtype_id, title, description, `foreign`, locate, price_individual, price_group, tickets_availables, discount.id as discount_id, subclass, discount.name as discount_name, discount.discount as discount_discount from camp inner join product_generic on camp.id_product_generic=product_generic.id inner join multimedia on product_generic.id=multimedia.id_product_generic inner join picture on multimedia.id=picture.id_multimedia inner join discount on product_generic.id_discount=discount.id inner join subtype on product_generic.id_subtype=subtype.id where subtype.id=" . $_GET['subcategory'] . " and `foreign`= " . $_GET['foreign']);
    }


    elseif ($_GET['foreign']!="" && $_GET['price']!="" && $_GET['subcategory']=="") {

      //SI CUMPLE LA CONDICION HACE EL SELECT SIN LA CONDICION DE LA CATEGORIA

      $select = $db->query("select distinct product_generic.id as product_generic_id, subtype.id as subtype_id, title, description, `foreign`, locate, price_individual, price_group, tickets_availables, discount.id as discount_id, subclass, discount.name as discount_name, discount.discount as discount_discount from camp inner join product_generic on camp.id_product_generic=product_generic.id inner join multimedia on product_generic.id=multimedia.id_product_generic inner join picture on multimedia.id=picture.id_multimedia inner join discount on product_generic.id_discount=discount.id inner join subtype on product_generic.id_subtype=subtype.id where `foreign`=" . $_GET['foreign'] . " " . $_GET['price']);
    }


    elseif ($_GET['foreign']!="" && $_GET['price']!="" && $_GET['subcategory']!="") {

      //EN CASO CONTRARIO LO REALIZA CON EL PRECIO, CON EL EXTRANJERO Y CON EL SUBTIPO

      $select = $db->query("select distinct product_generic.id as product_generic_id, subtype.id as subtype_id, title, description, `foreign`, locate, price_individual, price_group, tickets_availables, discount.id as discount_id, subclass, discount.name as discount_name, discount.discount as discount_discount from camp inner join product_generic on camp.id_product_generic=product_generic.id inner join multimedia on product_generic.id=multimedia.id_product_generic inner join picture on multimedia.id=picture.id_multimedia inner join discount on product_generic.id_discount=discount.id inner join subtype on product_generic.id_subtype=subtype.id  where `foreign`=" . $_GET['foreign'] . " and subtype.id=" . $_GET['subcategory'] . " " . $_GET['price']);
    }


    //GUARDO EN VARIABLES EL CONTENIDO DE LOS DESCUENTOS

    $cont = 1;
    foreach ($select as $row) {
      $price_individual_with_discount = ((100-$row['discount_discount'])/100)*$row['price_individual'];
      $price_group_with_discount = ((100-$row['discount_discount'])/100)*$row['price_group'];


      if ($cont==1) {
        $imagenes = $db->query("select route, name from multimedia where id_product_generic=" . $row['product_generic_id'] . " limit 1");

        foreach ($imagenes as $images) {
          $route_img = $images['route'];
          $name_img = $images['name'];
        }



        ?>

        <div id="special" class="grid_4 alpha grid_special">
          <div class="tour">
            <img src="<?php echo $route_img . $name_img; ?>" alt="<?php echo $name_img; ?>" class="img_inner fleft">
            <div class="extra_wrapper">
              <p class="text1"><?php echo $row['title']; ?></p>

              <?php

                if ($row['discount_id']!=1) {
                  echo "<p class='price'>Precio Individual <span>Por " . $price_individual_with_discount . " €</span></p>
                        <p class='price'>Precio de Grupo <span>Por " . $price_group_with_discount . " €</span></p>";
                }
                else {
                  echo "<p class='price'>Precio Individual <span>Por " . $row['price_individual'] . " €</span></p>
                        <p class='price'>Precio de Grupo <span>Por " . $row['price_group'] . " €</span></p>";
                }

               ?>

            </div>
            <form method="get" action="show_product.php">
              <input type="submit" class="btn btn-info button_show" value="Detalles">
              <input type="text" name="product_generic_id" value="<?php echo $row['product_generic_id']; ?>" hidden>
              <input type="text" name="table" value="camp" hidden>
            </form>
          </div>
        </div>

        <?php

          }
          else {

            //CON ESTA CONSULTA MUESTRO SOLO LA PRIMERA IMAGEN DE CADA PRODUCTO

              $imagenes2 = $db->query("select route, name from multimedia where id_product_generic=" . $row['product_generic_id'] . " limit 1");

              foreach ($imagenes2 as $images2) {
                $route_img2 = $images2['route'];
                $name_img2 = $images2['name'];
              }

         ?>

        <div id="special" class="grid_4 omega grid_special">
          <div class="tour">
            <img src="<?php echo $route_img2 . $name_img2; ?>" alt="<?php echo $name_img2; ?>" class="img_inner fleft">
            <div class="extra_wrapper">
              <p class="text1"><?php echo $row['title']; ?></p>

              <?php

                if ($row['discount_id']!=1) {
                  echo "<p class='price'>Precio Individual <span>Por " . $price_individual_with_discount . " €</span></p>
                        <p class='price'>Precio de Grupo <span>Por " . $price_group_with_discount . " €</span></p>";
                }
                else {
                  echo "<p class='price'>Precio Individual <span>Por " . $row['price_individual'] . " €</span></p>
                        <p class='price'>Precio de Grupo <span>Por " . $row['price_group'] . " €</span></p>";
                }

               ?>

            </div>
            <form method="get" action="show_product.php">
              <input type="submit" class="btn btn-info button_show" value="Detalles">
              <input type="text" name="product_generic_id" value="<?php echo $row['product_generic_id']; ?>" hidden>
              <input type="text" name="table" value="camp" hidden>
            </form>
          </div>
        </div>

      <?php

      $cont++;
    }
  }
}



//SI NO SE BUSCA NADA ESPECIFICO SE MUESTRA SIEMPRE ESTA POR DEFECTO
//EN CASO DE QUE NO HAYA GET SE MOSTRARAN TODAS LAS ENTRADAS ORDENADAS POR LA FECHA EN LA QUE HAN SIDO ACTUALIZDAS. SIGUIENTO LA MISMA ESTRUCFTURA QUE ANTES CON EL CONTADOR PARA LAS CLASES Y LA VENTANA MODAL

  else {
    $select = $db->query("select distinct product_generic.id as product_generic_id, subtype.id as subtype_id, title, description, `foreign`, locate, price_individual, price_group, tickets_availables, discount.id as discount_id, subclass, discount.name as discount_name, discount.discount as discount_discount from camp inner join product_generic on camp.id_product_generic=product_generic.id inner join multimedia on product_generic.id=multimedia.id_product_generic inner join picture on multimedia.id=picture.id_multimedia inner join discount on product_generic.id_discount=discount.id inner join subtype on product_generic.id_subtype=subtype.id order by product_generic.updated desc");

    $cont = 1;

    foreach ($select as $row) {
      $price_individual_with_discount = ((100-$row['discount_discount'])/100)*$row['price_individual'];
      $price_group_with_discount = ((100-$row['discount_discount'])/100)*$row['price_group'];


      if ($cont==1) {
        $imagenes = $db->query("select route, name from multimedia where id_product_generic=" . $row['product_generic_id'] . " limit 1");

        foreach ($imagenes as $images) {
          $route_img = $images['route'];
          $name_img = $images['name'];
        }



        ?>

        <div id="special" class="grid_4 alpha grid_special">
          <div class="tour">
            <img src="<?php echo $route_img . $name_img; ?>" alt="<?php echo $name_img; ?>" class="img_inner fleft">
            <div class="extra_wrapper">
              <p class="text1"><?php echo $row['title']; ?></p>

              <?php

                if ($row['discount_id']!=1) {
                  echo "<p class='price'>Precio Individual <span>Por " . $price_individual_with_discount . " €</span></p>
                        <p class='price'>Precio de Grupo <span>Por " . $price_group_with_discount . " €</span></p>";
                }
                else {
                  echo "<p class='price'>Precio Individual <span>Por " . $row['price_individual'] . " €</span></p>
                        <p class='price'>Precio de Grupo <span>Por " . $row['price_group'] . " €</span></p>";
                }

               ?>

            </div>
            <form method="get" action="show_product.php">
              <input type="submit" class="btn btn-info button_show" value="Detalles">
              <input type="text" name="product_generic_id" value="<?php echo $row['product_generic_id']; ?>" hidden>
              <input type="text" name="table" value="camp" hidden>
            </form>
          </div>
        </div>

        <?php

          }
          else {

            //CON ESTA CONSULTA MUESTRO SOLO LA PRIMERA IMAGEN DE CADA PRODUCTO

              $imagenes2 = $db->query("select route, name from multimedia where id_product_generic=" . $row['product_generic_id'] . " limit 1");

              foreach ($imagenes2 as $images2) {
                $route_img2 = $images2['route'];
                $name_img2 = $images2['name'];
              }

         ?>

        <div id="special" class="grid_4 omega grid_special">
          <div class="tour">
            <img src="<?php echo $route_img2 . $name_img2; ?>" alt="<?php echo $name_img2; ?>" class="img_inner fleft">
            <div class="extra_wrapper">
              <p class="text1"><?php echo $row['title']; ?></p>

              <?php

                if ($row['discount_id']!=1) {
                  echo "<p class='price'>Precio Individual <span>Por " . $price_individual_with_discount . " €</span></p>
                        <p class='price'>Precio de Grupo <span>Por " . $price_group_with_discount . " €</span></p>";
                }
                else {
                  echo "<p class='price'>Precio Individual <span>Por " . $row['price_individual'] . " €</span></p>
                        <p class='price'>Precio de Grupo <span>Por " . $row['price_group'] . " €</span></p>";
                }

               ?>

            </div>
            <form method="get" action="show_product.php">
              <input type="submit" class="btn btn-info button_show" value="Detalles">
              <input type="text" name="product_generic_id" value="<?php echo $row['product_generic_id']; ?>" hidden>
              <input type="text" name="table" value="camp" hidden>
            </form>
          </div>
        </div>

      <?php

      $cont++;
    }
  }
}


if (mysqli_num_rows($select) == 0) {
  ?>
    <style type="text/css">
      .message_without_product {
        display: inherit !important;
      }
    </style>
  <?php
}


   ?>

   <p class="message_without_product" style="display: none;">En estos momentos no disponemos de productos con estas características.</p>

  </div>
</div>
