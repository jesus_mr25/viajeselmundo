<div id="search" class="grid_3 search">
  <h3>Buscar viajes</h3>
    <?php

    //COMO LA COLUMNA DE BUSCAR ENTRE LAS ENTRADAS ES EL MISMO EN TODAS LAS PAGINAS, REALIZO LO SIGUIENTE PARA OBTENER LA URL Y LA URI DE LA WEB EN LA QUE ESTOY EN ESE MOMENTO

      $host = $_SERVER["HTTP_HOST"];
      $url = $_SERVER["REQUEST_URI"];

    //ADEMAS OBTENGO EL NOMBRE DEL ARCHIVO EN EL QUE ESTOY

      $aux = basename($_SERVER['PHP_SELF']);

     ?>

     <!--ESTO LO HAGO PORQUE TODAS LAS ENTRADAS TIENEN LA OPCION DE ORDENAR LAS OFERTAS POR PRECIO INDIVIDUAL Y DE GRUPO, A EXCEPCION DEL CAMPUS, QUE SOLO TIENE PRECIO NORMAL.-->

    <!--CON ESTOS DOS SELECT MOSTRARE TODAS LAS OPCIONES DE BUSQUEDA DE LAS ENTRADAS-->

    <?php

    if (basename($_SERVER['PHP_SELF'])=="tickets.php") {
      $subtype = 2;
      $page = "tickets";
    }
    elseif (basename($_SERVER['PHP_SELF'])=="sports.php") {
      $subtype = 1;
      $page = "sports";
    }
    elseif (basename($_SERVER['PHP_SELF'])=="offer.php") {
      $subtype = 5;
      $page = "offer";
    }
    elseif (basename($_SERVER['PHP_SELF'])=="campus.php") {
      $subtype = 4;
      $page = "campus";
    }
    elseif (basename($_SERVER['PHP_SELF'])=="freetime.php") {
      $subtype = 3;
      $page = "freetime";
    }



      $aux = explode(".", $url);

      if ($aux = "tickets.php" || $aux = "sports.php" || $aux = "offer.php" || $aux = "freetime.php") {

    ?>
        <form action="<?php echo $page . ".php"; ?>" method="get" id="form2" class="form1" name="search">
            <label class="special">
                <span class="spann col1">Extranjero</span>
                  <select class="spann combo2 form-control" name="foreign">
                    <option value="">Cualquiera</option>
                    <option value="1">En el Extranjero</option>
                    <option value="0">En España</option>
                  </select>
            </label>

            <label class="mb0 special">
                <span class="spann">Localizar</span>
                <select class="combo3 form-control" name="price">
                    <option value="">Cualquiera</option>
                    <option value="order by price_individual asc">De más barato a más caro (Individual)</option>
                    <option value="order by price_individual desc">De más caro a más barato (Individual)</option>
                    <option></option>
                    <option value="order by price_group asc">De más barato a más caro (Grupos)</option>
                    <option value="order by price_group desc">De más caro a más barato (Grupos)</option>
                </select>
            </label>

            <label class="mb0 special">
                <span class="spann">Categorías</span>
                <select class="combo4 form-control" name="subcategory">
                    <option value="">Cualquiera</option>

                    <?php
                      $query_subtypes = $db->query("select * from subtype where id_type=" . $subtype);

                      foreach ($query_subtypes as $row35) {
                        echo "<option value='" . $row35['id'] . "'>" . $row35['subclass'] . "</option>";
                      }
                    ?>
                </select>
            </label>

            <div class="clear"></div>
            <button class="btn btn-info" style="margin-top: 20px;">Buscar</button>
        </form>

        <?php

          }
          else {
          ?>

            <form action="<?php echo "http://" . $host . $url; ?>" method="get" id="form2" class="form1" name="search">
                <label class="special">
                    <span class="spann col1">Extranjero</span>
                      <select class=" combo2 form-control" name="foreign">
                        <option value="">Cualquiera</option>
                        <option value="1">En el Extranjero</option>
                        <option value="0">En España</option>
                      </select>
                </label>

                <label class="mb0 special">
                    <span class="spann">Precio</span>
                    <select class="combo3 form-control" name="price">
                        <option value="">Cualquiera</option>
                        <option value="order by price asc">De más barato a más caro</option>
                        <option value="order by price desc">De más caro a más barato</option>
                    </select>
                </label>

                <label class="mb0 special">
                    <span class="spann">Categorías</span>
                    <select class="combo4 form-control" name="subcategory">
                        <option value="">Cualquiera</option>

                        <?php
                          $query_subtypes = $db->query("select * from subtype where id_type=2");

                          foreach ($query_subtypes as $row35) {
                            echo "<option value='" . $row35['id'] . "'>" . $row35['subclass'] . "</option>";
                          }
                        ?>
                    </select>
                </label>

                <div class="clear"></div>
                <button class="btn btn-info" style="margin-top: 20px;">Buscar</button>
            </form>

          <?php
          }

         ?>
</div>


<!--ESTE SCRIPT SE REPITE EN MUCHAS PAGINAS Y SIRVE PARA CARGAR LOS COMBOBOX CON EL VALOR QUE TIENEN DEL GET-->

<script type="text/javascript">
  $('.combo2').children('option[value="<?php if (isset($_GET['foreign'])) {echo $_GET['foreign'];} ?>"]').prop('selected', true);
  $('.combo3').children('option[value="<?php if (isset($_GET['price'])) {echo $_GET['price'];} ?>"]').prop('selected', true);
  $('.combo4').children('option[value="<?php if (isset($_GET['subcategory'])) {echo $_GET['subcategory'];} ?>"]').prop('selected', true);
</script>
