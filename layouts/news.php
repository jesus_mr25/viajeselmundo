 <!--ESTO ES EL CONTENEDOR DE LAS NOTICIAS-->

<?php

  $active = 0;

  $host = $_SERVER["HTTP_HOST"];

  $now = date("Y-m-d");


  if (isset($_POST['email'])) {

    $select_subscriber = $db->query("select id from subscriber where email='" . $_POST['email'] . "'");

    if (mysqli_num_rows($select_subscriber)) {
      $active = 3;
    }
    else {

      //A CONTINUACION, ENVIO UN CORREO AL NUEVO SUBSCRIPTOR PARA QUE RECIBA NOTICIAS CADA VEZ QUE SE CREEN

      require('phpmailer/PHPMailerAutoload.php');

      $subject = "Suscripción al boletín de Viajes El Mundo";
      $body = "<html lang='es'>
                <head>
                  <meta charset='utf-8'>
                </head>
                <body>
                  <img src='http://www.viajeselmundo.esy.es/images/mini_logo.png'>
                  <br>
                  <br>
                  <p>Bienvenido al boletín oficial de Viajes El Mundo.</p>
                  <p>Viajes El Mundo envia periódicamente un boletín de noticias, con la información más relvante acaecida en los últimos viajes.</p>
                  <p>El objetivo de este boletín de noticias o newsletter es compatir con nuestros clientes toda la información posible acerca de viajes interesantes.</p>
                  <p>Al ser suscriptor de Viajes El Mundo, recibirá estos correos electrónicos periódicamente a esta dirección.</p>
                  <br>
                  <br>
                  <p>Este boletín es voluntario, es decir, si no desea recibir esta información solo ha de seguir este <a href='http://" . $host . "/unscribe.php?user_unscribe=" . $_POST['email'] . "'>ENLACE</a></p>
                </body>
              </html>";

      $email = $_POST['email'];


      //CREO UN NUEVA CLASE DE PHPMAILER
      //LE PASO EL CORREO DEL RECEPTOR Y EL NOMBRE DEL REMITENTE
      //LE PASO EL CORREO DEL RECEPTOR
      //LE PONGO EL ASUNTO

      //LE PASO EL CUERPO DEL MENSAJE
      //LE PASO LA CODIFICACION DEL LENGUAJE
      //LE PASO EL TIPO DE MENSAJE
      //Y LO ENVIO

      $mailer = new PHPMailer();
      $mailer->setFrom('no-reply@viajeselmundo.es','Noticias - Viajes El Mundo');
      $mailer->addAddress($email);
      $mailer->Subject = $subject;
      $mailer->isHTML(true);
      $mailer->Body = $body;
      $mailer->CharSet = 'utf-8';
      $mailer->mailer = 'mail';

      //SI NO SE ENVIA EL MENSSAJE SE MOSTRARA UN MENSAJE DE DISCONFORMIDAD

      if (!$mailer->send()) {
          $active = 2;
      }

      else {
        $insert_suscriber = $db->query("insert into subscriber values(NULL, '" . $email . "', '" . $now . "')");

        $active = 1;
      }
    }
  }

 ?>




 <div class="grid_4 news">
  <div class="newsletter_title">Noticias </div>
  <div class="n_container">
    <form id="newsletter" action="index.php#news" method="post">
      <div class="success">¡Su solicitud ha sido enviada!</div>
      <div class="text1">Regístrese para recibir nuestros boletines</div>
      <label class="email">
        <input type="email" placeholder="Email" name="email" required>
      </label>
      <a name="news"></a>
      <div class="clear"></div>
      <div class="send_email"><button class="btn btn-info">Recibir Noticias</button></div>

      <?php

        if ($active == 1) {
          echo "<h4 style='color: black;'>¡Suscripción aceptada!</h4>";
        }
        elseif ($active == 2) {
          echo "<h4 style='color: black;'>¡Error al enviar el mensaje!</h4>";
        }
        elseif ($active == 3) {
          echo "<h4 style='color: black;'>¡Usuario ya suscrito!</h4>";
        }

       ?>

    </form>

    <ul class="list">


      <li><a href="#"></a></li>

    </ul>
  </div>
</div>
<div class="clear"></div>
