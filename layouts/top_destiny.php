<!--ESTOS SERIAN LOS CUADROS QUE APARECEN JUSTO POR DEBAJO DEL SLIDER PRINCIPAL DEL INDEX-->

<div class="container_12">
  <div class="grid_12">
    <h3 class="top">Destinos top</h3>
  </div>
  <div class="boxes">
    <div id="topDestiny" class="grid_4">
        <figure class="shadow">
          <?php 

          //MUESTRO TODOS LOS PROSUCTOS QUE HAN SIDO INSERTADOS EN LA TABLA TOP DESTINY

          //LA PRIMERA ES LA DE LA IZQUIERDA

            $select_product1 = $db->query("select title, description, link_product, name from product_generic inner join top_destiny on product_generic.id=top_destiny.id_product_generic inner join multimedia on product_generic.id=multimedia.id_product_generic where top_destiny.id=1 limit 1");

            foreach ($select_product1 as $row1) {
              $title1 = $row1['title'];
              $description1 = $row1['description'];
              $link_product1 = $row1['link_product'];
              $name1 = $row1['name'];
            }

           ?>
          <div><img src="backend/uploads/<?php echo $name1; ?>" alt=""></div>
          <figcaption>
            <h3><?php echo $title1; ?></h3>
            <note><?php echo substr($description1,0,147) . "..."; ?></note>
            <a href="<?php echo $link_product1; ?>" class="btn">Detalles</a>
          </figcaption>
        </figure>
      </div>



    <!--LA SEGUNDA ES LA DEL CENTRO-->


      <div id="topDestiny" class="grid_4">
        <figure class="shadow">
          <?php 

            $select_product2 = $db->query("select title, description, link_product, name from product_generic inner join top_destiny on product_generic.id=top_destiny.id_product_generic inner join multimedia on product_generic.id=multimedia.id_product_generic where top_destiny.id=2 limit 1");

            foreach ($select_product2 as $row2) {
              $title2 = $row2['title'];
              $description2 = $row2['description'];
              $link_product2 = $row2['link_product'];
              $name2 = $row2['name'];
            }

           ?>
          <div><img src="backend/uploads/<?php echo $name2; ?>" alt=""></div>
          <figcaption>
            <h3><?php echo $title2; ?></h3>
            <note><?php echo substr($description2,0,146) . " ..."; ?></note>
            <a href="<?php echo $link_product2; ?>" class="btn">Detalles</a>
          </figcaption>
        </figure>
      </div>


    <!--LA TERCERA ES LA DE LA DERECHA-->



      <div id="topDestiny" class="grid_4">
        <figure class="shadow">
          <?php 

            $select_product3 = $db->query("select title, description, link_product, name from product_generic inner join top_destiny on product_generic.id=top_destiny.id_product_generic inner join multimedia on product_generic.id=multimedia.id_product_generic where top_destiny.id=3 limit 1");

            foreach ($select_product3 as $row3) {
              $title3 = $row3['title'];
              $description3 = $row3['description'];
              $link_product3 = $row3['link_product'];
              $name3 = $row3['name'];
            }

           ?>
          <div><img src="backend/uploads/<?php echo $name3; ?>" alt=""></div>
          <figcaption>
            <h3><?php echo $title3; ?></h3>
            <note><?php echo substr($description3,0,147) . "..."; ?></note>
            <a href="<?php echo $link_product3; ?>" class="btn">Detalles</a>
          </figcaption>
        </figure>
      </div>
      <div class="clear"></div>
    </div>
</div>