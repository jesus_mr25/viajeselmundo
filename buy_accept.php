<!DOCTYPE html>
<html lang="es">
<head>
	<title></title>
	<meta charset="utf-8">
</head>
	<body>

		<?php

		    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

		    date_default_timezone_set("Europe/Madrid");


    		include('backend/conexionDB.php');

			$now = date("Y-m-d") . " " . date("G:i:s");

			//LA VARIABLE DEL POST ES LA QUE ENVIA PAYPAL SI EL PAGO SE HA PRODUCIDO CORRECTAMENTE

			if ($_POST['payment_status']=='Completed' || $_POST['payment_status']=='Processed') {

				//ESTA ES LA VARIABLE QUE USO PARA PASAR VARIOS VALORES DE UNA VEZ POR LA VARIABLE CUSTOM DEL POST DE PAYPAL

				$custom = explode(',', $_POST['custom']);

				/* LA VARIABLE CUSTOM:
				LA POSICION 0 ES LA TABLA EN LA QUE ESTA EL PRODUCTO,
				LA POSICION 1 ES EL DESCUENTO APLICADO EN EL PRODUCTO COMPRADO,
				LA POSICION 2 ES EL ID DEL USUARIO QUE HA COMPRADO,
				LA POSICION 3 ES EL TOKEN DE LA COMPRA,
				LA POSICION 4 ES EL NOMBRE COMPLETO DEL COMPRADOR,
				LA POSICION 5 ES LA DIRECCION DEL COMPRADOR,
				LA POSICION 6 ES EL CODIGO POSTAL DEL COMPRADOR,
				LA POSICION 7 ES LA CIUDAD DEL COMPRADOR,
				LA POSICION 8 ES EL TELEFONO DEL COMPRADOR,
				LA POSICION 9 ES EL EMAIL DEL COMPRADOR
				LA POSICION 10 ES EL ID DEL PRODUCTO EN LA TABLA PRODUCT_GENERIC
				LA POSICION 11 ES EL TITULO DEL PRODUCTO QUE SE HA COMPRADO
				LA POSICION 12 ES LA CANTIDAD DE PRODUCTOS COMPRADOS
				*/


				//REALIZO ESTE SWITCH PARA SABER EN QUE TABLA TENGO QUE REALIZAR LA CONSULTA DE LA RESTA DEL PRODUCTO

				switch ($custom[0]) {
					case 'ticket':
						$id_table = 2;
						break;

					case 'sport':
						$id_table = 1;
						break;

					case 'offer':
						$id_table = 5;
						break;

					case 'freetime':
						$id_table = 3;
						break;

					case 'camp':
						$id_table = 4;
						break;
				}


				$select = $db->query("select tickets_availables from product_generic where id='". $custom[10] . "'");

				foreach ($select as $row) {
					$old_quantity = $row['tickets_availables'];
				}

				$new_quantity = $old_quantity - $custom[12];

				$update = $db->query("update product_generic set tickets_availables='" . $new_quantity . "', updated='" . $now . "' where id='" . $custom[10] . "'");


				//AQUI COMPRUEBO QUE SI EL USUARIO ES NULL (QUE EL USUARIO NO ESTA REGISTRADO), ME TOMARA EL USUARIO QUE ESTA DEFINIDO PARA LOS NO REGISTRADOS


				if ($custom[2]=="NULL") {

					$select1 = $db->query("select id from user where name='no_user'");

					foreach ($select1 as $row) {
						$idUser = $row['id'];
					}

					$insert = $db->query("insert into buy values(null, '" . $id_table . "', '" . $idUser . "', '" . $custom[3] . "', '" . $custom[1] . "', '" . $_POST['mc_gross'] . "', '" . $now . "', '" . $custom[4] . "', '" . $custom[5] . "', '" . $custom[6] . "', '" . $custom[7] . "', '" . $custom[8] . "', '" . $custom[9] . "', '" . $custom[12] . "', '" . $custom[11] . "', '" . $now . "', '" . $now . "')");

				}
				else {

				}

					$select3 = $db->query("select * from user where id='" . $custom[2] . "'");

					foreach ($select3 as $row) {
						$full_name = $row['name'] . " " . $row['surname'];
						$address = $row['home_address'];
						$post_code = $row['post_code'];
						$city = $row['city'];
						$phone = $row['telephone'];
						$email = $row['address'];
					}

					$insert = $db->query("insert into buy values(null, " . $id_table . ", '" . $custom[2] . "', '" . $custom[3] . "', '" . $custom[1] . "', '" . $_POST['mc_gross'] . "', '" . $now . "', '" . $full_name . "', '" . $address . "', '" . $post_code . "', '" . $city . "', '" . $phone . "', '" . $email . "', '" . $custom[12] . "', '" . $custom[11] . "', '" . $now . "', '" . $now . "')");
			}



			$db->close();

		 ?>

	</body>
</html>
