<?php
  if (isset($_GET['new_pass'])) {
    $user = $_GET['new_pass'];
  }
  elseif (isset($_POST['pass'])) {
      require('backend/conexionDB.php');

      $user = $_POST['email'];

      if ($_POST['pass'] == $_POST['re_pass']) {
        $now = date("Y-m-d") . " " . date("G:i:s");

        $db->query("update user set pass='" . hash("sha256" , $_POST['re_pass']) . "', updated='" . $now . "' where address='" . $_POST['email'] . "'");

        ?>
          <style type="text/css">
            #success_pass {
              display: inherit !important;
            }
          </style>
        <?php

      }
      else {
        ?>
          <style type="text/css">
            #error_pass {
              display: inherit !important;
            }
          </style>
        <?php
      }
  }
  else {
    ?>
      <script type="text/javascript">
        window.location.href="login.php";
        alert("Su contraseña ha sido modificada satisfactoriamente.");
      </script>
    <?php
  }
 ?>





<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Viajes El Mundo</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="backend/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="backend/dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition lockscreen">

      <div id="error_pass" class="callout callout-danger" style="display: none;">
        <h4>¡Ups, algo salió mal!</h4>
        <p>Las contraseñas introducidas no son iguales, por favor vuelva a intentarlo.</p>
      </div>

      <div id="success_pass" class="callout callout-info" style="display: none;">
        <h4>¡Estupendo!</h4>
        <p>Su contraseña a sido cambiada satisfactoriamente.</p>
      </div>

    <!-- Automatic element centering -->
    <div class="lockscreen-wrapper">
      <div class="lockscreen-logo">
        <a href="index.php"><b>Viajes El Mundo</b></a>
      </div>

      <!-- START LOCK SCREEN ITEM -->
      <div class="lockscreen-item">

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials" method="post" action="pass_forgot.php">
          <div class="input-group">
            <input type="password" class="form-control" name="pass" placeholder="Contraseña" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Debe contener Mayúsculas, Minúsculas, Carácteres Especiales y debe tenes mínimo 8 caracteres" required>
            <input type="password" class="form-control" name="re_pass" placeholder="Repetir Contraseña" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Debe contener Mayúsculas, Minúsculas, Carácteres Especiales y debe tenes mínimo 8 caracteres" required>
            <input type="text" name="email" value="<?php echo $user; ?>" hidden>
            <div class="input-group-btn">
              <button class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
            </div>
          </div>
        </form><!-- /.lockscreen credentials -->

      </div><!-- /.lockscreen-item -->
      <div class="help-block text-center">
        Introduce tu nueva contraseña y quedará restaurada
      </div>

    </div><!-- /.center -->

    <!-- jQuery 2.1.4 -->
    <script src="../../plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="../../bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
