<!================= HEADER ====================>

	<?php include('layouts/header.php'); ?>
	
	<div class="main">
		<div class="container_12">

			<!================ LOGIN ===================>

			<?php include('layouts/login_content.php'); ?>
		</div>
	</div>

	<!================ BLUE FOOTER ===================>

	<?php include('layouts/blue_footer.php'); ?>

	<!================ SOCIAL FOOTER ===================>

	<?php include('layouts/social_footer.php'); ?>

	<!================ BUTTON FOOTER ===================>
	
	<?php include('layouts/button_bottom.php'); ?>

</body>
</html>

<?php 

	if (isset($_GET['token'])) {

	    //PONGO ESTO PORQUE SI EL SERVIDOR NO ESTA EN ESPAÑA TOME LA HORA ESPAÑOLA COMO POR DEFECTO

	    date_default_timezone_set("Europe/Madrid");
	    

		$now = date("Y-m-d") . " " . date("G:i:s");

		$db->query("update user set active='1', updated='" . $now . "', token_to_active='NULL' where token_to_active='" . $_GET['token'] . "'");

	}

	$db->close();

 ?>