<?php 


  include("layouts/header.php");
  include("layouts/left_column.php");
  include("layouts/top_emails.php");


//DIVIDO EL NUMERO TOTAL DE CORREOS PARA MOSTRARLOS DE 10 EN 10 POSTERIORMENTE

$result = $num_emails/10;

$cont = 1;
$cont2 = 1;
$color = "";

//CON EL BUCLE SACO TODOS LOS MENSAJE QUE TENGA EN LA BANDEJA DE ENTRADA

foreach($emails as $email_number) {    
    $overview = imap_fetch_overview($inbox,$email_number,0);
    $from = $overview[0]->from;
    $subject = $overview[0]->subject;
    $to = $overview[0]->to;
    $date = $overview[0]->date;
    $read = $overview[0]->seen;
    $uid = $overview[0]->uid;
    $num = imap_num_msg($inbox); 
    $color = "";


    //SI EL MENSAJE ESTA MARCADO COMO NO LEIDO, SE MOSTRARA EN COLOR VERDE


    if($read == 0) {
        $color = "green";
    }

    //PARA QUE DESDE EL PRINCIPIO NO SE MUESTREN TODOS LOS CORREOS, MIENTRAS $CONT2 SEA 1 SOLO SE IRAN CARGANDO TODOS LOS CORREOS, PERO CUANDO $CONT LLEGUE A 10, $CONT2 SE INCREMENTARA EN 1 POR LO QUE SOLO SE MOSTRARAN LOS 10 PRIMEROS CORREOS, TODOS LOS DEMAS SE IRAN CARGANDO PERO ESTARAN OCULTOS. UNA VEZ QUE SE HAYAN CARGADO TODOS LOS CORREOS, AL PINCHAR SOBRE LAS FLECHAS DERECHA E IZQUIERDA, SE IRAN PAGINANDO DE 10 EN 10

    if ($cont2==1) {
      echo "<tr class='td" . $cont2 . "' data-toggle='modal' data-target='#myModal" . $cont . "' style=' cursor: pointer;'>
            <td>
              <div aria-disabled='false' aria-checked='false' style='position: relative;' class='icheckbox_flat-blue'><input style='position: absolute; opacity: 0;' type='checkbox'>
                <ins style='position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;' class='iCheck-helper'></ins>
              </div>
            </td>
            <td class='mailbox-name'>
              <a style='color: " . $color . "; text-decoration: none; cursor: pointer;'><b>" . iconv_mime_decode($from) . "</b></a>
            </td>
            <td class='mailbox-subject'>" . iconv_mime_decode($subject) . "
          </td>
          <td class='mailbox-date'>" . date("d/m/Y", strtotime($date)) . "</td>
          </tr>";
    }
    else {
      echo "<tr class='td" . $cont2 . "' data-toggle='modal' data-target='#myModal" . $cont . "' style=' cursor: pointer;' hidden>
            <td>
              <div aria-disabled='false' aria-checked='false' style='position: relative;' class='icheckbox_flat-blue'><input style='position: absolute; opacity: 0;' type='checkbox'>
                <ins style='position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;' class='iCheck-helper'></ins>
              </div>
            </td>
            <td class='mailbox-name'>
              <a style='color: " . $color . "; text-decoration: none; cursor: pointer;'><b>" . imap_utf8($from) . "</b></a>
            </td>
            <td class='mailbox-subject'>" . iconv_mime_decode($subject) . "
          </td>
          <td class='mailbox-date'>" . date("d/m/Y", strtotime($date)) . "</td>
          </tr>";
    }

    if ($cont%10==0) {
        $cont2++;
    }

    ?>

    <!--EN ESTA VENTANA MODAL ES DONDE MUESTRO EL CONTENIDO DEL CORREO-->

    <div class="modal fade" id="myModal<?php echo $cont; ?>" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content" style="padding: 22px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">X</button>
                    <h3><?php echo iconv_mime_decode($subject); ?></h3>
                </div>

                <div class="modal-body">
                    <p>De <?php echo $to; ?></p>
                    <p>Recibido el <?php echo date("d/m/Y", strtotime($date)); ?> a las <?php echo date("G:m:s", strtotime($date)); ?></p>
                    <br>
                    <?php 

                        echo "<p></p>";

                    ?>
                </div>
            </div>

        </div>
    </div>

    <?php

    $cont++;
}


  imap_close($inbox);


    include("layouts/bottom_emails.php");
    include("layouts/footer.php");

 ?>

 <!---->

 <script type="text/javascript">
     $(document).ready(function(){
        var i = 1;

        $('.td1').prop('hidden', false);                          /*LOS 10 PRIMEROS TD ESTAN SIN OCULTAR*/
        $('.prev').prop('disabled', true);                        /*EL BOTON ANTERIOR ESTA DESACTIVADO*/

        $('.next').click(function(){                              /*AL HACER CLICK EN EL BOTON SIGUIENTE, OBTENGO EL VALOR DE I Y LE VOY SUMANDO 1 CADA VEZ QUE PINCHE EN SIGUIENTE*/
            $('.td' + i).prop('hidden', true);

            i++;
            
            $('.td' + i).prop('hidden', false);                   /*COMO SIEMPRE ESTAN OCULTOS LOS MENSAJES DE LAS PAGINAS SIGUIENTES, VOY OCULTANDO EL ANTERIOR Y MOSTRANDO EL SIGUIENTE*/
            $('.prev').prop('disabled', false);                   /*EN EL MOMENTO EN EL QUE PULSE SIGUIENTE UNA VEZ EL BOTON ANTERIOR SE ACTIVA*/

            if (i == <?php echo $cont2 ?>) {                      /*SI EL VALOR DE I ES IGUAL QUE EL ULTIMO GRUPO DE DIEZ DE LOS CORREOS, QUE SERIA $CONT2, PONE EL BOTON SIGUIENTE EN DISABLE*/
                $('.next').prop('disabled', true);
            }
        });


        $('.prev').click(function(){/*EL BOTON ANTERIOR FUNCIONA IGUAL QUE SIGUIENTE, PERO EN LA CONDICION EN VEZ DE SER EL NUMER MÁXIMO PAGINAS SERIA SOLO EN LA 1 CUANDO SE PONE EL BOTON CON LA PROPIEDAD DISABLE*/
            $('.td' + i).prop('hidden', true);

            i--;
            
            $('.td' + i).prop('hidden', false);
            $('.next').prop('disabled', false);

            if (i == 1) {
                $('.prev').prop('disabled', true);
            }
        });

     });
 </script>