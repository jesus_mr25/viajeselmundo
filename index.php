<!================= HEADER ====================>

	<?php include('layouts/header.php'); ?>
	
	<div class="main">

		<!=============== PRINCIPAL SLIDER ================>

		<?php include('layouts/sliders.php'); ?>

		<!================ TOP DESTINY ===================>

		<?php include('layouts/top_destiny.php'); ?>

		<div class="up_footer">

			<!================= TABS ====================>

			<?php include('layouts/tabs.php'); ?>

			<!================= NEWS ====================>

			<?php include('layouts/news.php'); ?>
		</div>
	</div>


	<!================ BLUE FOOTER ===================>

	<?php include('layouts/blue_footer.php'); ?>

	<!================ SOCIAL FOOTER ===================>

	<?php include('layouts/social_footer.php'); ?>

	<!================ BUTTON FOOTER ===================>
	
	<?php include('layouts/button_bottom.php'); ?>

</body>
</html>

<?php 

	$db->close();

 ?>