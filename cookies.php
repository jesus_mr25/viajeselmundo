<!================= HEADER ====================>

	<?php include('layouts/header.php'); ?>

	<div class="main" style="padding: 50px;">

		<h1 style="font-size: 35px;">Política de Cookies</h1><br>

		<p>Esta web utiliza cookies propias y de terceros para mejorar la experiencia de usuario y ofrecer contenidos adaptados a sus intereses, mediante la personalización de espacios publicitarios. Con el registro en la web y/o la mera navegación (con el navegador habilitado para aceptar cookies), el usuario acepta la instalación de cookies, salvo que se oponga, según se detalla en esta Política de Cookies.</p>
<br>
		<p>Nuestra Política de Cookies está sujeta a actualizaciones periódicas. Su propósito es ayudarle a comprender el uso que hacemos de las cookies, la finalidad de las cookies utilizadas, así como las opciones que tiene el usuario para gestionarlas.</p>
<br>
		<p>Los usuarios pueden acceder a esta información en todo momento a través del link habilitado en la web (información legal ) . Asimismo, podrán modificar sus preferencias sobre la aceptación de cookies a través de las opciones de su navegador.</p>
<br>
		<p>La Política de Cookies de Viajes El Mundo comprende a este dominio y a todos los servicios que ofrezcamos.</p>

		<br><br>

		<h1 style="font-size: 25px;">¿Qué es una Cookie?</h1><br>

		<p>Una “Cookie” es un pequeño archivo de texto que un sitio web almacena en el navegador del usuario. Las cookies facilitan el uso y la navegación por una página web y son esenciales para el funcionamiento de internet, aportando innumerables ventajas en la prestación de servicios interactivos.</p>
<br>
		<p>Las cookies se utilizan, por ejemplo, para gestionar la sesión del usuario (reduciendo el número de veces que tiene que incluir su contraseña), mejorar los servicios ofrecidos, o para adecuar los contenidos de una página web a sus preferencias.</p>
<br>
		<p>Las cookies pueden ser de “sesión”, por lo que se borrarán una vez el usuario abandone la página web que las generó o “persistentes”, que permanecen en su ordenador hasta una fecha determinada.</p>
<br>
		<p>Asimismo las Cookies pueden ser “propias”, gestionadas por el dominio al que el usuario accede y del que solicita un determinado servicio (en este caso los dominios titularidad de Viajes El Mundo) o “cookies de terceros”, enviadas al equipo de usuario desde un dominio diferente al que se accede.</p>

		<br><br>

		<h1 style="font-size: 25px;">Cookies utilizadas</h1><br>

		<p>En todo momento, y al menos, durante la instalación o puesta al día del navegador, el usuario tiene la posibilidad de aceptar o rechazar la instalación de cookies, o bien rechazar la instalación de un determinado tipo de cookies, como las cookies de publicidad y de terceros. Además, después de cada sesión puede eliminar todas o algunas de las cookies almacenadas. Asimismo el usuario puede activar:</p>
		<br>
		<p>La navegación privada, mediante la cual su navegador deja de guardar el historial de navegación, contraseñas de sitios web, cookies y otra información de las páginas que visita, o la función de no rastrear, por la que el navegador pide a los sitios web que visita que no rastreen sus hábitos de navegación, para por ejemplo, servirle publicidad de su interés en los sitios que visita.</p>
		<br>
		<p>Le recomendamos que consulte la ayuda de su navegador para informarse de las diferentes opciones sobre cómo gestionar las cookies. A continuación incluimos los vínculos para gestionar cookies en los navegadores más relevantes:</p>

		    <li style="margin-left: 20px;">Internet Explorer</li>
		    <li style="margin-left: 20px;">Firefox</li>
		    <li style="margin-left: 20px;">Safari</li>
		    <li style="margin-left: 20px;">Google Chrome</li>

		    <br>

		<p>Tenga en cuenta que el bloqueo de cookies puede afectar a algunas de las funcionalidades de nuestra web.</p>

	</div>


	<!================ BLUE FOOTER ===================>

	<?php include('layouts/blue_footer.php'); ?>

	<!================ SOCIAL FOOTER ===================>

	<?php include('layouts/social_footer.php'); ?>

	<!================ BUTTON FOOTER ===================>

	<?php include('layouts/button_bottom.php'); ?>

</body>
</html>
