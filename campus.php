<!================= HEADER ====================>

  <?php include('layouts/header.php'); ?>
  
  <div class="main">
    <div class="container_12">

      <!=============== SEARCH FORMS ================>

      <?php include('layouts/search_travel.php'); ?>

      <!============== CAMPUS =================>

      <?php include('layouts/campus_content.php'); ?>
      
    </div>
  <div class="clear"></div>
  </div>

  <!================ BLUE FOOTER ===================>

  <?php include('layouts/blue_footer.php'); ?>

  <!================ SOCIAL FOOTER ===================>

  <?php include('layouts/social_footer.php'); ?>

  <!================ BUTTON FOOTER ===================>
  
  <?php include('layouts/button_bottom.php'); ?>

</body>
</html>

<?php 

  $db->close();

 ?>