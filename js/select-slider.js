/**
 * This script will handle events Gigabanner selection
 */

//Variable that list all binded elements.
var slidersSelector_list;

/**
 * Handler that will change configured hidden input's value
 */
var sliderSelector_handler = function sliderSelectorHandler(event) {
	$.ajax({
		url: ''+sliderSelector_url,
		data: {id:event.data.slider_id},
		success: function(data) {
			$('#modalSelectSlider').modal('toggle');
			if(data!=false) {
				$('[name="'+sliderSelector_input+'"]').val(event.data.slider_id);
				$(sliderSelector_trigger).html('Cambiar');
				sliderSelectorUpdateSlider(data);
			} else {
				console.log('No se ha podido añadir el item.');
			}
			sliderSelectorUnbindAll();
			sliderSelectorClearModalContet();
			
		}
	});
}

/**
 * This function binds all retrieved sliders' buttons
 */
function sliderSelectorBindAll() {
	slidersSelector_list = $("[ajax-function='ajaxSelectSlider']");
	for(i=0;i<slidersSelector_list.size();i++) {
		$(slidersSelector_list[i]).bind(
			"click",
			{
				slider_id:$(slidersSelector_list[i],this).attr('var-id')
			},
			sliderSelector_handler
		);
	}
}

/**
 * Clear binded event. This prevents system from clogging with too many events.
 */
function sliderSelectorUnbindAll() {
	for(i=0;i<slidersSelector_list.size();i++) {
		$(slidersSelector_list[i]).unbind("click",sliderSelector_handler);
	}
}

/**
 * Clears modal's body content.
 */
function sliderSelectorClearModalContet() {
	$('#modalSelectSlider .modal-body').html('');
}

function sliderSelectorUpdateSlider(data) {
	$('#selected_slider > .box-body').html(
		data
	);
}

/**
 * This binds the button that triggers modal box
 */
$(sliderSelector_trigger).on('click',function(event){
	$.ajax({
		url: ''+sliderSelector_ajax,
		data: {ajax_function:'ajaxSelectSlider'},
		success: function(data) {
			$('#modalSelectSlider #ajax-data-result').html(data);
			sliderSelectorBindAll();
		}
	});
});