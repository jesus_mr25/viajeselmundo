var groupReserveSelector = $('[data-action="select"]');
var groupReserveOutput = $('[data-action="selected-offer"]');
var groupReserveCalendar = $('[role="product-calendar"]');
var groupReserveShadowCalendar = $('[role="calendar-item-shadow"]');

groupReserveSelector.change(function(){
	console.log('seleccionado un nuevo evento: ' + $(this).val());
	groupReserveCalendar.html('');
	
	$.ajax({
		url: ajax_url,
		method: 'get',
		dataType: 'json',
		data: {id:$(this).val()},
		success: function(data) {
			console.log(data);
			
			$.each(data, function(key,value) {
				console.log(value.date);
				createCalendarEntry(value);
			});
		},
		error: function(data) {
			console.log('error');
		}
	});
});

function createCalendarEntry(entry) {
	var new_item = groupReserveShadowCalendar.clone();
	new_item.removeAttr('role').attr('role','calendar-item');
	new_item.removeClass('hidden');
	new_item.find('.calendar-date').html(entry.date);
	var button = new_item.find('.btn');
	new_item.find('.btn').attr('data-offer-id',entry.id);
	new_item.find('.btn').attr('data-offer-date',entry.date);
	new_item.find('.btn').attr('data-action','select-date');
		
	new_item.appendTo(groupReserveCalendar);
	button.on('click',function(){
		setEventCalendarEntry(entry.date);
		var buttons = $('[data-action="select-date"]');
		$.each(buttons, function(index,value){
			$(value).removeAttr('selected');
			$(value).closest('[role="calendar-item"]').removeClass('selected-date');
		});
		if(!button.attr('selected')) {
			button.attr('selected',true);
			button.closest('[role="calendar-item"]').addClass('selected-date');
		}
	});
}

function setEventCalendarEntry(id) {
	groupReserveOutput.val(id);
}