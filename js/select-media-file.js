/**
 * This script is part of the modal media gallery inserting process.
 */

//Variable that stores media elements that will have binded ajax functions
var media_list;
//Handler for those media elements
var media_handler = function ajaxSelectMedia(event) {
	$.ajax({
		url: ''+ajax_url,
		data: {id:event.data.media_id},
		success: function(data) {
			$('#modalSelectImage').modal('toggle');
			if(data!=false) {
				//$('#banner-media_id').val(event.data.media_id);
				$('[name="'+hiddenMediaInput+'"]').val(event.data.media_id);
				$('#selectMediaFile').html('Cambiar');
				updateSelectedMedia(data);
				unbindAllMediaElements();
				clearModalContet();
			} else {
				console.log('No se ha podido añadir el item.');
				unbindAllMediaElements();
				clearModalContet();
			}
			
		}
	});
				
};

//Bind all multimedia elements from modal
function bindAllMediaElements() {
	media_list = $("[asynch-function='ajaxSelectMedia']");
	for(i=0;i<media_list.size();i++) {
		$(media_list[i]).bind(
			"click",
			{
				media_id:$(media_list[i],this).attr('var-id')
			},
			media_handler
		);
	}
}

//Unbind all multimedia elements from modal to cleanup the queue
function unbindAllMediaElements() {
	for(i=0;i<media_list.size();i++) {
		$(media_list[i]).unbind("click",media_handler);
	}
}

//Performs an ajax query to get
function updateSelectedMedia(data) {
	$('#selected_media > .box-body').html(
		data
	);
}

//This function clears content inside modal
function clearModalContet() {
	$('#modalSelectImage .modal-body').html('');
}


//Ajax Script that requests list of multimedia elements
$('#selectMediaFile').on('click',function(e) {
	$.ajax({
		url: ''+ajax_url2,
		data: {ajax_function:'ajaxSelectMedia'},
		success: function(data) {
			$('#modalSelectImage #ajax-data-result').html(data);
			bindAllMediaElements();
		}
	});
});