/**
 * This script is part of the modal media gallery inserting process.
 */

//Variable that stores media elements that will have binded ajax functions
var media_list;
//Handler for those media elements
var media_handler = function ajaxAddSlide(event) {
	$.ajax({
		url: ''+ajax_url,
		data: {id_slider:event.data.slider,id_media:event.data.media},
		success: function(data) {
			$('#myModal').modal('toggle');
			if(data!=false) {
				unbindAllMediaElements();
				insertCreatedSlide(data);
				increaseSlideCountBadge();
				clearModalContet();
			} else {
				alert('No se ha podido añadir el item.');
			}
			
		}
	});
};

//Bind all multimedia elements from modal
function bindAllMediaElements(slider_id) {
	media_list = $("[asynch-function='ajaxAddSlide']");
	for(i=0;i<media_list.size();i++) {
		$(media_list[i]).bind(
			"click",
			{
				slider:slider_id,
				media:$(media_list[i],this).attr('var-id')
			},
			media_handler
		);
	}
}

//Unbind all multimedia elements from modal to cleanup the queue
function unbindAllMediaElements() {
	for(i=0;i<media_list.size();i++) {
		$(media_list[i]).unbind("click",media_handler);
	}
}

//Performs an ajax query to get
function insertCreatedSlide(data) {
	$('#slides_list > .box-body').append(
		data
	);
}

//Increase badge value in box-header
function increaseSlideCountBadge() {
	var badge = $('#slides_list > .box-header > .box-title > .badge');
	var count = parseInt(badge.html()) + 1;
	if(isNaN(count))
		count = 0;
	badge.html(count);
}

//This function clears content inside modal
function clearModalContet() {
	$('#myModal .modal-body').html('');
}


//Ajax Script that requests list of multimedia elements
$('#add_slide').on('click',function(e) {
	$.ajax({
		url: ''+ajax_url2,
		data: {},
		success: function(data) {
			$('#myModal #ajax-data-result').html(data);
			bindAllMediaElements($($('#add_slide'),this).attr('data-model-id'));
		}
	});
});