/**
 * This script is ...
 */


//Elements handle list
var items_list;

var item_toggle_handler = function itemToggle(event) {
	var target = $(this);
	$.ajax({
		url: '?r=/item/ajax-operation',
		data: {id:event.data.item_id, op:event.data.op},
		success: function(data) {
			if(event.data.op=='status') {
				if(data=='visible') {
					target.children('span').html('Visible');
					target.children('i').removeClass('fa-eye-slash text-red');
					target.children('i').addClass('fa-eye text-green');
				} else {
					target.children('span').html('Oculto');
					target.children('i').removeClass('fa-eye text-green');
					target.children('i').addClass('fa-eye-slash text-red');
				}
			} else {
				if(data==1) {
					target.children('span').html('Destacado');
					target.children('i').removeClass('text-gray');
					target.children('i').addClass('text-yellow');
				} else {
					target.children('span').html('No Destacado');
					target.children('i').removeClass('text-yellow');
					target.children('i').addClass('text-gray');
				}
			}
		}
	});
}

//Binding function
function bindAllItemElements() {
	items_list = $("[ajax-op='true']");
	for(i=0;i<items_list.size();i++) {
		$(items_list[i]).bind(
			"click",
			{item_id:$($(items_list[i]),this).attr('ajax-item-id'), op:$(items_list[i]).attr('ajax-toggle')},
			item_toggle_handler
		);
	}
}

//Call function
bindAllItemElements();