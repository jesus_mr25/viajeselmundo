/*
 * Variables
 */
var slide_nodes, main_slider, input_node;
main_slider = $('#main-slider-form');

/*
 * Ajax Operations
 */
function ajaxUpdateSlideLink(slide_id,slide_link, btn){
	btn.button('loading');
	$.ajax({
		method:'post',
		url:ajax_update_link,
		data:{
			id:slide_id,
			link:slide_link,
			_csrf:ajax_csrf
		},
	})
	.done(function(data){
	})
	.always(function(){
		btn.button('reset');
	});
}

//Select all slides when ready
main_slider.ready(function(){
	slide_nodes = $('[slide-action="update"]');

	$(slide_nodes).each(function(index){
		$(this).on('click',function(){
			//Get the input data
			input_node = $('#'+$(this).attr('slide-for'));
			//Execute the Ajax query
			ajaxUpdateSlideLink($(this).attr('slide-id'), $(input_node).val(), $(this));
		});
	});
});