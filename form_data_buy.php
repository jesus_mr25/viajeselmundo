<!--ESTA ES LA VENTANA QUE PEDIRA AL USUARIO LOS DATOS ANTES DE REALIZAR LA COMPRA PARA PODER INSERTARLOS DESPUES EN EL RECIBO-->

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Viajes El Mundo</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="backend/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="backend/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="backend/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition register-page">
    <div class="register-box">
      <div class="register-logo">
        <a href="index.php"><b>Viajes El Mundo</b></a>
      </div>

      <div class="register-box-body">
        <p class="login-box-msg">Rellene los datos para continuar o <a href="login.php">inicie sesión</a></p>
        <form action="buy_ticket.php" method="get">
          <div class="form-group has-feedback">
            <input type="text" name="full_name" class="form-control" placeholder="Nombre y Apellidos" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="email" name="email" class="form-control" placeholder="Email" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" name="address" class="form-control" placeholder="Dirección" required>
            <span class="glyphicon glyphicon-home form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" name="post_code" class="form-control" placeholder="Código Postal" maxlength="5" pattern="[0-9]{5}" title="Debe contener 5 números" required>
            <span class="fa fa-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" name="city" class="form-control" placeholder="Ciudad" required>
            <span class="fa fa-fw fa-building form-control-feedback" style="margin-right: 8px;"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" name="phone" class="form-control" placeholder="Teléfono" maxlength="9" pattern="[0-9]{9}" title="Debe contener 9 números" required >
            <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox" required> Acepto los <a href="term_user.php">términos de uso</a>
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Continuar</button>
            </div><!-- /.col -->
          </div>

          <!--ESTOS SON LOS CAMPOS OCULTOS DONDE ALMACENO LOS DATOS DE LA COMPRA QUE SE QUIERE REALIAR-->

          <input type="text" name="title" hidden value="<?php echo $_GET['title']; ?>">                                                                     <!--TITULO-->
          <input type="text" name="description" hidden value="<?php echo $_GET['description']; ?>">                                                         <!--DESCRIPCION-->
          <input type="text" name="price_individual" hidden value="<?php echo $_GET['price_individual']; ?>">                                               <!--PRECIO GRUPO-->
          <input type="text" name="price_group" hidden value="<?php echo $_GET['price_group']; ?>">                                                         <!--PRECIO INDIVIDUAL-->
          <input type="text" name="table" value="ticket" hidden>                                                                                            <!--NOMBRE DE LA TABLA-->
          <input type="text" name="id_user" hidden value="<?php echo $_GET['id_user']; ?>">                                                                 <!--ID USUARIO-->
          <input type="text" name="type_price" hidden value="<?php if(isset($_GET['type_price'])){echo $_GET['type_price'];}else{echo "off";} ?>">          <!--PRECIO GRUPO O INDIVIDUAL-->
          <input type="text" name="num_ticket" hidden value="<?php echo $_GET['num_ticket']; ?>">                                                           <!--CANTIDAD-->
          <input type="text" name="discount" hidden value="<?php echo $_GET['discount']; ?>">                                                               <!--DISCOUNT-->
          <input type="text" name="id_product_generic" hidden value="<?php echo $_GET['id_product_generic']; ?>">                                           <!--ID DEL PRODUCTO-->

        </form>

        <div class="social-auth-links text-center">

        </div>

      </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    <!-- jQuery 2.1.4 -->
    <script src="backend/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="backend//bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="backend/plugins/iCheck/icheck.min.js"></script>
  </body>

<!--ESTA FUNCION REALIZA EL CHEQUEADO DEL CHECKBOX-->

  <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
</html>
